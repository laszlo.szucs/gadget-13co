#ifdef RADCOL

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <mpi.h>
#include <unistd.h>
#include <limits.h>

#include "allvars.h"
#include "proto.h"


/*  Calculates the radial density profile (gas) on the fly
    for use in the Dark Star project. The centre is taken to be
    the peak potential in the code.
    Also works out the column density from the resulting density
    profile. Note that the output of this subroutine is in CODE units! :)
*/
     
void density_profile(void)
{
   int i, ishell;
   int igotsofar;
   int ibin;                         /* Interger radial bin number */
   int MasterTask;                   /* Task on which global potenital minimum resides */              
   double local_max_potential;       /* Value of potential minimum on this thread */
   double global_max_potential;      /* Value of potential minimum on this thread */
   double xcom, ycom, zcom;          /* Location of local potential minimum */
   double hmin;                      /* the smoothing length associated with this particle*/
   double mass_thread;               /* Mass on this thread */
   double dx, dy, dz, rad;           /* Coordinates relative to COM */
   double mrprofile_local[NRSHELLS]; /* Local radial mass profile */
   double logrbin;                   /* Width of the radial bins in log10 */
   double buffer[4];                 /* Holds the position/h of the minimum for export/inport */
   double shell_volume;              /* Volume of each shell... used for density profile*/
   double mass_check;                /* quick check that the total mass is correct!*/
   double usigma;                    /* units of column density */
   double rpower;                    /* for converting from log10 to real radii */
   MPI_Status status;                /* For finding task with overall potential min */
   struct{
      double val;
      int rank;
   }in, out;
   
  
  /* Compute the units of column density */
 
   usigma = All.TotalMass/All.UnitLength_in_cm/All.UnitLength_in_cm;

   /* Need the potential to be computed... */
   compute_potential();
   
   /* Find the radius of the cloud first! */

   printf("Task %d calling get_max_rcloud \n", ThisTask);

    get_max_rcloud();

   /* find local potential maximum... */
   
   printf("Task %d getting local pot max \n", ThisTask);

   igotsofar = 0;
   for (i=0; i < N_gas; i++) {
      if (P[i].Type != 0 || P[i].ID < 0)
         continue;
      if ( igotsofar == 0 ) {
         igotsofar = 1;
         local_max_potential = fabs( P[i].Potential );
         xcom = P[i].Pos[0];
         ycom = P[i].Pos[1];
         zcom = P[i].Pos[2];
         hmin = SphP[i].Hsml;
         }
      else if ( fabs( P[i].Potential ) > local_max_potential  ) {
         local_max_potential = fabs( P[i].Potential );
         xcom = P[i].Pos[0];
         ycom = P[i].Pos[1];
         zcom = P[i].Pos[2];
         hmin = SphP[i].Hsml;
         }
     }
     
     /* now find the maximum potential from all threads */
   
   printf("Task %d Doing pot communication \n", ThisTask);

   in.val = local_max_potential;
   in.rank = ThisTask;
   MPI_Allreduce(&in, &out, 1, MPI_DOUBLE_INT, MPI_MAXLOC, MPI_COMM_WORLD);
   global_max_potential = out.val;
   MasterTask = out.rank;
     
   buffer[0] = xcom;
   buffer[1] = ycom;
   buffer[2] = zcom;
   buffer[3] = hmin;
   

   printf("Task %d Getting location \n", ThisTask);
     
   MPI_Bcast(buffer, 3, MPI_DOUBLE, MasterTask, MPI_COMM_WORLD);  
         
   All.xcom = buffer[0];
   All.ycom = buffer[1];
   All.zcom = buffer[2];
   All.hmin = buffer[3];
     
   if (ThisTask == 0) {
      printf("Maximum potential is %g on task %d \n", global_max_potential, MasterTask);  
      printf("Position: %g %g %g smoothing length %g \n", All.xcom, All.ycom, All.zcom, All.hmin);
   } 
    
   /* clean local version of the radial mass profile */
     
   for (i=0; i<NRSHELLS; i++) {
      mrprofile_local[i] = 0.0;
   }
   logrbin = (log10(All.RMaxCloud) - log10(All.hmin))/NRSHELLS;
#ifdef DMA
   All.RadiusFactor = pow(10, logrbin);
#endif     

     
   /* bin particles by logarithm of radius */
     
   for (i=0; i < N_gas; i++) {
   if (P[i].Type != 0 || P[i].ID < 0)
      continue;
      dx = P[i].Pos[0] - All.xcom;
      dy = P[i].Pos[1] - All.ycom;
      dz = P[i].Pos[2] - All.zcom;
      rad = sqrt(dx*dx + dy*dy + dz*dz);
      ibin = (log10(rad) - log10(All.hmin))/logrbin;
      if ( rad < All.hmin ) 
         ibin = 0;
      if (ibin > (NRSHELLS - 1) ) 
         ibin = (NRSHELLS - 1);
      if ( ibin < 0  ) {
         printf("density_profile 1: ibin < 0! ibin = %d \n", ibin);
        endrun(666);
      }
      mrprofile_local[ibin] += P[ibin].Mass;
   }
     
   /* Now do sum all the local contributions to get the global radial mass profile */

   printf("Task %d summing the mass profiles \n", ThisTask);
   MPI_Allreduce(mrprofile_local, All.MRProfile, NRSHELLS, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
   printf("Task %d done the mass sum \n", ThisTask);  
 
   /* Check the mass profile! */
   
   mass_check = 0.;
   for (i=0; i<NRSHELLS; i++) {
      mass_check += All.MRProfile[i];
   }
   
   if (ThisTask == 0) {
      printf("Total mass from particles %g and from profile %g \n", All.TotalMass, mass_check);
   } 

   /* Set the values of the radius */

   printf("Getting the radii \n");
   for(i=0; i<NRSHELLS; i++) {
      rpower = (i+1)*logrbin + log10(All.hmin);
      All.RValues[i] = pow(10, rpower);
   }
   /* Work out the density profile */

   printf("Task %d getting density profile \n", ThisTask);
   for(i=0; i<NRSHELLS; i++) {
      if ( i==0 ) {
         shell_volume = 4.0*PI/3.0*pow(All.RValues[i], 3);
         }
      else { 
         shell_volume = 4.0*PI/3.0*(pow(All.RValues[i], 3) - pow(All.RValues[i-1], 3));
         }
      All.RhoValues[i] = All.MRProfile[i]/shell_volume;
   }
   
   if (ThisTask == 0) {
      printf("All.min : %g \n", All.hmin);
      printf("Density from centre, middle and edge %g %g %g \n", All.RhoValues[0], All.RhoValues[NRSHELLS/2], All.RhoValues[NRSHELLS-1]);
      printf("Radius from centre, middle and edge %g %g %g \n", All.RValues[0], All.RValues[NRSHELLS/2], All.RValues[NRSHELLS-1]);
   }
   
   /* Get the column density for each particle to the surface */      
   
   printf("Task %d Calculating column densities... \n", ThisTask);
   for (i=0; i<NRSHELLS; i++) {
      ishell = NRSHELLS - 1 - i;
      if ( ishell==(NRSHELLS-1) ) {
         All.ColProfile[ishell] = 2.0*All.RhoValues[ishell]*(All.RValues[ishell] - All.RValues[ishell-1]);
      }
      else if ( ishell==0 ) {
         All.ColProfile[ishell] = All.ColProfile[ishell+1] + 2.0*All.RhoValues[ishell]*All.RValues[ishell];
      }
      else {
         All.ColProfile[ishell] = All.ColProfile[ishell+1] + 2.0*All.RhoValues[ishell]*(All.RValues[ishell] - All.RValues[ishell-1]);
      }
   }
   
   if (ThisTask == 0) {
      printf("Column Density from centre, middle and edge %g %g %g \n", All.ColProfile[0], 
      All.ColProfile[NRSHELLS/2], All.ColProfile[NRSHELLS-1]);
   }
   /*endrun(666);*/
   
   printf("Task %d assigning col dens to particles \n", ThisTask);
   for (i=0; i < N_gas; i++) {
   if (P[i].Type != 0 || P[i].ID < 0)
      continue;
      dx = P[i].Pos[0] - All.xcom;
      dy = P[i].Pos[1] - All.ycom;
      dz = P[i].Pos[2] - All.zcom;
      rad = sqrt(dx*dx + dy*dy + dz*dz);
      ibin = (log10(rad) - log10(All.hmin))/logrbin;
      if ( rad < All.hmin )
         ibin = 0;
      if (ibin > (NRSHELLS - 1) ) 
         ibin = (NRSHELLS - 1);
      if ( ibin < 0  ) {
         printf("density_profile 2: ibin < 0! ibin = %d \n", ibin);
         endrun(666);
      }
      SphP[i].RadCol = All.ColProfile[ibin];  /* This is converted into cgs in chemcool! */
    }
    
}    
    
#endif /* RadCol */


