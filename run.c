#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <mpi.h>
#include <unistd.h>
#include <limits.h>

#include "allvars.h"
#include "proto.h"

/*! \file run.c
 *  \brief  iterates over timesteps, main loop
 */

/*! This routine contains the main simulation loop that iterates over single
 *  timesteps. The loop terminates when the cpu-time limit is reached, when a
 *  `stop' file is found in the output directory, or when the simulation ends
 *  because we arrived at TimeMax.
 */
void run(void)
{
  FILE *fd;
  int stopflag = 0;
  char stopfname[200], contfname[200];
  double t0, t1, tstart, tend;
  int nsinks, i; /*SINKS*/

  sprintf(stopfname, "%sstop", All.OutputDir);
  sprintf(contfname, "%scont", All.OutputDir);
  unlink(contfname);


#if defined RAYTRACE && defined CHEMCOOL
  tstart = second();

  if (All.PhotochemApprox == 2) {
    raytrace();
  }

  tend = second();
  All.CPU_Raytrace += timediff(tstart, tend);
#endif /* RAYTRACE && CHEMCOOL */

/* iphoto = 4 needs the maximum radius to calculate the column density */
#ifdef CHEMCOOL
  if ( All.PhotochemApprox == 4 ) {
     get_max_rcloud();
     if(ThisTask == 0) {
        printf("Maximum radius of cloud: %g \n ", All.RMaxCloud);
        fflush(stdout);
     }
  }
#endif 

  do				/* main loop */
    {
      t0 = second();

      find_next_sync_point_and_drift();	/* find next synchronization point and drift particles to this time.
					 * If needed, this function will also write an output file
					 * at the desired time.
					 */

      every_timestep_stuff();	/* write some info to log-files */


      domain_Decomposition();	/* do domain decomposition if needed */

      N_sinks = 0;
      for(i = 0; i < NumPart; i++)
       {
         if(P[i].Type == 5)
           N_sinks++;
       }

      compute_accelerations(0);	/* compute accelerations for 
				 * the particles that are to be advanced  
				 */

      /* check whether we want a full energy statistics */
      if((All.Time - All.TimeLastStatistics) >= All.TimeBetStatistics)
	{
#ifdef COMPUTE_POTENTIAL_ENERGY
	  compute_potential();
#endif
	  energy_statistics();	/* compute and output energy statistics */
	  All.TimeLastStatistics += All.TimeBetStatistics;
	  
	  /* iphoto = 4 needs the maximum radius to calculate the column density */
#ifdef CHEMCOOL
	  if ( All.PhotochemApprox == 4 ) {
	    get_max_rcloud();	    
	    if(ThisTask == 0) {
             printf("Maxium radius of cloud: %g \n ", All.RMaxCloud);
             fflush(stdout);
           }
	  }
#endif
          /* Ray trace is done here only every time the statistics are computed 
             Moved from timestep.c */
 #if defined RAYTRACE && defined CHEMCOOL
          tstart = second();

          if (All.PhotochemApprox == 2) {
            raytrace();
          }

          tend = second();
          All.CPU_Raytrace += timediff(tstart, tend);
#endif /* RAYTRACE && CHEMCOOL */


	}

/*SINKS*/
      if(ThisTask == 0)
       {
         printf("sink checking started\n ");
         fflush(stdout);
       }

      tstart = second();
      nsinks = accrete();                    /*SINK: accrete for sink particles*/
      tend = second();
      All.CPU_Sinks += timediff(tstart,tend);
      MPI_Barrier(MPI_COMM_WORLD);
      fflush(stdout);
      if(ThisTask == 0)
       {
         printf("sink checking ended %d\n\n",nsinks);
         fflush(stdout);
       }

      if(nsinks)
        { /* SINK:do domain decomposition if sinks created */
          All.NumForcesSinceLastDomainDecomp = All.TotNumPart*All.TreeDomainUpdateFrequency+1;
        }
/*SINKS*/

      advance_and_find_timesteps();	/* 'kick' active particles in
					 * momentum space and compute new
					 * timesteps for them
					 */
#ifdef DEBUG_SIGMA_PCC   
      exit(0);
#endif

#ifdef HISTORY
      fflush(FdHist);
#endif

#ifdef TURBULENCE 
      if(N_gas>0)
        {
          tstart = second();
 
          rsk_turbdriving();
 
          tend = second();
          All.CPU_Turbulence+= timediff(tstart,tend);
        }
#endif 

      All.NumCurrentTiStep++;

      /* Check whether we need to interrupt the run */
      if(ThisTask == 0)
	{
	  /* Is the stop-file present? If yes, interrupt the run. */
	  if((fd = fopen(stopfname, "r")))
	    {
	      fclose(fd);
	      stopflag = 1;
	      unlink(stopfname);
	    }

	  /* are we running out of CPU-time ? If yes, interrupt run. */
	  if(CPUThisRun > 0.85 * All.TimeLimitCPU)
	    {
	      printf("reaching time-limit. stopping.\n");
	      stopflag = 2;
	    }
	}

      MPI_Bcast(&stopflag, 1, MPI_INT, 0, MPI_COMM_WORLD);

      if(stopflag)
	{
	  restart(0);		/* write restart file */
	  MPI_Barrier(MPI_COMM_WORLD);

	  if(stopflag == 2 && ThisTask == 0)
	    {
	      if((fd = fopen(contfname, "w")))
		fclose(fd);
	    }

	  if(stopflag == 2 && All.ResubmitOn && ThisTask == 0)
	    {
	      close_outputfiles();
	      system(All.ResubmitCommand);
	    }
	  return;
	}

      /* is it time to write a regular restart-file? (for security) */
      if(ThisTask == 0)
	{
	  if((CPUThisRun - All.TimeLastRestartFile) >= All.CpuTimeBetRestartFile)
	    {
	      All.TimeLastRestartFile = CPUThisRun;
	      stopflag = 3;
	    }
	  else
	    stopflag = 0;
	}

      MPI_Bcast(&stopflag, 1, MPI_INT, 0, MPI_COMM_WORLD);

      if(stopflag == 3)
	{
	  restart(0);		/* write an occasional restart file */
	  stopflag = 0;
	}

      t1 = second();

      All.CPU_Total += timediff(t0, t1);
      CPUThisRun += timediff(t0, t1);
    }
  while(All.Ti_Current < TIMEBASE && All.Time <= All.TimeMax);

  restart(0);

  savepositions(All.SnapshotFileCount++);	/* write a last snapshot
						 * file at final time (will
						 * be overwritten if
						 * All.TimeMax is increased
						 * and the run is continued)
						 */
}


/*! This function finds the next synchronization point of the system (i.e. the
 *  earliest point of time any of the particles needs a force computation),
 *  and drifts the system to this point of time.  If the system drifts over
 *  the desired time of a snapshot file, the function will drift to this
 *  moment, generate an output, and then resume the drift.
 */
void find_next_sync_point_and_drift(void)
{
  int n, min, min_glob, flag, *temp, i;
  double timeold;
  double t0, t1;
#if defined(TREE_RAD) && defined(TREE_RAD_DEBUG)
  int k;
  double theta, phi;
  char buf[200];
  FILE *FdTree;
#endif
#if defined(TREE_RAD) && defined(HEALPIX_DUMP)
  int k;
  char buf[200];
  FILE *FdTree;
#endif

  t0 = second();

  timeold = All.Time;

  /*SINK - must skip any accreted particles at the beginning of the SPH particle list*/
  for(i = 0; i < NumPart; i++)
    {
      if(P[i].Type == 5 || P[i].ID >= 0)
	break;
    }


  if(i == NumPart)
    min = INT_MAX;
  else
    {
      for(n = i+1, min = P[i].Ti_endstep; n < NumPart; n++)
	{
	  if(P[n].Type == 0 && P[n].ID < 0) /*SINK*/
	    continue;

          if(min > P[n].Ti_endstep)
            min = P[n].Ti_endstep;
	}
    } 

  MPI_Allreduce(&min, &min_glob, 1, MPI_INT, MPI_MIN, MPI_COMM_WORLD);

  /* We check whether this is a full step where all particles are synchronized */
  flag = 1;
  for(n = 0; n < NumPart; n++)
    {
      if(P[n].Type == 0 && P[n].ID < 0) /*SINK*/
	continue;
      if(P[n].Ti_endstep > min_glob)
        flag = 0;
    }

  MPI_Allreduce(&flag, &Flag_FullStep, 1, MPI_INT, MPI_MIN, MPI_COMM_WORLD);

#ifdef PMGRID
  if(min_glob >= All.PM_Ti_endstep)
    {
      min_glob = All.PM_Ti_endstep;
      Flag_FullStep = 1;
    }
#endif

  /* Determine 'NumForceUpdate', i.e. the number of particles on this processor that are going to be active */
  for(n = 0, NumForceUpdate = 0; n < NumPart; n++)
    {
      if(P[n].Type == 0 && P[n].ID < 0) /*SINK*/
	continue;
      if(P[n].Ti_endstep == min_glob)
#ifdef SELECTIVE_NO_GRAVITY
        if(!((1 << P[n].Type) & (SELECTIVE_NO_GRAVITY)))
#endif
          NumForceUpdate++;
    }

  /* note: NumForcesSinceLastDomainDecomp has type "long long" */
  temp = malloc(NTask * sizeof(int));
  MPI_Allgather(&NumForceUpdate, 1, MPI_INT, temp, 1, MPI_INT, MPI_COMM_WORLD);
  for(n = 0; n < NTask; n++)
    All.NumForcesSinceLastDomainDecomp += temp[n];
  free(temp);



  t1 = second();

  All.CPU_Predict += timediff(t0, t1);

  while(min_glob >= All.Ti_nextoutput && All.Ti_nextoutput >= 0)
    {
#ifdef CHEMCOOL
      All.NeedAbundancesForOutput = 1;
#endif
      move_particles(All.Ti_Current, All.Ti_nextoutput);

      All.Ti_Current = All.Ti_nextoutput;

      if(All.ComovingIntegrationOn)
	All.Time = All.TimeBegin * exp(All.Ti_Current * All.Timebase_interval);
      else
	All.Time = All.TimeBegin + All.Ti_Current * All.Timebase_interval;

#ifdef OUTPUTPOTENTIAL
      All.NumForcesSinceLastDomainDecomp = 1 + All.TotNumPart * All.TreeDomainUpdateFrequency;
      domain_Decomposition();
      compute_potential();
#endif

      /*crjs New function to calc cooling/heating rates for everything*/
#ifdef CHEMCOOL
      updatecooling();
#endif
      savepositions(All.SnapshotFileCount++);	/* write snapshot file */
#if defined(TREE_RAD) && defined(TREE_RAD_DEBUG)
      sprintf(buf, "%s", "tree-rad-debug-file");
      for (n = 0; n < N_gas; n++) {
        if (P[n].ID == TREE_RAD_DEBUG_ID) {
	  if (!(FdTree = fopen(buf, "w")))
	    {
              printf("error in opening file '%s'\n", buf);
              endrun(1);
	    }
	  for (k = 0; k < NPIX; k++) {
            GET_ANGULAR_COORDS(&k, &theta, &phi); 
	    fprintf(FdTree, "%d %g %g %g\n", k, theta, phi, SphP[n].Projection[k]);
	  }
	  fclose(FdTree);
	  break;
	}
      }
#endif
#if defined(TREE_RAD) && defined(HEALPIX_DUMP) 
      for (n = 0; n < N_gas; n++) {
        if ( P[n].ID == 10 || P[n].ID == 1624343 || P[n].ID == 7550) {
          if ( P[n].ID == 10 ) sprintf(buf, "%s", "healpix_info_particle_0010.dat");
          if ( P[n].ID == 1624343 ) sprintf(buf, "%s", "healpix_info_particle_1624343.dat");
          if ( P[n].ID == 7550 ) sprintf(buf, "%s", "healpix_info_particle_7550.dat");
          if (!(FdTree = fopen(buf, "w")))
            {
              printf("error in opening file '%s'\n", buf);
              endrun(1);
            }
          for (k = 0; k < NPIX; k++) 
            fprintf(FdTree, " %g \n", SphP[n].Projection[k]);
          fclose(FdTree);    
        }
      }
#endif

#ifdef CHEMCOOL
      All.NeedAbundancesForOutput = 0;
#endif

      All.Ti_nextoutput = find_next_outputtime(All.Ti_nextoutput + 1);
#ifdef CHEMCOOL
      All.Ti_nextnextoutput = find_next_outputtime(All.Ti_nextoutput + 1);
#endif
    }

  move_particles(All.Ti_Current, min_glob);

  All.Ti_Current = min_glob;

  if(All.ComovingIntegrationOn)
    All.Time = All.TimeBegin * exp(All.Ti_Current * All.Timebase_interval);
  else
    All.Time = All.TimeBegin + All.Ti_Current * All.Timebase_interval;

  All.TimeStep = All.Time - timeold;
}



/*! this function returns the next output time that is equal or larger to
 *  ti_curr
 */
int find_next_outputtime(int ti_curr)
{
  int i, ti, ti_next, iter = 0;
  double next, time;

  ti_next = -1;

  if(All.OutputListOn)
    {
      for(i = 0; i < All.OutputListLength; i++)
	{
	  time = All.OutputListTimes[i];

	  if(time >= All.TimeBegin && time <= All.TimeMax)
	    {
	      if(All.ComovingIntegrationOn)
		ti = log(time / All.TimeBegin) / All.Timebase_interval;
	      else
		ti = (time - All.TimeBegin) / All.Timebase_interval;

	      if(ti >= ti_curr)
		{
		  if(ti_next == -1)
		    ti_next = ti;

		  if(ti_next > ti)
		    ti_next = ti;
		}
	    }
	}
    }
  else
    {
      if(All.ComovingIntegrationOn)
	{
	  if(All.TimeBetSnapshot <= 1.0)
	    {
	      printf("TimeBetSnapshot > 1.0 required for your simulation.\n");
	      endrun(13123);
	    }
	}
      else
	{
	  if(All.TimeBetSnapshot <= 0.0)
	    {
	      printf("TimeBetSnapshot > 0.0 required for your simulation.\n");
	      endrun(13123);
	    }
	}

      time = All.TimeOfFirstSnapshot;

      iter = 0;

      while(time < All.TimeBegin)
	{
	  if(All.ComovingIntegrationOn)
	    time *= All.TimeBetSnapshot;
	  else
	    time += All.TimeBetSnapshot;

	  iter++;

	  if(iter > 100000000)
	    {
	      printf("Can't determine next output time.\n");
	      endrun(110);
	    }
	}

      while(time <= All.TimeMax)
	{
	  if(All.ComovingIntegrationOn)
	    ti = log(time / All.TimeBegin) / All.Timebase_interval;
	  else
	    ti = (time - All.TimeBegin) / All.Timebase_interval;

	  if(ti >= ti_curr)
	    {
	      ti_next = ti;
	      break;
	    }

	  if(All.ComovingIntegrationOn)
	    time *= All.TimeBetSnapshot;
	  else
	    time += All.TimeBetSnapshot;

	  iter++;

	  if(iter > 100000000)
	    {
	      printf("Can't determine next output time.\n");
	      endrun(111);
	    }
	}
    }

  if(ti_next == -1)
    {
      ti_next = 2 * TIMEBASE;	/* this will prevent any further output */

      if(ThisTask == 0)
	printf("\nThere is no valid time for a further snapshot file.\n");
    }
  else
    {
      if(All.ComovingIntegrationOn)
	next = All.TimeBegin * exp(ti_next * All.Timebase_interval);
      else
	next = All.TimeBegin + ti_next * All.Timebase_interval;

      if(ThisTask == 0) {
	printf("\nSetting next time for snapshot file to Time_next= %g\n\n", next);
        printf("\nInternal output time ti_next = %d\n", ti_next);
      }
    }

  return ti_next;
}




/*! This routine writes one line for every timestep to two log-files.  In
 *  FdInfo, we just list the timesteps that have been done, while in FdCPU the
 *  cumulative cpu-time consumption in various parts of the code is stored.
 */
void every_timestep_stuff(void)
{
  double z;

  if(ThisTask == 0)
    {
      if(All.ComovingIntegrationOn)
	{
	  z = 1.0 / (All.Time) - 1;
	  fprintf(FdInfo, "\nBegin Step %d, Time: %g, Redshift: %g, Systemstep: %g, Dloga: %g\n",
		  All.NumCurrentTiStep, All.Time, z, All.TimeStep,
		  log(All.Time) - log(All.Time - All.TimeStep));
	  printf("\nBegin Step %d, Time: %g, Redshift: %g, Systemstep: %g, Dloga: %g\n", All.NumCurrentTiStep,
		 All.Time, z, All.TimeStep, log(All.Time) - log(All.Time - All.TimeStep));
	  fflush(FdInfo);
	}
      else
	{
	  fprintf(FdInfo, "\nBegin Step %d, Time: %g, Systemstep: %g\n", All.NumCurrentTiStep, All.Time,
		  All.TimeStep);
	  printf("\nBegin Step %d, Time: %g, Systemstep: %g\n", All.NumCurrentTiStep, All.Time, All.TimeStep);
	  fflush(FdInfo);
	}

#ifdef XXLINFO
      if(Flag_FullStep == 1)
	{
	  fprintf(FdXXL, "%d %g ", All.NumCurrentTiStep, All.Time);
#ifdef MAGNETIC
	  fprintf(FdXXL, "%e ", MeanB);
#ifdef TRACEDIVB
	  fprintf(FdXXL, "%e ", MaxDivB);
#endif
#endif
#ifdef TIME_DEP_ART_VISC
	  fprintf(FdXXL, "%f ", MeanAlpha);
#endif
	  fprintf(FdXXL, "\n");
	  fflush(FdXXL);
	}
#endif

      fprintf(FdCPU, "Step %d, Time: %g, CPUs: %d\n", All.NumCurrentTiStep, All.Time, NTask);
      fprintf(FdCPU,
             "%10.2f %10.2f %10.2f %10.2f %10.2f %10.2f %10.2f %10.2f %10.2f %10.2f %10.2f %10.2f %10.2f %10.2f %10.2f %10.2f %10.2f %10.2f %10.2f %10.2f %10.2f %10.2f %10.2f %10.2f %10.2f %10.2f ",
             All.CPU_Total, All.CPU_Gravity, All.CPU_Hydro, All.CPU_Domain, All.CPU_Potential,
             All.CPU_Predict, All.CPU_TimeLine, All.CPU_Snapshot, All.CPU_TreeWalk, All.CPU_TreeConstruction,
             All.CPU_CommSum, All.CPU_Imbalance, All.CPU_HydCompWalk, All.CPU_HydCommSumm,
	     All.CPU_HydImbalance, All.CPU_EnsureNgb, All.CPU_PM, All.CPU_Peano, All.CPU_Sinks,
	      All.CPU_SinkCreate, All.CPU_SinkAccrete, All.CPU_SinkCreateComm, All.CPU_SinkAccreteComm,
	      All.CPU_SinktryCOM, All.CPU_SinktryNEIGH, All.CPU_Sinkfindtotal);
#ifdef TURBULENCE
      fprintf(FdCPU, "%10.2f ", All.CPU_Turbulence);
#endif
#ifdef CHEMCOOL
      fprintf(FdCPU, "%10.2f ", All.CPU_Chemcool);
#ifdef RAYTRACE
      fprintf(FdCPU, "%10.2f ", All.CPU_Raytrace);
#endif /* RAYTRACE */
#endif /* CHEMCOOL */
#ifdef TREE_RAD
      fprintf(FdCPU, "%10.2f ", All.CPU_ProjectColumn);
#endif
      fprintf(FdCPU, "\n");
      fflush(FdCPU);
    }

  set_random_numbers();
}


/*! This routine first calls a computation of various global quantities of the
 *  particle distribution, and then writes some statistics about the energies
 *  in the various particle components to the file FdEnergy.
 */
void energy_statistics(void)
{
  int i;

  compute_global_quantities_of_system();

  if(ThisTask == 0)
    {
      fprintf(FdEnergy,
	      "%g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g ",
	      All.Time, SysState.EnergyInt, SysState.EnergyPot, SysState.EnergyKin, SysState.EnergyIntComp[0],
	      SysState.EnergyPotComp[0], SysState.EnergyKinComp[0], SysState.EnergyIntComp[1],
	      SysState.EnergyPotComp[1], SysState.EnergyKinComp[1], SysState.EnergyIntComp[2],
	      SysState.EnergyPotComp[2], SysState.EnergyKinComp[2], SysState.EnergyIntComp[3],
	      SysState.EnergyPotComp[3], SysState.EnergyKinComp[3], SysState.EnergyIntComp[4],
	      SysState.EnergyPotComp[4], SysState.EnergyKinComp[4], SysState.EnergyIntComp[5],
	      SysState.EnergyPotComp[5], SysState.EnergyKinComp[5], SysState.MassComp[0],
	      SysState.MassComp[1], SysState.MassComp[2], SysState.MassComp[3], SysState.MassComp[4],
	      SysState.MassComp[5]);
#ifdef CHEMCOOL
      for (i=0; i < TRAC_NUM; i++) {
        fprintf(FdEnergy, "%g ", SysState.MolAbund[i]);
      }
#endif
      fprintf(FdEnergy, "\n");
      fflush(FdEnergy);
    }
}

void get_max_rcloud(void)
{
   int i;
   double radius, rmaxlocal;

   rmaxlocal = 0.;
   for (i=0; i < N_gas; i++) {
     if (P[i].Type != 0 || P[i].ID < 0)
       continue;
       radius = P[i].Pos[0]*P[i].Pos[0] + P[i].Pos[1]*P[i].Pos[1] + P[i].Pos[2]*P[i].Pos[2];
       if ( radius > rmaxlocal )
	  rmaxlocal = radius;
     }
     MPI_Allreduce(&rmaxlocal,&All.RMaxCloud, 1, MPI_DOUBLE, MPI_MAX, MPI_COMM_WORLD);
     All.RMaxCloud = sqrt(All.RMaxCloud);
}

