#ifdef GALACTIC_POTENTIAL
#define  PI               3.14159265358979323846

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <mpi.h>
#include <unistd.h>
#include <limits.h>

#include "allvars.h"
#include "proto.h"


/*  
   pcc - 17.04.2010
   Time-dependent spiral potential used by Clare Dobbs.
*/
     
void galactic_potential(void)
{
   int i;
   double xi, yi, zi, hi, dhi, d2, r;
   double rc2, p1, zq, Co, Rc;
   double halox, haloy, haloz;
   double potent1, potent2;
   
   zq = 0.7;
   rc2 = 2.019e21 / All.UnitLength_in_cm;
   p1 = 6.19e-31 * pow(All.UnitLength_in_cm, 3) / All.UnitMass_in_g;
   Co = 2.31e14 * pow(All.UnitTime_in_s, 2) / pow(All.UnitLength_in_cm, 2);
   Rc = 3.09398e20 / All.UnitLength_in_cm;

   if (ThisTask == 0) {
      printf("Adding gravitational contributions from the galactic potential \n");
   }

   /* Loop around particles and get accels... */

   for (i=0; i < N_gas; i++) {
      if ( P[i].Ti_endstep == All.Ti_Current ) {   
         if (P[i].Type != 0 || P[i].ID < 0) continue;   
         xi = P[i].Pos[0];
         yi = P[i].Pos[1];
         zi = P[i].Pos[2];
         hi = SphP[i].Hsml;
         dhi = hi/1000.;
         d2 = (xi*xi + yi*yi);
         r = sqrt(d2 + zi*zi);
   
         /* First, contribution from logarithmic potential and halo 
            Expression comes from Binney & Tremaine (1987)
         */
   
         halox = -2.*Co*xi/(Rc*Rc + d2 + pow(zi/zq, 2)) - p1*pow(rc2/r, 2)*(r-rc2*atan(r/rc2))*xi/r;
         haloy = -2.*Co*yi/(Rc*Rc + d2 + pow(zi/zq, 2)) - p1*pow(rc2/r, 2)*(r-rc2*atan(r/rc2))*yi/r;
         haloz = -2.*Co*zi/((Rc*Rc + d2 + pow(zi/zq, 2))*zq*zq);
         
         if (P[i].ID == 1)  printf("Particle 1: Accels before halo %g %g %g \n", P[i].GravAccel[0], P[i].GravAccel[1] ,P[i].GravAccel[2]);
         P[i].GravAccel[0] += halox;
         P[i].GravAccel[1] += haloy;
         P[i].GravAccel[2] += haloz;
         if (P[i].ID == 1)   printf("Particle 1: Accels after halo %g %g %g \n", P[i].GravAccel[0], P[i].GravAccel[1] ,P[i].GravAccel[2]);

         /* Now add the contributions from the time-dependent spiral 
            given by Cox & Gomez (2002)
         */ 
   
         potent1 = spiral_potential_calc(xi, yi, zi, All.Time);
         
         potent2 = spiral_potential_calc(xi+dhi, yi, zi, All.Time);
         P[i].GravAccel[0] += -(potent2-potent1)/dhi;
         
         potent2 = spiral_potential_calc(xi, yi+dhi, zi, All.Time);
         P[i].GravAccel[1] += -(potent2-potent1)/dhi;
         
         potent2 = spiral_potential_calc(xi, yi, zi+dhi, All.Time);
         P[i].GravAccel[2] += -(potent2-potent1)/dhi;
         if (P[i].ID == 1)   printf("Particle 1: Accels after spiral %g %g %g \n", P[i].GravAccel[0], P[i].GravAccel[1] ,P[i].GravAccel[2]); 
      }
   }
  
   if (ThisTask == 0)
      printf("Done with spiral potential contributions \n");

}

/* 
  The function below adds the spiral contribution. Called several times
  to get gradients.
*/

double spiral_potential_calc(double x, double y, double z, double ti)
{
  double t0;
  double r, phi, gamma, Kn, Bn, Dn, sum;
  double r0, NN, alpha, rS, p0, phir, Hz;
  double Co, Rc, Ro, z0;
  double Cz[3];
  double np1float;
  double spiral;
  int n;
  
  
  /* Set constants */
  
  r0= 2.47518e22 / All.UnitLength_in_cm;
  NN = 4;
  alpha = 0.261799;
  rS = 2.16578e22 /  All.UnitLength_in_cm;
  p0 = 2.12889e-24 * pow(All.UnitLength_in_cm, 3) /  All.UnitMass_in_g;
  phir = 6.3e-16 *  All.UnitTime_in_s;
  Hz = 5.56916e20 /  All.UnitLength_in_cm;

  Co = 2.31e14 * pow(All.UnitTime_in_s, 2) / pow(All.UnitLength_in_cm, 2);
  Rc = 3.09398e20 /  All.UnitLength_in_cm;
  Ro = 3.09398e21 /  All.UnitLength_in_cm;
  z0 = 3.09398e21 /  All.UnitLength_in_cm;
  Cz[0] = 8/(3*PI);
  Cz[1] = 0.5;
  Cz[2] = 8/(15*PI);

  /* Time at initial conditions */
  
  t0 = 3.153e+15 /  All.UnitTime_in_s;

  /* Convert to polar co-ords */
  
  if ( x != 0  &&  y != 0 ) {
    phi = atan(y/x);
    if (x < 0 ) phi += PI;
  }
  else {
    if ( x == 0  &&  y > 0 )  phi = PI/2.;
    if ( x == 0  &&  y < 0 )  phi = -PI/2.;
    if ( y == 0  &&  x > 0 )  phi = 0;
    if ( y == 0  &&  x < 0 )  phi = PI;
  }
  
  r = sqrt(x*x + y*y);
  
  /* Calculate the potential */

  gamma = NN*(phi+phir*(t0+ti)-log(r/r0)/tan(alpha));

  sum=0;
  for (n=0; n<3; n++) {
    np1float = 1 + (float)n;
    Kn = np1float*NN/(r*sin(alpha));
    Bn = Kn*Hz*(1. + 0.4*Kn*Hz);
    Dn = ( 1. + Kn*Hz+0.3*pow(Kn*Hz, 2) )/(1.+0.3*Kn*Hz);

    sum += (Cz[n]/(Dn*Kn))*cos(np1float*gamma)*pow(1./cosh((Kn*z)/Bn), Bn);
  }  
  
  spiral = -4.*PI*GRAVITY*Hz*p0*exp(-(r-r0)/rS)*sum;
  return spiral;
}


#endif
