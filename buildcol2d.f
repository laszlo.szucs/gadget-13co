	subroutine buildcol2d
	
	!
	! This subroutine generates a table of vaules for the 
	! kernal, when using the 2D grid. The kernal for each
	! value of r/h is integrated along the z axis, and
	! normalised appropriately.
	!
	! This is just a copy of the subroutine by Matthew Bate
	!
	! To actually get the kernal value, use the function:
	!
	!    	kernal2d(r/h)
	!
	
	implicit none
	
	integer i, j
	double precision r, dist, step
	double precision ypos
	double precision v, v2, v2m
	double precision val
	double precision coldens
	real pi
	double precision coltable(1000)
	
	common / table / coltable
	
        pi = 3.14159265

        do i=1,1000
           r=(i-1)/500.
           dist=sqrt(4.0-r*r)
           step=dist/4000.0
           ypos=0.
         
           coldens=0.0
           do j=1,4000
              v=sqrt(r*r+ypos*ypos)
              if (v.LT.1.0) then
                 v2=v*v
                 val=1.0-1.5*v2+0.75*v2*v
                 coldens=coldens+val*step
              else
                 v2m=2.0-v
                 val=0.25*v2m*v2m*v2m
                 coldens=coldens+val*step        
              end if
              ypos=ypos+step
           end do
           coltable(i)=2.0*coldens/pi
	   if ( coltable(i).lt.0 ) then
	      write(*,*)  "coltable < 0 !!!", coltable(i)
  	      stop
 	   end if 
        end do
	end subroutine buildcol2d
