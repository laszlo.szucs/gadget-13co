## Steps for compiling the code (on Hydra)

1) Load the necessary modules/libraries:

    module load mkl
    module load gsl
    module load fftw
    module load hdf5-mpi
    module load intel
    module load mpi.ibm

2) Compile the code

    make -f Makefile_NL99
