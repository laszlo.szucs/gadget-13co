/*
 *      blumenthal.h
 * 
 *      Author: Christopher Dietl
 * 		Date:	04/03/2009
 */



// Constants for getSign(double x)
#define NEG -1
#define POS +1

// interval width at which the bisection method stops
#define EPSILON_BISEC (1E8)

// indices for the mass profile arrays
#define RADIUS 0
#define MASS   1

// Newtin Iteration is not used. These were values from tests
// relative interval width at which the newton iteration stops
#define EPSILON_NEWTON (1E-10)
// maximum iteration number after which the iteration is cancelled
#define MAX_NEWTON_ITER 50

// interface for outside code
void BlumenthalContraction(double initialRadius[],
                           double initialBaryonMassProfile[],
                           double initialDMMassProfile[],
                           double finalBaryonRadius[],
                           double finalBaryonMass[],
                           double radiusFactor,
                           int sizeInitial,
                           int sizeFinal,
                           double finalDMRadius[],
                           double finalDMMass[]);
