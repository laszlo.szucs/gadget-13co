#ifdef CHEMCOOL

#include <stdio.h>
#include <math.h>
#include "allvars.h"
#include "proto.h"

void updatecooling(void)
{

  /*This function calls get cool time to ensure that the up-to-date
  *cooling rates are wrote out. Important for accretion luminosity close to 
  *central sink*/
  int i,iupcount;
  double dummy,distance;
  double radius, gaccr;
  double t2, t3;

  t2 = second();

  iupcount=0;

  for (i=0;i<N_gas;i++){


	iupcount++;

	radius = sqrt(P[i].Pos[0] * P[i].Pos[0] + P[i].Pos[1] * P[i].Pos[1] + P[i].Pos[2] * P[i].Pos[2]);
	gaccr = fabs(P[i].GravAccel[0]*P[i].Pos[0] + P[i].GravAccel[1]*P[i].Pos[1] + P[i].GravAccel[2]*P[i].Pos[2]);

	COOLI.index_current = i;

	dummy = GetCoolTime(&SphP[i], radius, gaccr);
#ifdef THERMAL_INFO_DUMP
	SphP[i].H2LineCool = THERMALINFO.cool_h2_line;
        SphP[i].H2FormHeat = THERMALINFO.heat_h2_form;
        SphP[i].AccHeat = THERMALINFO.heat_accr;
        SphP[i].H2PdissHeat = THERMALINFO.heat_h2_pdiss;
        SphP[i].H2PumpHeat  = THERMALINFO.heat_h2_pump;
        SphP[i].CIICool = THERMALINFO.cool_cplus;
        SphP[i].OxyCool = THERMALINFO.cool_oxy;
        SphP[i].COCool = THERMALINFO.cool_co;
        SphP[i].i13COCool = THERMALINFO.cool_13co;
        SphP[i].i12COCool = THERMALINFO.cool_12co;
        SphP[i].CICool = THERMALINFO.cool_c;
        SphP[i].CosmicRayHeat = THERMALINFO.heat_cray;
        SphP[i].PhotoElecHeat = THERMALINFO.heat_photo_elec;
        SphP[i].GasDustCool = THERMALINFO.cool_gas_dust;
#endif
        SphP[i].DustTempOut = COOLR.tdust;

   }

  printf("No. of cooling rates updated, %d \n",iupcount);
  fflush(stdout);

  t3 = second();
  All.CPU_Chemcool += timediff(t2,t3);
}



#endif
