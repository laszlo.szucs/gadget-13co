#if defined TREE_RAD || defined TREE_RAD_H2
#define NPIX  12*NSIDE*NSIDE

 subroutine project_column(column, columnH2, theta, phi, angular_radius, projection, projectionH2)
 use healpix
 real*8, intent(inout) :: column, columnH2, theta, phi, angular_radius
 real*8, dimension(0:NPIX-1), intent(inout) :: projection
 real*8, dimension(0:NPIX-1), intent(inout) :: projectionH2
 
 real*8, dimension(3) :: center
 integer, dimension(0:NPIX-1) :: listpix
 integer :: ipixel, I, nlist

 call ang2vec(theta, phi, center)
 call ang2pix_ring(NSIDE, theta, phi, ipixel)
! Last two arguments set number scheme (ring=0, nested=1) and inclusive option
! (off if 0, on if 1); the latter determines whether to include pixels if
! they're overlapped only slightly (inclusive=1), or only if their center is
! within the disc (inclusive=0)
 call query_disc(NSIDE, center, angular_radius, listpix, nlist, 0, 0)

 if (nlist .eq. 1) then
#ifdef TREE_RAD
   projection(ipixel)   = projection(ipixel) + column
#endif
#ifdef TREE_RAD_H2
   projectionH2(ipixel) = projectionH2(ipixel) + columnH2
#endif
 else
   do I = 0, nlist-1
     index = listpix(I)
#ifdef TREE_RAD
     projection(index) = projection(index) + column
#endif
#ifdef TREE_RAD_H2
   projectionH2(ipixel) = projectionH2(ipixel) + columnH2
#endif
   enddo
 endif

 return
 end subroutine project_column 
 
 subroutine get_angular_coords(ipix, theta, phi)
 use healpix
 integer, intent(inout) :: ipix
 real*8, intent(inout) :: theta, phi

 call pix2ang_ring(NSIDE, ipix, theta, phi)

 return
 end subroutine get_angular_coords

 subroutine get_pixels_for_xyz_axes(pixels)
 use healpix
 integer, dimension(6), intent(inout) :: pixels

 real*8, dimension(3) :: vector
 integer :: I, ipring

 do I = 1, 6
   vector(1) = 0.0
   vector(2) = 0.0
   vector(3) = 0.0
   select case (I)
   case (1)
     vector(1) = 1.0
   case (2)
     vector(1) = -1.0
   case (3)
     vector(2) = 1.0
   case (4)
     vector(2) = -1.0
   case (5)
     vector(3) = 1.0
   case (6)
     vector(3) = -1.0
   end select
   call vec2pix_ring(NSIDE, vector, ipring)
   pixels(I) = ipring
 enddo

 return
 end subroutine get_pixels_for_xyz_axes
#endif /* TREE_RAD || TREE_RAD_H2 */
