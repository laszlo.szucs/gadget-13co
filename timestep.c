
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <mpi.h>
#include "allvars.h"
#include "proto.h"

/*! \file timestep.c 
 *  \brief routines for 'kicking' particles in momentum space and assigning new timesteps
 */


#ifdef PERIODIC
static double boxSize, boxHalf;

#ifdef LONG_X
static double boxSize_X, boxHalf_X;
#else
#define boxSize_X boxSize
#define boxHalf_X boxHalf
#endif
#ifdef LONG_Y
static double boxSize_Y, boxHalf_Y;
#else
#define boxSize_Y boxSize
#define boxHalf_Y boxHalf
#endif
#ifdef LONG_Z
static double boxSize_Z, boxHalf_Z;
#else
#define boxSize_Z boxSize
#define boxHalf_Z boxHalf
#endif
#endif

#define NGB_PERIODIC_X(x) (xtmp=(x),(xtmp>boxHalf_X)?(xtmp-boxSize_X):((xtmp<-boxHalf_X)?(xtmp+boxSize_X):xtmp))
#define NGB_PERIODIC_Y(x) (xtmp=(x),(xtmp>boxHalf_Y)?(xtmp-boxSize_Y):((xtmp<-boxHalf_Y)?(xtmp+boxSize_Y):xtmp))
#define NGB_PERIODIC_Z(x) (xtmp=(x),(xtmp>boxHalf_Z)?(xtmp-boxSize_Z):((xtmp<-boxHalf_Z)?(xtmp+boxSize_Z):xtmp))


static double fac1, fac2, fac3, hubble_a, atime, a3inv;
static double dt_displacement = 0;
static long n_displace, n_max, n_accel, n_courant, n_synchro;
#ifdef CHEMCOOL
static long n_cooling, n_entr, n_output;
#endif

/*! This function advances the system in momentum space, i.e. it does apply
 *  the 'kick' operation after the forces have been computed. Additionally, it
 *  assigns new timesteps to particles. At start-up, a half-timestep is
 *  carried out, as well as at the end of the simulation. In between, the
 *  half-step kick that ends the previous timestep and the half-step kick for
 *  the new timestep are combined into one operation.
 */
void advance_and_find_timesteps(void)
{
  int i, j, no, ti_step, ti_min, tend, tstart;
#ifdef SYNC_HACK
  int test_ti_step;
#endif
  double dt_entr, dt_entr2, dt_gravkick, dt_hydrokick, dt_gravkick2, dt_hydrokick2, t0, t1;
  double t2, t3;
  double aphys;
#if !defined(CHEMCOOL) && !defined(POLYTROPE)
  double minentropy;
#endif
  FLOAT dv[3];

#ifdef FLEXSTEPS
  int ti_grp;
#endif
#if defined(PSEUDOSYMMETRIC) && !defined(FLEXSTEPS)
  double apred, prob;
  int ti_step2;
#endif
#ifdef PMGRID
  double dt_gravkickA, dt_gravkickB;
#endif
#ifdef MAKEGLASS
  double disp, dispmax, globmax, dmean, fac, disp2sum, globdisp2sum;
#endif
#ifdef CHEMCOOL
  double old_entropy, dt_entr_cc, dummy;
  double energy, rho, yn, abh2, temp, ekn;
#ifdef SEMI_IMPLICIT
  int tbegstep, tendstep;
  double entropy_at_tend;
#endif
#endif
#if defined(TIME_DEP_ART_VISC) || defined(TIME_DEP_MAGN_DISP) || defined(TIME_DEP_EULER_DISSIPATION)
  double dmin1, dmin2;
#endif
  int last_step;
  double radius, gaccr;

  int counters[8], sumcounters[8];
  int id_of_particle_with_mindt, index_of_particle_with_mindt;

  t0 = second();

  if(All.ComovingIntegrationOn)
    {
      fac1 = 1 / (All.Time * All.Time);
#ifndef CHEMCOOL
      fac2 = 1 / pow(All.Time, 3 * GAMMA - 2);
      fac3 = pow(All.Time, 3 * (1 - GAMMA) / 2.0);
#endif
      hubble_a = All.Omega0 / (All.Time * All.Time * All.Time)
	+ (1 - All.Omega0 - All.OmegaLambda) / (All.Time * All.Time) + All.OmegaLambda;

      hubble_a = All.Hubble * sqrt(hubble_a);
      a3inv = 1 / (All.Time * All.Time * All.Time);
      atime = All.Time;
    }
  else
    fac1 = fac2 = fac3 = hubble_a = a3inv = atime = 1;

#ifdef NOPMSTEPADJUSTMENT
  dt_displacement = All.MaxSizeTimestep;
#else
  if(Flag_FullStep || dt_displacement == 0)
    find_dt_displacement_constraint(hubble_a * atime * atime);
#endif

#ifdef PMGRID
  if(All.ComovingIntegrationOn)
    dt_gravkickB = get_gravkick_factor(All.PM_Ti_begstep, All.Ti_Current) -
      get_gravkick_factor(All.PM_Ti_begstep, (All.PM_Ti_begstep + All.PM_Ti_endstep) / 2);
  else
    dt_gravkickB = (All.Ti_Current - (All.PM_Ti_begstep + All.PM_Ti_endstep) / 2) * All.Timebase_interval;

  if(All.PM_Ti_endstep == All.Ti_Current)	/* need to do long-range kick */
    {
      /* make sure that we reconstruct the domain/tree next time because we don't kick the tree nodes in this case */
      All.NumForcesSinceLastDomainDecomp = 1 + All.TotNumPart * All.TreeDomainUpdateFrequency;
    }
#endif


#ifdef MAKEGLASS
  for(i = 0, dispmax = 0, disp2sum = 0; i < NumPart; i++)
    {
      if(P[i].Type == 0 && P[i].ID < 0) /*SINK*/
	continue;
      for(j = 0; j < 3; j++)
	{
	  P[i].GravPM[j] *= -1;
	  P[i].GravAccel[j] *= -1;
	  P[i].GravAccel[j] += P[i].GravPM[j];
	  P[i].GravPM[j] = 0;
	}

      disp = sqrt(P[i].GravAccel[0] * P[i].GravAccel[0] +
		  P[i].GravAccel[1] * P[i].GravAccel[1] + P[i].GravAccel[2] * P[i].GravAccel[2]);

      disp *= 2.0 / (3 * All.Hubble * All.Hubble);

      disp2sum += disp * disp;

      if(disp > dispmax)
	dispmax = disp;
    }

  MPI_Allreduce(&dispmax, &globmax, 1, MPI_DOUBLE, MPI_MAX, MPI_COMM_WORLD);
  MPI_Allreduce(&disp2sum, &globdisp2sum, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);

  dmean = pow(P[0].Mass / (All.Omega0 * 3 * All.Hubble * All.Hubble / (8 * M_PI * All.G)), 1.0 / 3);

  if(globmax > dmean)
    fac = dmean / globmax;
  else
    fac = 1.0;

  if(ThisTask == 0)
    {
      printf("\nglass-making:  dmean= %g  global disp-maximum= %g  rms= %g\n\n",
	     dmean, globmax, sqrt(globdisp2sum / All.TotNumPart));
      fflush(stdout);
    }

  for(i = 0, dispmax = 0; i < NumPart; i++)
    {
      if(P[i].Type == 0 && P[i].ID < 0) /*SINK*/
	continue;
      for(j = 0; j < 3; j++)
	{
	  P[i].Vel[j] = 0;
	  P[i].Pos[j] += fac * P[i].GravAccel[j] * 2.0 / (3 * All.Hubble * All.Hubble);
	  P[i].GravAccel[j] = 0;
	}
    }
#endif

  /* Update column densities before doing chemistry update below.
   * NB. We only need to do this when using the raytracing approx.
   */
 #if defined RAYTRACE && defined CHEMCOOL
/*  t2 = second();

  if (All.PhotochemApprox == 2) {
    raytrace();
  }

  t3 = second();
  All.CPU_Raytrace += timediff(t2, t3);
*/
#endif /* RAYTRACE && CHEMCOOL */

  /* Now assign new timesteps and kick */

  if((All.Ti_Current % (4 * All.PresentMinStep)) == 0)
    if(All.PresentMinStep < TIMEBASE)
      All.PresentMinStep *= 2;

  index_of_particle_with_mindt = -1;
  id_of_particle_with_mindt    = -1;
  for(i = 0; i < NumPart; i++)
    {
      if(P[i].ID < 0) /*SINK*/
	continue;
      if(P[i].Ti_endstep == All.Ti_Current)
	{
#ifdef HISTORY
          if (SphP[i].history_output_required) {
            dump_history(i);
	  }
#endif
#ifdef CACHE_DTCOOL
	  /* Invalidate cache */
          SphP[i].CoolingTime = -1;
#endif
	  ti_step = get_timestep(i, &aphys, 0, 0);
	  /* make it a power 2 subdivision */
	  ti_min = TIMEBASE;
	  while(ti_min > ti_step)
	    ti_min >>= 1;
	  ti_step = ti_min;

	  if(ti_step < All.PresentMinStep) {
	    All.PresentMinStep = ti_step;
	    id_of_particle_with_mindt = P[i].ID;
            index_of_particle_with_mindt = i;
	  }
       }
    }

  /*  printf("Processor %d, particle ID %d, particle index %d, Stepsize %d -- %g\n", ThisTask, id_of_particle_with_mindt, index_of_particle_with_mindt,
      All.PresentMinStep, All.PresentMinStep * All.Timebase_interval); */
  if (index_of_particle_with_mindt >= 0) {
    /* Non-verbose output for particle on the minimum timestep */
    ti_step = get_timestep(index_of_particle_with_mindt, &aphys, 0, 0);
  }

  ti_step = All.PresentMinStep;
  MPI_Allreduce(&ti_step, &All.PresentMinStep, 1, MPI_INT, MPI_MIN, MPI_COMM_WORLD);

  if(dt_displacement < All.MaxSizeTimestep)
    ti_step = (int) (dt_displacement / All.Timebase_interval);
  else
    ti_step = (int) (All.MaxSizeTimestep / All.Timebase_interval);

  /* make it a power 2 subdivision */
  ti_min = TIMEBASE;
  while(ti_min > ti_step)
    ti_min >>= 1;
  All.PresentMaxStep = ti_min;


  if(ThisTask == 0)
    printf("Syn Range = %g  PresentMinStep = %d  PresentMaxStep = %d \n",
	   (double) All.PresentMaxStep / All.PresentMinStep, All.PresentMinStep, All.PresentMaxStep);

  /* Initialize timestep diagnostic counters */
  n_displace = n_max = n_accel = n_courant = n_synchro = 0;
#ifdef CHEMCOOL
  n_cooling = n_entr = n_output = 0;
#endif

  for(i = 0; i < NumPart; i++)
    {
      if(P[i].Type == 0 && P[i].ID < 0) /*SINK*/
	continue;
      if(P[i].Ti_endstep == All.Ti_Current)
	{
          if(P[i].Type == 5) /*SINK*/
	    ti_step = All.PresentMinStep;
	  else
  	    ti_step = get_timestep(i, &aphys, 0, 0);

	  /* make it a power 2 subdivision */
	  ti_min = TIMEBASE;
	  while(ti_min > ti_step)
	    ti_min >>= 1;
	  ti_step = ti_min;

#ifdef FLEXSTEPS
	  ti_grp = P[i].FlexStepGrp % All.PresentMaxStep;
	  ti_grp = (ti_grp / All.PresentMinStep) * All.PresentMinStep;
	  ti_step = ((P[i].Ti_endstep + ti_grp + ti_step) / ti_step) * ti_step - (P[i].Ti_endstep + ti_grp);
#else

#ifdef PSEUDOSYMMETRIC
	  if(P[i].Type != 0)
	    {
	      if(P[i].Ti_endstep > P[i].Ti_begstep)
		{
		  apred = aphys + ((aphys - P[i].AphysOld) / (P[i].Ti_endstep - P[i].Ti_begstep)) * ti_step;
		  if(fabs(apred - aphys) < 0.5 * aphys)
		    {
		      ti_step2 = get_timestep(i, &apred, -1, 0);
		      ti_min = TIMEBASE;
		      while(ti_min > ti_step2)
			ti_min >>= 1;
		      ti_step2 = ti_min;

		      if(ti_step2 < ti_step)
			{
			  get_timestep(i, &apred, ti_step, 0);
			  prob =
			    ((apred - aphys) / (aphys - P[i].AphysOld) * (P[i].Ti_endstep -
									  P[i].Ti_begstep)) / ti_step;
			  if(prob < get_random_number(P[i].ID))
			    ti_step /= 2;
			}
		      else if(ti_step2 > ti_step)
			{
			  get_timestep(i, &apred, 2 * ti_step, 0);
			  prob =
			    ((apred - aphys) / (aphys - P[i].AphysOld) * (P[i].Ti_endstep -
									  P[i].Ti_begstep)) / ti_step;
			  if(prob < get_random_number(P[i].ID + 1))
			    ti_step *= 2;
			}
		    }
		}
	      P[i].AphysOld = aphys;
	    }
#endif

#ifdef SYNCHRONIZATION
#ifdef SYNC_HACK
	  if(ti_step > (P[i].Ti_endstep - P[i].Ti_begstep)) {
	    test_ti_step = ti_step;
    	    while(test_ti_step > (P[i].Ti_endstep - P[i].Ti_begstep)
		  && ((TIMEBASE - P[i].Ti_endstep) % test_ti_step) > 0 && P[i].Ti_endstep > P[i].Ti_begstep) {
	      test_ti_step >>= 1;
	    }
	    ti_step = test_ti_step;
	  }
#else
	  if(ti_step > (P[i].Ti_endstep - P[i].Ti_begstep))	/* timestep wants to increase */
	    {
	      if(((TIMEBASE - P[i].Ti_endstep) % ti_step) > 0) {
		ti_step = P[i].Ti_endstep - P[i].Ti_begstep;	/* leave at old step */
		n_synchro++;
	      }
	    }
#endif /* SYNC_HACK */
#endif
#endif /* end of FLEXSTEPS */

	  if(All.Ti_Current == TIMEBASE)	/* we here finish the last timestep. */
	    ti_step = 0;

	  if((TIMEBASE - All.Ti_Current) < ti_step)	/* check that we don't run beyond the end */
	    ti_step = TIMEBASE - All.Ti_Current;

	  tstart = (P[i].Ti_begstep + P[i].Ti_endstep) / 2;	/* midpoint of old step */
	  tend = P[i].Ti_endstep + ti_step / 2;	/* midpoint of new step */

	  if(All.ComovingIntegrationOn)
	    {
	      dt_entr = (tend - tstart) * All.Timebase_interval;
	      dt_entr2 = (tend - P[i].Ti_endstep) * All.Timebase_interval;
	      dt_gravkick = get_gravkick_factor(tstart, tend);
	      dt_hydrokick = get_hydrokick_factor(tstart, tend);
	      dt_gravkick2 = get_gravkick_factor(P[i].Ti_endstep, tend);
	      dt_hydrokick2 = get_hydrokick_factor(P[i].Ti_endstep, tend);
	    }
	  else
	    {
	      dt_entr = dt_gravkick = dt_hydrokick = (tend - tstart) * All.Timebase_interval;
	      dt_gravkick2 = dt_hydrokick2 = dt_entr2 = (tend - P[i].Ti_endstep) * All.Timebase_interval;
	    }

	  P[i].Ti_begstep = P[i].Ti_endstep;
	  P[i].Ti_endstep = P[i].Ti_begstep + ti_step;
#ifdef SEMI_IMPLICIT
          tbegstep = P[i].Ti_begstep * All.Timebase_interval;
          tendstep = P[i].Ti_endstep * All.Timebase_interval;
#endif
	  /* do the kick */

	  for(j = 0; j < 3; j++)
	    {
	      dv[j] = P[i].GravAccel[j] * dt_gravkick;
	      P[i].Vel[j] += dv[j];
	    }

	  if(P[i].Type == 0)	/* SPH stuff */
	    {
	      for(j = 0; j < 3; j++)
		{
		  dv[j] += SphP[i].HydroAccel[j] * dt_hydrokick;
		  P[i].Vel[j] += SphP[i].HydroAccel[j] * dt_hydrokick;

		  SphP[i].VelPred[j] =
		    P[i].Vel[j] - dt_gravkick2 * P[i].GravAccel[j] - dt_hydrokick2 * SphP[i].HydroAccel[j];
#ifdef PMGRID
		  SphP[i].VelPred[j] += P[i].GravPM[j] * dt_gravkickB;
#endif

#ifdef MAGNETIC
#ifndef EULERPOTENTIALS
		  SphP[i].B[j] += SphP[i].DtB[j] * dt_entr;
		  SphP[i].BPred[j] = SphP[i].B[j] - SphP[i].DtB[j] * dt_entr2;
#endif
#endif
		}

#if defined(MAGNETIC) && defined(DIVBCLEANING_DEDNER)
	      SphP[i].Phi += SphP[i].DtPhi * dt_entr;
	      SphP[i].PhiPred = SphP[i].Phi - SphP[i].DtPhi * dt_entr2;
#endif
#ifdef TIME_DEP_ART_VISC
	      SphP[i].alpha += SphP[i].Dtalpha * dt_entr;
	      SphP[i].alpha = DMIN(SphP[i].alpha, All.ArtBulkViscConst);
	      if(SphP[i].alpha < All.AlphaMin)
		SphP[i].alpha = All.AlphaMin;
                //printf("%f\n",SphP[i].alpha);
#endif
#if defined(TIME_DEP_MAGN_DISP) || defined(TIME_DEP_EULER_DISSIPATION)
	      SphP[i].Balpha += SphP[i].DtBalpha * dt_entr;
	      SphP[i].Balpha = DMIN(SphP[i].Balpha, All.ArtMagDispConst);
	      if(SphP[i].Balpha < All.ArtMagDispMin)
		SphP[i].Balpha = All.ArtMagDispMin;
                //printf("%f\n",SphP[i].Balpha);
#endif

#ifdef CHEMCOOL
              t2 = second();
	      old_entropy = SphP[i].Entropy;

#ifdef DEBUG_EVOLVE_3B
	      if (SphP[i].TracAbund[0] < 1.0e-4)
		{
		  printf("Problem with abundance of particle %d \n", P[i].ID);
		}
#endif
              COOLI.id_current = P[i].ID;
	      COOLI.index_current = i;
 	      radius = sqrt(P[i].Pos[0] * P[i].Pos[0] + P[i].Pos[1] * P[i].Pos[1] + P[i].Pos[2] * P[i].Pos[2]);	
              gaccr = fabs(P[i].GravAccel[0]*P[i].Pos[0] + P[i].GravAccel[1]*P[i].Pos[1] + P[i].GravAccel[2]*P[i].Pos[2]);
              gaccr /= radius;
#ifdef DEBUG_SIGMA_PCC
		if ( P[i].ID == 1 ) {
		   printf("radius %g  gaccr %g \n", radius, gaccr);
		}
#endif /* DEBUG_SIGMA_PCC */

		if (ti_step == TIMEBASE - All.Ti_Current) {
		  /* Last step... */
		  last_step = 1;
		}
		else {
		  last_step = 0;
		}

#ifdef SEMI_IMPLICIT
		dt_entr_cc = (tbegstep - tstart) * All.Timebase_interval;
		dummy = do_chemcool_step(dt_entr_cc, &SphP[i], radius,  gaccr, 1);
		SphP[i].Ebeg = SphP[i].Entropy;
		SphP[i].DtTherm = GetCoolTime(&SphP[i], radius, gaccr);

		if (tend > All.Ti_nextoutput) {
		  /* Since the system is currently at time tbegstep, any outputs prior to this time                                               
		   * must already have occurred; hence, we only need to worry about the case where 
		   * the output occurs between tend and tbegstep                                                                                  
		   */
		  dt_entr_cc = (All.Ti_nextoutput - tbegstep) * All.Timebase_interval;
		  dummy = do_chemcool_step(dt_entr_cc, &SphP[i], radius,  gaccr, 1);
		  save_output_chemcool(&SphP[i]);

		  dt_entr_cc = (tend - All.Ti_nextoutput) * All.Timebase_interval;
		  dummy = do_chemcool_step(dt_entr_cc, &SphP[i], radius,  gaccr, 1);
		  entropy_at_tend = SphP[i].Entropy;

		  dt_entr_cc = (tendstep - tend) * All.Timebase_interval;
		  dummy = do_chemcool_step(dt_entr_cc, &SphP[i], radius, gaccr, 3);
		  SphP[i].Efinal  = SphP[i].Entropy;
		  SphP[i].Entropy = entropy_at_tend;
		}
		else {
		  dt_entr_cc = (tend - tbegstep) * All.Timebase_interval;
		  dummy = do_chemcool_step(dt_entr_cc, &SphP[i], radius,  gaccr, 1);
		  entropy_at_tend = SphP[i].Entropy;

		  if (last_step) {
		    for(j = 0; j < TRAC_NUM; j++) {
		      SphP[i].TracAbundOut[j] = SphP[i].TracAbund[j];
		    }
		    SphP[i].EntropyOut = SphP[i].Entropy;
		    SphP[i].GammaOut   = SphP[i].Gamma;
		    SphP[i].DustTempOut = SphP[i].DustTemp;
		  }

		  dt_entr_cc = (tendstep - tend) * All.Timebase_interval;
		  dummy = do_chemcool_step(dt_entr_cc, &SphP[i], radius, gaccr, 3);
		  SphP[i].Efinal  = SphP[i].Entropy;
		  SphP[i].Entropy = entropy_at_tend;
		}
#else
              if (tend > All.Ti_nextoutput) {
		/* We're evolving past an output time, so we need to do this in two parts */
		dt_entr_cc = (All.Ti_nextoutput - tstart) * All.Timebase_interval;
		dummy = do_chemcool_step(dt_entr_cc, &SphP[i], radius,  gaccr, 1);

		/* Save a copy of the abundances and entropy at the output time */
                for(j = 0; j < TRAC_NUM; j++) {
                  SphP[i].TracAbundOut[j] = SphP[i].TracAbund[j];
  	        }
		SphP[i].EntropyOut = SphP[i].Entropy;
                SphP[i].GammaOut   = SphP[i].Gamma;
		/* Compute gas temperature */
		energy = SphP[i].Entropy * pow(SphP[i].Density, SphP[i].Gamma) / (SphP[i].Gamma - 1.0);
                energy *= All.UnitEnergy_in_cgs / pow(All.UnitLength_in_cm, 3);
                rho    = SphP[i].Density * All.UnitDensity_in_cgs;
                yn     = rho / ((1.0 + 4.0 * ABHE) * PROTONMASS);
                abh2   = SphP[i].TracAbundOut[IH2];
                ekn  = energy / (BOLTZMANN * (1.0 + ABHE - abh2) * yn);
                CALC_TEMP(&abh2, &ekn, &temp);
                SphP[i].TempOut = temp;
                SphP[i].DustTempOut = SphP[i].DustTemp;
		/*#ifdef THERMAL_INFO_DUMP
                dummy = GetCoolTime(&SphP[i], radius, gaccr);
                SphP[i].H2LineCool = THERMALINFO.cool_h2_line;
                SphP[i].H2CIECool  = THERMALINFO.cool_h2_cie;
                SphP[i].H2DissCool = THERMALINFO.cool_h2_diss;
                SphP[i].H2FormHeat = THERMALINFO.heat_3b;
                SphP[i].PdVHeat    = THERMALINFO.pdv_heat;
                SphP[i].DtCool     = THERMALINFO.dtcool_nopdv;
	       SphP[i].AccHeat    = THERMALINFO.acc_heat;
	       #endif*/

#ifdef DEBUG_FEEDBACK
	if (P[i].ID==1866844){
	  printf("ID= %d in timestep \n",P[i].ID);
	  printf("Task %d, ID %d, Printed rate %g\n", ThisTask,P[i].ID,SphP[i].AccHeat);
	  fflush(stdout);
	}
	if (P[i].ID==1866845){
	  printf("ID= %d in timestep \n",P[i].ID);
	  printf("Task %d, ID %d, Printed rate %g\n", ThisTask,P[i].ID,SphP[i].AccHeat);
	  fflush(stdout);
	}
#endif

#ifdef ID_TRACE
                if ( P[i].ID == All.TraceID ) {
                   fprintf(FdTrace, "%g %g %g %g %g %g %g %g %g %g %g %g", tend, SphP[i].Density, SphP[i].EntropyOut, SphP[i].TempOut, SphP[i].GammaOut, SphP[i].DivVel, SphP[i].H2LineCool, SphP[i].H2CIECool, SphP[i].H2DissCool, SphP[i].H2FormHeat, SphP[i].PdVHeat, SphP[i].DtCool); 
                }
#endif
		dt_entr_cc = (tend - All.Ti_nextoutput) * All.Timebase_interval;
		dummy = do_chemcool_step(dt_entr_cc, &SphP[i], radius, gaccr, 1);
	      }
	      else {
		dummy = do_chemcool_step(dt_entr, &SphP[i], radius, gaccr, 1);

                if (last_step) {
                  for(j = 0; j < TRAC_NUM; j++) {
                    SphP[i].TracAbundOut[j] = SphP[i].TracAbund[j];
  	          }
		  SphP[i].EntropyOut = SphP[i].Entropy;
                  SphP[i].GammaOut   = SphP[i].Gamma;
                  SphP[i].DustTempOut = SphP[i].DustTemp;
		}
	      }
#endif

#ifdef SEMI_IMPLICIT
             SphP[i].DtEntropy = 0.0;
#else
 	      if (dt_entr > 0) {
  	        SphP[i].DtEntropy = (SphP[i].Entropy - old_entropy) / dt_entr;
	      }
	      else {
		/* Actually, it's undefined, but this ensures that we store _some_ value
		 * here, rather than just leaving whatever junk was there before
		 */
		SphP[i].DtEntropy = 0.0;
	      }
#endif /* SEMI_IMPLICIT */
              t3 = second();
              All.CPU_Chemcool += timediff(t2, t3);
#else /* CHEMCOOL */
#ifndef POLYTROPE
	      /* In case of cooling, we prevent that the entropy (and
	         hence temperature decreases by more than a factor 0.5 */

	      if(SphP[i].DtEntropy * dt_entr > -0.5 * SphP[i].Entropy)
		SphP[i].Entropy += SphP[i].DtEntropy * dt_entr;
	      else
		SphP[i].Entropy *= 0.5;

	      if(All.MinEgySpec)
		{
		  minentropy = All.MinEgySpec * GAMMA_MINUS1 / pow(SphP[i].Density * a3inv, GAMMA_MINUS1);
		  if(SphP[i].Entropy < minentropy)
		    {
		      SphP[i].Entropy = minentropy;
		      SphP[i].DtEntropy = 0;
		    }
		}

	      /* In case the timestep increases in the new step, we
	         make sure that we do not 'overcool' when deriving
	         predicted temperatures. The maximum timespan over
	         which prediction can occur is ti_step/2, i.e. from
	         the middle to the end of the current step */

	      dt_entr = ti_step / 2 * All.Timebase_interval;
	      if(SphP[i].Entropy + SphP[i].DtEntropy * dt_entr < 0.5 * SphP[i].Entropy)
		SphP[i].DtEntropy = -0.5 * SphP[i].Entropy / dt_entr;
#endif /* POLYTROPE */
#endif /* CHEMCOOL */
	    }


	  /* if tree is not going to be reconstructed, kick parent nodes dynamically.
	   */
	  if(All.NumForcesSinceLastDomainDecomp < All.TotNumPart * All.TreeDomainUpdateFrequency)
	    {
	      no = Father[i];
	      while(no >= 0)
		{
		  for(j = 0; j < 3; j++)
		    Extnodes[no].vs[j] += dv[j] * P[i].Mass / Nodes[no].u.d.mass;

		  no = Nodes[no].u.d.father;
		}
	    }
	}
    }



#ifdef PMGRID
  if(All.PM_Ti_endstep == All.Ti_Current)	/* need to do long-range kick */
    {
      ti_step = TIMEBASE;
      while(ti_step > (dt_displacement / All.Timebase_interval))
	ti_step >>= 1;

      if(ti_step > (All.PM_Ti_endstep - All.PM_Ti_begstep))	/* PM-timestep wants to increase */
	{
	  /* we only increase if an integer number of steps will bring us to the end */
	  if(((TIMEBASE - All.PM_Ti_endstep) % ti_step) > 0)
	    ti_step = All.PM_Ti_endstep - All.PM_Ti_begstep;	/* leave at old step */
	}

      if(All.Ti_Current == TIMEBASE)	/* we here finish the last timestep. */
	ti_step = 0;

      tstart = (All.PM_Ti_begstep + All.PM_Ti_endstep) / 2;
      tend = All.PM_Ti_endstep + ti_step / 2;

      if(All.ComovingIntegrationOn)
	dt_gravkick = get_gravkick_factor(tstart, tend);
      else
	dt_gravkick = (tend - tstart) * All.Timebase_interval;

      All.PM_Ti_begstep = All.PM_Ti_endstep;
      All.PM_Ti_endstep = All.PM_Ti_begstep + ti_step;

      if(All.ComovingIntegrationOn)
	dt_gravkickB = -get_gravkick_factor(All.PM_Ti_begstep, (All.PM_Ti_begstep + All.PM_Ti_endstep) / 2);
      else
	dt_gravkickB =
	  -((All.PM_Ti_begstep + All.PM_Ti_endstep) / 2 - All.PM_Ti_begstep) * All.Timebase_interval;

      for(i = 0; i < NumPart; i++)
	{
	  if(P[i].Type == 0 && P[i].ID < 0) /*SINK*/
	    continue;
	  for(j = 0; j < 3; j++)	/* do the kick */
	    P[i].Vel[j] += P[i].GravPM[j] * dt_gravkick;

	  if(P[i].Type == 0)
	    {
	      if(All.ComovingIntegrationOn)
		{
		  dt_gravkickA = get_gravkick_factor(P[i].Ti_begstep, All.Ti_Current) -
		    get_gravkick_factor(P[i].Ti_begstep, (P[i].Ti_begstep + P[i].Ti_endstep) / 2);
		  dt_hydrokick = get_hydrokick_factor(P[i].Ti_begstep, All.Ti_Current) -
		    get_hydrokick_factor(P[i].Ti_begstep, (P[i].Ti_begstep + P[i].Ti_endstep) / 2);
		}
	      else
		dt_gravkickA = dt_hydrokick =
		  (All.Ti_Current - (P[i].Ti_begstep + P[i].Ti_endstep) / 2) * All.Timebase_interval;

	      for(j = 0; j < 3; j++)
		SphP[i].VelPred[j] = P[i].Vel[j]
		  + P[i].GravAccel[j] * dt_gravkickA
		  + SphP[i].HydroAccel[j] * dt_hydrokick + P[i].GravPM[j] * dt_gravkickB;
	    }
	}
    }
#endif

  for (j=0; j<8; j++) {
    counters[j] = 0;
  }
  counters[0] = n_displace;
  counters[1] = n_max;
  counters[2] = n_accel;
  counters[3] = n_courant;
  counters[4] = n_synchro;
#ifdef CHEMCOOL
  counters[5] = n_cooling;
  counters[6] = n_entr;
  counters[7] = n_output;
#endif
  MPI_Reduce(counters, sumcounters, 8, MPI_INT, MPI_SUM, 0, MPI_COMM_WORLD);
  if (ThisTask == 0) {
    fprintf(FdTimings, "Timestep counters: displace = %d, max = %d, accel = %d, courant = %d, synchro = %d\n",
            sumcounters[0], sumcounters[1], sumcounters[2], sumcounters[3], sumcounters[4]);
#ifdef CHEMCOOL
    fprintf(FdTimings, "cooling = %d, entropy = %d, output = %d\n", sumcounters[5], sumcounters[6], sumcounters[7]);
#endif
    fflush(FdTimings);
  }
  t1 = second();
  All.CPU_TimeLine += timediff(t0, t1);
}




/*! This function normally (for flag==0) returns the maximum allowed timestep
 *  of a particle, expressed in terms of the integer mapping that is used to
 *  represent the total simulated timespan. The physical acceleration is
 *  returned in `aphys'. The latter is used in conjunction with the
 *  PSEUDOSYMMETRIC integration option, which also makes of the second
 *  function of get_timestep. When it is called with a finite timestep for
 *  flag, it returns the physical acceleration that would lead to this
 *  timestep, assuming timestep criterion 0.
 */
int get_timestep(int p,		/*!< particle index */
		 double *aphys,	/*!< acceleration (physical units) */
		 int flag,	/*!< either 0 for normal operation, or finite timestep to get corresponding aphys */
                 int verbose)   /*!< 0 for normal operation, 1 to switch on verbose output mode */
{
  double ax, ay, az, ac, csnd;
  double dt = 0, dt_courant = 0, dt_accel = 0;
  double dt_phys;
  int ti_step;
  double dx, dy, dz, r2;
  int isink;

#ifdef CHEMCOOL
  double dt_cooling = -1, dt_entr = -1;
  double dt_output = -1;
  double new_dtcool, old_dtcool;
  int tend, time_after_next;
  double radius, gaccr;
  double energy, yn, T_est;
  int i, ntry;
  double t2, t3;
#endif

#ifdef CONDUCTION
  double dt_cond;
#endif

#ifdef PERIODIC
  double xtmp;
#endif

  if(flag == 0)
    {
      ax = fac1 * P[p].GravAccel[0];
      ay = fac1 * P[p].GravAccel[1];
      az = fac1 * P[p].GravAccel[2];

#ifdef PMGRID
      ax += fac1 * P[p].GravPM[0];
      ay += fac1 * P[p].GravPM[1];
      az += fac1 * P[p].GravPM[2];
#endif

      if(P[p].Type == 0)
	{
#ifdef CHEMCOOL
	  if(All.ComovingIntegrationOn)
            {
               fac2 = 1 / pow(All.Time, 3 * SphP[p].Gamma - 2);
	    }
#endif
	  ax += fac2 * SphP[p].HydroAccel[0];
	  ay += fac2 * SphP[p].HydroAccel[1];
	  az += fac2 * SphP[p].HydroAccel[2];
	}

      ac = sqrt(ax * ax + ay * ay + az * az);	/* this is now the physical acceleration */
      *aphys = ac;
    }
  else
    ac = *aphys;

  if(ac == 0)
    ac = 1.0e-30;

  switch (All.TypeOfTimestepCriterion)
    {
    case 0:
      if(flag > 0)
	{
	  dt = flag * All.Timebase_interval;
	  dt /= hubble_a;	/* convert dloga to physical timestep  */
	  ac = 2 * All.ErrTolIntAccuracy * atime * All.SofteningTable[P[p].Type] / (dt * dt);
	  *aphys = ac;
	  return flag;
	}
      dt = dt_accel = sqrt(2 * All.ErrTolIntAccuracy * atime * All.SofteningTable[P[p].Type] / ac);
#ifdef ADAPTIVE_GRAVSOFT_FORGAS
      if(P[p].Type == 0)
	dt = dt_accel = sqrt(2 * All.ErrTolIntAccuracy * atime * SphP[p].Hsml / 2.8 / ac);
#endif
      if (verbose == 1) {
	printf("Verbose mode: Processor %d, particle %d, dt %g, dt_accel %g\n", ThisTask, P[p].ID, dt, dt_accel); 
      }
      break;
    default:
      endrun(888);
      break;
    }

  if(P[p].Type == 0)
    {
#ifdef CHEMCOOL
      csnd = sqrt(SphP[p].Gamma * SphP[p].Pressure / SphP[p].Density);
#else
      csnd = sqrt(GAMMA * SphP[p].Pressure / SphP[p].Density);
#endif

      if(All.ComovingIntegrationOn) {
#ifdef CHEMCOOL
        fac3 = pow(All.Time, 3 * (1 - SphP[p].Gamma) / 2.0);
#endif
	dt_courant = 2 * All.CourantFac * All.Time * SphP[p].Hsml / (fac3 * SphP[p].MaxSignalVel);
      }
      else {
	dt_courant = 2 * All.CourantFac * SphP[p].Hsml / SphP[p].MaxSignalVel;
      }

#ifdef IGM
      while(P[p].Ti_endstep >= P[p].Ti_next_disperse) {
        P[p].Ti_next_disperse += (All.DisperseFac * (dt_courant / All.CourantFac) / All.Timebase_interval);
      }
#endif

      if (dt_courant < 1e-8) {
        printf("Short Courant timestep: %g\n", dt_courant);
        printf("h, vsig: %g, %g\n", SphP[p].Hsml, SphP[p].MaxSignalVel);
	fflush(stdout);
      }

      if(dt_courant < dt)
	dt = dt_courant;

      if (verbose == 1) {
	printf("Verbose mode: Processor %d, particle %d, dt %g, dt_courant %g\n", ThisTask, P[p].ID, dt, dt_courant); 
      }

#ifdef CHEMCOOL
      t2 = second();
      COOLI.id_current = P[p].ID;
      COOLI.index_current = p;
      radius = sqrt(P[p].Pos[0] * P[p].Pos[0] + P[p].Pos[1] * P[p].Pos[1] + P[p].Pos[2] * P[p].Pos[2]);
      gaccr = fabs(P[p].GravAccel[0]*P[p].Pos[0] + P[p].GravAccel[1]*P[p].Pos[1] + P[p].GravAccel[2]*P[p].Pos[2]);
      gaccr /= radius;
#ifdef CACHE_DTCOOL
      if (SphP[p].CoolingTime > 0.0) {
        dt_cooling = SphP[p].CoolingTime;
      }
      else {
#endif
      dt_cooling = GetCoolTime(&SphP[p], radius, gaccr);  
#ifndef SEMI_IMPLICIT
#ifndef NO_ADAPTIVE_DTCOOL
      /* Get a new estimate */
      if (dt_cooling < dt && dt_cooling < All.MaxSizeTimestep) {
        new_dtcool = do_chemcool_step(dt_cooling, &SphP[p], radius,  gaccr, 2);
      }
      else {
        new_dtcool = -1.0;
      }

      ntry = 1;
      old_dtcool = 0.;
      while (new_dtcool > dt_cooling && new_dtcool > old_dtcool && new_dtcool < dt && ntry < 5) {
	old_dtcool = new_dtcool;
        new_dtcool = do_chemcool_step(old_dtcool, &SphP[p], radius,  gaccr, 2);
	ntry++;
      }
      if (new_dtcool < old_dtcool) {
	new_dtcool = old_dtcool;
      }

#ifdef DEBUG_COOLING_RATES
      if (dt_cooling < 1e-8 && new_dtcool < 1e-8) {
        printf("Cooling time comparison: old %g, new %g\n", dt_cooling, new_dtcool);
	fflush(stdout);
      }
#endif
      if (new_dtcool > dt_cooling) {
        dt_cooling = new_dtcool;
      }
#ifdef CACHE_DTCOOL
      SphP[p].CoolingTime = dt_cooling;
      }
#endif
#ifdef DEBUG_COOLING_RATES
      if (dt_cooling < dt && dt_cooling < 1e-8) {
	printf("Particle %d, ID %d, timestep %g\n", p, P[p].ID, dt_cooling);
        for (i = 0; i < NRATES; i++) {
          printf("Radiative rate number %d = %g\n", i, rate_block_.radiative_rates[i]);
	}
        for (i = 0; i < NRATES_CHEM; i++) {
          printf("Chemical rate number %d = %g\n", i, rate_block_.chemical_rates[i]);
	}
	fflush(stdout);
      }
#endif
#endif /* ! NO_ADAPTIVE_DTCOOL */
      if(dt_cooling < dt) 
        dt = dt_cooling;

      if (verbose == 1) {
	printf("Verbose mode: Processor %d, particle %d, dt %g, dt_cooling %g\n", ThisTask, P[p].ID, dt, dt_cooling); 
      }

      /* Additional timestep constraint to prevent large entropy variations in a single step */
      if (SphP[p].DtEntropy > 0.0) {
        dt_entr = fabs(ENTROPY_TIMESTEP_FACTOR * SphP[p].Entropy / SphP[p].DtEntropy);
        if(dt_entr < dt) {
          dt = dt_entr;
	}
      }

      if (verbose == 1) {
	printf("Verbose mode: Processor %d, particle %d, dt %g, dt_entr %g\n", ThisTask, P[p].ID, dt, dt_entr); 
      }
      t3 = second();
      All.CPU_Chemcool += timediff(t2,t3);
#endif /* !SEMI_IMPLICIT */
#endif /* CHEMCOOL */

    }

#ifdef CHEMCOOL
  /* Limit timestep to prevent us from evolving past multiple output times. 
   * We can (and often must) overshoot one, but overshooting several causes
   * more trouble than it is worth
   */
  tend = P[p].Ti_endstep + (dt / All.Timebase_interval) / 2;
  if (tend > All.Ti_nextoutput) {
    time_after_next = All.Ti_nextnextoutput;
    if (time_after_next > 0 && time_after_next <= TIMEBASE && tend >= time_after_next) {
      /* The first check here is necessary in case find_next_outputtime returns a negative
       * value (which it will do if there's no further valid output time). We use a factor
       * of 1.95 rather than 2 in the line below to ensure that we fall just before the
       * output time 
       */
      dt = dt_output = 1.95 * All.Timebase_interval * (time_after_next - P[p].Ti_endstep);

      if (verbose == 1) {
	printf("Verbose mode: Processor %d, particle %d, dt %g, dt_output %g\n", ThisTask, P[p].ID, dt, dt_output); 
      }

    }
  }


#endif /* CHEMCOOL */

  /* convert the physical timestep to dloga if needed. Note: If comoving integration has not been selected,
     hubble_a=1.
   */
  dt_phys = dt;
  dt *= hubble_a;

  if(dt >= All.MaxSizeTimestep)
    dt = All.MaxSizeTimestep;

  if(dt >= dt_displacement)
    dt = dt_displacement;
/*
#ifdef CHEMCOOL
  if(dt < 1.3e-8) {
    printf("Short timestep: %g\n", dt);
    printf("Cooling time:   %g\n", dt_cooling);
    printf("Courant time:   %g\n", dt_courant);
    printf("Accel   time:   %g\n", dt_accel);
    printf("Entropy time:   %g\n", dt_entr);
    printf("Output  time:   %g\n", dt_output);
    printf("Particle Properties: \n");
    printf("pos %g %g %g \n ", P[p].Pos[0], P[p].Pos[1], P[p].Pos[2]);
    printf("vel %g %g %g \n ", P[p].Vel[0], P[p].Vel[1], P[p].Vel[2]);
    printf("rho %g u %g \n", SphP[p].Density, SphP[p].Entropy * pow(SphP[p].Density, SphP[p].Gamma) * All.UnitEnergy_in_cgs / pow(All.UnitLength_in_cm, 3));
  }
#endif
*/

  /* Determine what limited timestep in current case and update count */
  if (dt == dt_displacement) {
    n_displace++;
    P[p].Timestep_reason = 1;
  }
  else if (dt == All.MaxSizeTimestep) {
    n_max++;
    P[p].Timestep_reason = 2;
  }
  else if (dt_phys == dt_accel) {
    n_accel++;
    P[p].Timestep_reason = 3;
  }
  else if (P[p].Type == 0 && dt_phys == dt_courant) {
    n_courant++;
    P[p].Timestep_reason = 4;
  }
#ifdef CHEMCOOL
  else if (P[p].Type == 0 && dt_phys == dt_cooling) {
    n_cooling++;
    P[p].Timestep_reason = 5;
  }
  else if (P[p].Type == 0 && dt_phys == dt_entr) {
    n_entr++;
    P[p].Timestep_reason = 6;
  }
  else if (P[p].Type == 0 && dt_phys == dt_output) {
    n_output++;
    P[p].Timestep_reason = 7;
  }
#ifdef DEBUGTIMESTEPS
  if(dt< 1.e-10) {
     printf("Particle %d has very small timestep (about 30mins) \n", P[p].ID);
     printf("Time step reason: %d \n", P[p].Timestep_reason);
     printf("dt_displacement %g \n", dt_displacement);
     printf("dt_accel        %g \n", dt_accel);
     printf("dt_courant      %g \n", dt_courant);
     printf("dt_cooling      %g \n", dt_cooling);
     printf("dt_entr         %g \n", dt_entr);
     printf("dt_output       %g \n", dt_output);
     printf("Particle Properties: \n");
     printf("pos %g %g %g \n ", P[p].Pos[0], P[p].Pos[1], P[p].Pos[2]);
     printf("vel %g %g %g \n ", P[p].Vel[0], P[p].Vel[1], P[p].Vel[2]);
     printf("rho %g u %g \n", SphP[p].Density, SphP[p].Entropy * pow(SphP[p].Density, SphP[p].Gamma) * All.UnitEnergy_in_cgs / pow(All.UnitLength_in_cm, 3));
#if CHEMISTRYNETWORK == 1
     printf("species %g %g %g %g %g %g \n", SphP[p].TracAbund[0], SphP[p].TracAbund[1],SphP[p].TracAbund[2],
           SphP[p].TracAbund[3], SphP[p].TracAbund[4], SphP[p].TracAbund[5]);
#endif
#ifndef NOSTOP_WHEN_BELOW_MINTIMESTEP
     endrun(888);
#endif
  }
#endif
#endif


  if(dt < All.MinSizeTimestep)
    {
      if (dt_courant * hubble_a < All.MinSizeTimestep) {
        printf("Warning: Courant time wants to be below the limit `MinSizeTimestep'\n");
        printf("ID %d, courant time = %g, min_step = %g\n", P[p].ID, dt_courant * hubble_a, All.MinSizeTimestep);
      }
#ifndef NOSTOP_WHEN_BELOW_MINTIMESTEP
      printf("warning: Timestep wants to be below the limit `MinSizeTimestep'\n");

      if(P[p].Type == 0)
	{
	  printf
	    ("Part-ID=%d  dt=%g dtc=%g ac=%g xyz=(%g|%g|%g)  hsml=%g  maxsignalvel=%g dt0=%g eps=%g\n",
	     (int) P[p].ID, dt, dt_courant * hubble_a, ac, P[p].Pos[0], P[p].Pos[1], P[p].Pos[2],
	     SphP[p].Hsml, SphP[p].MaxSignalVel,
	     sqrt(2 * All.ErrTolIntAccuracy * atime * All.SofteningTable[P[p].Type] / ac) * hubble_a,
	     All.SofteningTable[P[p].Type]);
#ifdef CHEMCOOL
          printf("Part-ID=%d  dt=%g  dt_cool=%g  dt_entr=%g\n", (int) P[p].ID, dt, dt_cooling * hubble_a,
	         dt_entr * hubble_a);
          energy = SphP[p].Entropy * pow(SphP[p].Density, SphP[p].Gamma) / (SphP[p].Gamma - 1.0) ;
          energy *= All.UnitEnergy_in_cgs / pow(All.UnitLength_in_cm, 3);
          yn = SphP[p].Density*All.UnitMass_in_g/pow(All.UnitLength_in_cm, 3)/((1.0 + 4.0 * ABHE) * PROTONMASS);
          T_est = (SphP[p].Gamma - 1.0) * energy / (BOLTZMANN * (1.0 + ABHE - SphP[p].TracAbund[0]) * yn);
          printf("Temp (approx) %g density %g number density %g chemistry on/off %d ", T_est, SphP[p].Density*All.UnitMass_in_g/pow(All.UnitLength_in_cm, 3), yn, SphP[p].ChemistryDisabled);
#if CHEMISTRYNETWORK == 1
          printf("species %g %g %g %g %g %g \n", SphP[p].TracAbund[0], SphP[p].TracAbund[1],SphP[p].TracAbund[2],
           SphP[p].TracAbund[3], SphP[p].TracAbund[4], SphP[p].TracAbund[5]);
#endif
#if CHEMISTRYNETWORK == 4 || CHEMISTRYNETWORK == 5 || CHEMISTRYNETWORK == 6
          printf("Dust temperature... %g \n", SphP[p].DustTemp);
          printf("Abundances: H2 = %g, H+ = %g", SphP[p].TracAbund[IH2], SphP[p].TracAbund[IHP]);
#if CHEMISTRYNETWORK == 5 
          printf("CO = %g", SphP[p].TracAbund[ICO]);
#endif
#if CHEMISTRYNETWORK == 6
          printf("CO = %g, CO (ice) = %g", SphP[p].TracAbund[ICO], SphP[p].TracAbund[ICOS]);
#endif
          printf("\n");
#endif /* CHEMISTRYNETWORK= 4 || 5 || 6 */
#endif /* CHEMCOOL */
	}
      else
	{
	  printf("Part-ID=%d  dt=%g ac=%g xyz=(%g|%g|%g)\n", (int) P[p].ID, dt, ac, P[p].Pos[0], P[p].Pos[1],
		 P[p].Pos[2]);
	}
      fflush(stdout);
      endrun(888);
#endif
      dt = All.MinSizeTimestep;
    }

  ti_step = dt / All.Timebase_interval;

  if(!(ti_step > 0 && ti_step < TIMEBASE))
    {
      printf("\nError: A timestep of size zero was assigned on the integer timeline!\n"
	     "We better stop.\n"
	     "Task=%d Part-ID=%d dt=%g tibase=%g ti_step=%d ac=%g xyz=(%g|%g|%g) tree=(%g|%g|%g) reason=%d\n\n",
	     ThisTask, (int) P[p].ID, dt, All.Timebase_interval, ti_step, ac,
	     P[p].Pos[0], P[p].Pos[1], P[p].Pos[2], P[p].GravAccel[0], P[p].GravAccel[1], P[p].GravAccel[2],
             P[p].Timestep_reason);
#ifdef CHEMCOOL
      printf("Part-ID=%d  dt=%g  dt_cool=%g\n", (int) P[p].ID, dt, dt_cooling);
      printf("Temperature %g Density %g \n", SphP[p].TempOut, SphP[p].Density);
#if CHEMISTRYNETWORK== 1
      printf("Abundances: 1) %g 2)  %g 3)  %g 4)  %g 5)  %g 6) %g \n", SphP[p].TracAbund[0],SphP[p].TracAbund[1],SphP[p].TracAbund[2],
              SphP[p].TracAbund[3],SphP[p].TracAbund[4],SphP[p].TracAbund[5]);
#endif /* CHEMISTRYNETWORK = 1 */
#if CHEMISTRYNETWORK == 4 || CHEMISTRYNETWORK == 5 || CHEMISTRYNETWORK == 6
      printf("Dust temperature... %g \n", SphP[p].DustTemp);
      printf("Abundances: H2 = %g, H+ = %g", SphP[p].TracAbund[IH2], SphP[p].TracAbund[IHP]);
#if CHEMISTRYNETWORK == 5 
      printf("CO = %g", SphP[p].TracAbund[ICO]);
#endif
#if CHEMISTRYNETWORK == 6
      printf("CO = %g, CO (ice) = %g", SphP[p].TracAbund[ICO], SphP[p].TracAbund[ICOS]);
#endif
      printf("\n");
#endif /* CHEMISTRYNETWORK= 4 || 5 || 6 */
#endif
#ifdef PMGRID
      printf("pm_force=(%g|%g|%g)\n", P[p].GravPM[0], P[p].GravPM[1], P[p].GravPM[2]);
#endif
      if(P[p].Type == 0)
	printf("hydro-frc=(%g|%g|%g)\n", SphP[p].HydroAccel[0], SphP[p].HydroAccel[1], SphP[p].HydroAccel[2]);
      if ( All.TotN_sinks > 0 ) {
        printf("Sinks also present... distances to each from this particle:");
        for(isink = 0; isink <  All.TotN_sinks; isink++) { 
           dx = P[p].Pos[0] - All.SinkStoreXyz[isink][0];
           dy = P[p].Pos[1] - All.SinkStoreXyz[isink][1];
           dz = P[p].Pos[2] - All.SinkStoreXyz[isink][2];
#ifdef PERIODIC
           /*  now find the closest image in the given box size  */
           dx = NGB_PERIODIC_X(dx);
           dy = NGB_PERIODIC_Y(dy);
           dz = NGB_PERIODIC_Z(dz);
#endif
           r2 = sqrt(dx*dx + dy*dy + dz*dz);
           printf("Sink %d distance (code units) %g \n", isink, r2); 
        }
      }
      fflush(stdout);
      endrun(818);
    }

  return ti_step;
}


/*! This function computes an upper limit ('dt_displacement') to the global
 *  timestep of the system based on the rms velocities of particles. For
 *  cosmological simulations, the criterion used is that the rms displacement
 *  should be at most a fraction MaxRMSDisplacementFac of the mean particle
 *  separation. Note that the latter is estimated using the assigned particle
 *  masses, separately for each particle type. If comoving integration is not
 *  used, the function imposes no constraint on the timestep.
 */
void find_dt_displacement_constraint(double hfac /*!<  should be  a^2*H(a)  */ )
{
  int i, j, type, *temp;
  int count[6];
  long long count_sum[6];
  double v[6], v_sum[6], mim[6], min_mass[6];
  double dt, dmean, asmth = 0;

  dt_displacement = All.MaxSizeTimestep;

  if(All.ComovingIntegrationOn)
    {
      for(type = 0; type < 6; type++)
	{
	  count[type] = 0;
	  v[type] = 0;
	  mim[type] = 1.0e30;
	}

      for(i = 0; i < NumPart; i++)
	{
	  if(P[i].Type == 0 && P[i].ID < 0) /*SINK*/
	    continue;
	  v[P[i].Type] += P[i].Vel[0] * P[i].Vel[0] + P[i].Vel[1] * P[i].Vel[1] + P[i].Vel[2] * P[i].Vel[2];
	  if(mim[P[i].Type] > P[i].Mass)
	    mim[P[i].Type] = P[i].Mass;
	  count[P[i].Type]++;
	}

      MPI_Allreduce(v, v_sum, 6, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
      MPI_Allreduce(mim, min_mass, 6, MPI_DOUBLE, MPI_MIN, MPI_COMM_WORLD);

      temp = malloc(NTask * 6 * sizeof(int));
      MPI_Allgather(count, 6, MPI_INT, temp, 6, MPI_INT, MPI_COMM_WORLD);
      for(i = 0; i < 6; i++)
	{
	  count_sum[i] = 0;
	  for(j = 0; j < NTask; j++)
	    count_sum[i] += temp[j * 6 + i];
	}
      free(temp);

      for(type = 0; type < 6; type++)
	{
	  if(count_sum[type] > 0)
	    {
	      if(type == 0)
		dmean =
		  pow(min_mass[type] / (All.OmegaBaryon * 3 * All.Hubble * All.Hubble / (8 * M_PI * All.G)),
		      1.0 / 3);
	      else
		dmean =
		  pow(min_mass[type] /
		      ((All.Omega0 - All.OmegaBaryon) * 3 * All.Hubble * All.Hubble / (8 * M_PI * All.G)),
		      1.0 / 3);

	      dt = All.MaxRMSDisplacementFac * hfac * dmean / sqrt(v_sum[type] / count_sum[type]);

#ifdef PMGRID
	      asmth = All.Asmth[0];
#ifdef PLACEHIGHRESREGION
	      if(((1 << type) & (PLACEHIGHRESREGION)))
		asmth = All.Asmth[1];
#endif
	      if(asmth < dmean)
		dt = All.MaxRMSDisplacementFac * hfac * asmth / sqrt(v_sum[type] / count_sum[type]);
#endif

	      if(ThisTask == 0)
		printf("type=%d  dmean=%g asmth=%g minmass=%g a=%g  sqrt(<p^2>)=%g  dlogmax=%g\n",
		       type, dmean, asmth, min_mass[type], All.Time, sqrt(v_sum[type] / count_sum[type]), dt);

	      if(dt < dt_displacement)
		dt_displacement = dt;
	    }
	}

      if(ThisTask == 0)
	printf("displacement time constraint: %g  (%g)\n", dt_displacement, All.MaxSizeTimestep);
    }
}


#ifdef CHEMCOOL
void save_output_chemcool(struct sph_particle_data *current_particle)
{
  int j;
  double energy, rho, yn, abh2, ekn, temp;

  /* Save a copy of the abundances and entropy */
  for(j = 0; j < TRAC_NUM; j++) {
    current_particle->TracAbundOut[j] = current_particle->TracAbund[j];
  }
  current_particle->EntropyOut = current_particle->Entropy;
  current_particle->GammaOut   = current_particle->Gamma;
  /* Compute gas temperature */
  energy = current_particle->Entropy * pow(current_particle->Density, current_particle->Gamma)
    / (current_particle->Gamma - 1.0);
  energy *= All.UnitEnergy_in_cgs / pow(All.UnitLength_in_cm, 3);
  rho    = current_particle->Density * All.UnitDensity_in_cgs;
  yn     = rho / ((1.0 + 4.0 * ABHE) * PROTONMASS);
  abh2   = current_particle->TracAbundOut[IH2];
  ekn  = energy / (BOLTZMANN * (1.0 + ABHE - abh2) * yn);
  CALC_TEMP(&abh2, &ekn, &temp);
  current_particle->TempOut = temp;
  current_particle->DustTempOut = current_particle->DustTemp;
  return;
}
#endif
