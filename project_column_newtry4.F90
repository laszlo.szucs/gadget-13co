#if defined TREE_RAD || defined TREE_RAD_H2
#define NPIX  12*NSIDE*NSIDE

 subroutine project_column(column, columnH2, columnCO, dx, dy, dz, angular_radius, projection, projectionH2, projectionCO)
 use healpix

 implicit none

 real(KIND=DP), intent(inout) :: column, columnH2, columnCO, dx, dy, dz, angular_radius
 real(KIND=DP), dimension(0:NPIX-1), intent(inout) :: projection, projectionH2, projectionCO
 integer(kind=I4B), dimension(0:NPIX-1) :: min_vector_heal
 real(KIND=DP) :: one_over_nodesize, one_over_nodesize2, one_over_rp2
 real(KIND=DP), dimension(0:2, 0:NPIX-1) :: pvector
 real(KIND=DP), dimension(0:NPIX-1) :: theta_for_dz, pnorm_for_xy
 real(KIND=DP) :: mvec, mcent, dotprod, diagonals, rguess
 real(KIND=DP) :: atheta, aphi, Rphi, Rtheta, rn
 real(KIND=DP) :: a_xy, pnorm
 real(KIND=DP) :: theta_n, theta_p 
 integer(kind=I4B) :: i, iaxis, ixpix
 real(KIND=DP) :: hp_pixang, angle, rp, tworp, nodesum, tworn, areanorm
 real(KIND=DP) :: px, py, pz, pmin 
 real(KIND=DP) :: dnormx, dnormy, dnormz
 real(KIND=DP) :: weighting
 real(KIND=DP) :: acos_pcc
 common / pixel_share / pvector, theta_for_dz, pnorm_for_xy, min_vector_heal
 common / healpix_res / rp, tworp, one_over_rp2
 common / debug_heal_pcc / ixpix  

 rn = angular_radius
 tworn = 2.*rn
 dnormz = 1./sqrt(dx*dx + dy*dy)
 dnormy = 1./sqrt(dx*dx + dz*dz)
 dnormx = 1./sqrt(dy*dy + dz*dz)
 nodesum = rp + rn
 if (rn > rp ) then
    areanorm = tworp
 else
    areanorm = tworn
 end if

 !
 ! To save time, we don't use the query disc subroutine anymore.
 ! We simply loop around all pixels and calculate the angle to them
 ! using the dot product. The angles here are computed using a lookup table. 
 ! 

#ifndef IGNORE_TREECOL_LOOP
    do i = 0, NPIX-1
       weighting = 0.
       !
       ! Get the orthogonal angular distances (equiv to x/y) that describe the
       ! position of the node and pixel.
       ! standard theta / phi prescription breaks down when near the pole
       ! so chose theta to be defined from the axis with the minimal projection
       !
       select case ( min_vector_heal(i) )
          case (2)
             theta_n = acos_pcc(dz)
             Rtheta = abs(theta_for_dz(i) - theta_n)
             !if ( Rtheta.gt.1.570 ) cycle
             Rphi    = acos_pcc( (dx*pvector(0, i) + dy*pvector(1, i)) * dnormz * pnorm_for_xy(i) )
          case (1)
             theta_n = acos_pcc(dy)
             Rtheta = abs(theta_for_dz(i) - theta_n)
             !if ( Rtheta.gt.1.570 ) cycle
             Rphi    = acos_pcc( (dx*pvector(0, i) + dz*pvector(2, i)) * dnormy * pnorm_for_xy(i) )
          case (0)
             theta_n = acos_pcc(dx)
             Rtheta = abs(theta_for_dz(i) - theta_n)
             !if ( Rtheta.gt.1.570 ) cycle
             Rphi    = acos_pcc( (dy*pvector(1, i) + dz*pvector(2, i)) * dnormx * pnorm_for_xy(i) )
       end select
       !if ( Rphi.gt.1.570 ) cycle
       !
       ! Now we work out the overlapping area between the node projection
       ! and the pixel, and calculate the appropriate weighting function
       ! 
       if ( (Rtheta .lt. nodesum) .and. (Rphi .lt. nodesum) ) then
          !
          ! There is some overlap between the node and the pixel
          !
          atheta = min(nodesum - Rtheta, areanorm)
          aphi = min(nodesum - Rphi, areanorm)
          !atheta = nodesum - Rtheta
          !aphi = nodesum - Rphi
          weighting = 0.25*atheta*aphi*one_over_rp2
       end if
#ifdef TREE_RAD
       projection(i) = projection(i) + column*weighting
#endif
#ifdef TREE_RAD_H2
       projectionH2(i) = projectionH2(i) + columnH2*weighting
       projectionCO(i) = projectionCO(i) + columnCO*weighting
#endif
    enddo
#endif !!! ignore_treecol_loop

 !
 ! Done!
 !

 return
 end subroutine project_column 
!____________________________________________________________________________________
 
 subroutine get_angular_coords(ipix, theta, phi)
 use healpix
 integer, intent(inout) :: ipix
 real*8, intent(inout) :: theta, phi

 call pix2ang_ring(NSIDE, ipix, theta, phi)

 return
 end subroutine get_angular_coords

 subroutine get_pixels_for_xyz_axes(pixels)
 use healpix
 integer, dimension(6), intent(inout) :: pixels

 real*8, dimension(3) :: vector
 integer :: I, ipring

 do I = 1, 6
   vector(1) = 0.0
   vector(2) = 0.0
   vector(3) = 0.0
   select case (I)
   case (1)
     vector(1) = 1.0
   case (2)
     vector(1) = -1.0
   case (3)
     vector(2) = 1.0
   case (4)
     vector(2) = -1.0
   case (5)
     vector(3) = 1.0
   case (6)
     vector(3) = -1.0
   end select
   call vec2pix_ring(NSIDE, vector, ipring)
   pixels(I) = ipring
 enddo

 return
 end subroutine get_pixels_for_xyz_axes
!__________________________________________________________________________________
subroutine calculate_pixel_centres

 use healpix

 implicit none
 
 real(KIND=DP), dimension(1:3) :: pixel_centre
 real(KIND=DP), dimension(0:2, 0:NPIX-1) :: pvector
 real(KIND=DP), dimension(0:NPIX-1) :: theta_for_dz, pnorm_for_xy
 integer(kind=I4B), dimension(0:NPIX-1) :: min_vector_heal
 real(KIND=DP) :: rp, theta, dotprod, hp_pixang 
 real(KIND=DP) :: x, y, z, xmin, tworp, one_over_rp2
 real(KIND=DP) :: qx, qy, qz, px, py, pz, norm, pmin
 integer i, ixpix

 common / pixel_share / pvector, theta_for_dz, pnorm_for_xy, min_vector_heal
 common / healpix_res / rp, tworp, one_over_rp2
 common / debug_heal_pcc / ixpix
 
 xmin = 1.
 do i = 1, NPIX
    call pix2vec_ring(NSIDE, i-1, pixel_centre)
    pvector(0, i-1) = pixel_centre(1)
    pvector(1, i-1) = pixel_centre(2)
    pvector(2, i-1) = pixel_centre(3)
    x = pixel_centre(1)
    y = pixel_centre(2)
    z = pixel_centre(3)
    pmin = min( abs(x), abs(y), abs(z) )
    if ( pmin.eq.abs(x) ) then
       min_vector_heal(i-1) = 0
       theta_for_dz(i-1) = acos(x)
       pnorm_for_xy(i-1) = 1./sqrt(y*y + z*z)  
    else if ( pmin.eq.abs(y) ) then
       min_vector_heal(i-1) = 1
       theta_for_dz(i-1) = acos(y)
       pnorm_for_xy(i-1) = 1./sqrt(x*x + z*z)
    else
       min_vector_heal(i-1) = 2
       theta_for_dz(i-1) = acos(z)
       pnorm_for_xy(i-1) = 1./sqrt(x*x + y*y)
    end if
    if ( pixel_centre(1).lt.xmin ) then
       xmin = pixel_centre(1)
       ixpix = i-1 
    end if
 end do
 print *, "min_vector_heal", min_vector_heal
 print *, "theta_for_dz   ", theta_for_dz
 print *, "pnorm_for_xy   ", pnorm_for_xy

 !
 ! Get the angular distance between healpix pixels.
 !

 hp_pixang = 1.0e30
 x = pvector(0, 0)
 y = pvector(1, 0)
 z = pvector(2, 0)
 do i = 1, NPIX-1
    dotprod = pvector(0, i)*x + pvector(1, i)*y + pvector(2, i)*z
    if ( dotprod.lt.0 ) cycle
    theta = acos(dotprod)
    if ( theta.lt.hp_pixang ) hp_pixang = theta
 end do
 rp = 0.5*hp_pixang 
 tworp = 2.*rp
 one_over_rp2 = 1./rp/rp
 print *, "rp, tworp, one_over_rp2", rp, tworp, one_over_rp2

end subroutine calculate_pixel_centres
!_____________________________________________________________________________
subroutine createtriglookup

!
! create loop-up tables on each thread for various trig functions
!

 use healpix

 integer :: i
 real(KIND=DP) :: dx
 real(KIND=DP) :: acos_table(2001)
 common / trig_tables / acos_table

 dx = 2.0/2000.

 do i = 1, 2000+1
    acos_table(i) = acos( -1 + real(i-1)*dx )
 end do

end subroutine createtriglookup
!_____________________________________________________________________________
function acos_pcc(x)
 
!
! The function that does the look-up for the inverse cos.
! Should be fed with a number between -1 and 1
!

 use healpix

 integer index
 real(KIND=DP) :: acos_pcc
 real(KIND=DP) :: x
 real(KIND=DP) :: acos_table(2001)
 common / trig_tables / acos_table
 
 index = int((x+1)*1000) + 1
 index = min(index, 2001)
 index = max(index, 1) 
 acos_pcc = acos_table(index)

end function acos_pcc

#endif /* TREE_RAD || TREE_RAD_H2 */
