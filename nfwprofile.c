#ifdef NFWPROFILE
#define  PI               3.14159265358979323846

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <mpi.h>
#include <unistd.h>
#include <limits.h>

#include "allvars.h"
#include "proto.h"


/*  Adds a contribution to the gravitational accelerations
    from a static DM radial distribution. At the moment
    it reads in a radial mass profile and calculates the 
    accels based on the interior mass. Obviously this can be
    altered in the future to do analytic potentials too. The
    radial mass profile is stored in logarithmically spaced
    bins in this  implementation.
    
    We assume that G in code units is 1 and that everything has
    been centred on the densest particle/center of mass...
*/
     
void nfw_profile(void)
{
   int i;
   double gacc_rad;
   double dx, dy, dz, rad, x, Ix;
   double dc, rhoc, r200, factor;

   dc   = 3000.;
   rhoc = 1.96e-26 / All.UnitDensity_in_cgs;
   r200 = 4.629e20 / All.UnitLength_in_cm;
   factor = 4. * PI * dc * rhoc * All.G * r200;

   if (ThisTask == 0) {
      printf("Adding gacc contributions from dark matter profile... \n");
   }

   /* Loop around particles and get accels... */

   for (i=0; i < N_gas; i++) {
      if ( P[i].Ti_endstep == All.Ti_Current ) {   
         if (P[i].Type != 0 || P[i].ID < 0) continue;
         dx = P[i].Pos[0] - 7500.;
         dy = P[i].Pos[1] - 7500.;
         dz = P[i].Pos[2] - 7500.;
         rad = sqrt(dx*dx + dy*dy + dz*dz);
         if (rad == 0.) continue;
         x = rad / r200;
         if (x > 1) {
           Ix = (1./27.) * (log(4.) - 0.75);
	 }
	 else {
           Ix = (1./27.) * (1./(1. + 3.*x) + log(1. + 3.*x) - 1.); 
	 }
         gacc_rad = -factor * Ix / (x * x);

         P[i].GravAccel[0] += gacc_rad*dx/rad;
         P[i].GravAccel[1] += gacc_rad*dy/rad;
         P[i].GravAccel[2] += gacc_rad*dz/rad;
      }
   }
  
   if (ThisTask == 0)
      printf("Done with radial dark matter contributions \n");

}
#endif
