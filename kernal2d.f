	function kernal2d(vr)
	
	!
	! This function returns the kernal value for a given
	! value of r/h. The subroutine:
	!
	! 	buildcol3d
	!
	! should be invoked before using this function
	!
	
	double precision kernal2d
	real*8 		 vr
	double precision coltable(1000)
	integer 	 pos, pos1, pos2
	double precision r1, r2
	double precision valuemean
	
	common / table / coltable
		
	pos=int(vr*500.0)+1
        if (pos.ge.1000) then
           pos1=999
           pos2=1000
        else if (pos.le.0) then
           pos1 = 2
           pos2 = 3
        else
           pos1=pos
           pos2=pos1+1
        end if
        r1=(pos1-1)/500.
        r2=(pos2-1)/500.
        if ( coltable(pos1).lt.0 .or. coltable(pos2).lt.0  ) then
           print *, "Picked up negative contribution from coltable"
	   print *, "coltables ", coltable(pos1), coltable(pos2)
	   print *, "pos values:", pos, pos1, pos2
        end if
        valuemean = (vr-r1)/(r2-r1)*(coltable(pos2)-coltable(pos1))
	kernal2d = valuemean + coltable(pos1)
        if ( kernal2d.lt.0 ) kernal2d = 0.
 	if ( kernal2d.lt.0 ) then
	   print *, "2D kernal < 0 !!!!"
	   print *, "r1, r2, vr", r1, r2, vr
	   print *, "pos1, pos2, pos", pos1, pos2, pos
	   print *, "coltable(pos2)-coltable(pos1)", 
     &	             coltable(pos2)-coltable(pos1)
	end if
        if ( kernal2d.lt.0 ) kernal2d = 0.
        if ( kernal2d/kernal2d.ne.1 ) then
           print *, "kernal2d problem: ", kernal2d
           stop
        end if
        
	end function kernal2d
