#ifdef DMA
/*
 *      blumenthal.c
 *      Author: Christopher Dietl
 * 		Date:	04/03/2009
 * 
 * 
 * 		This code uses the Blumenthal method (see below) to solve iteratively for the dark matter mass profile after an
 * 		adiabatic contraction of a halo with a given intial dark matter mass profile and intial baryonic mass profile.
 * 
 * 		For more information about the physical background: Blumenthal G. R., Faber S. M., Flores R., Primack J. R., ApJ, 1986, 301, 27 
 * 
 * 
 * 		Note: 	This Code does not perform any parameter checks and does not produces any error messages, so it needs proper input.
 * 			Especially the final baryonic mass profile array needs to be wider than the the intial mass profiles.
 * 		        Note, that the radius of a dark matter mass shell is not always shifted inwards (see plots of core density 10E-4 in the 
 *                      "example plots" folder). 
 */

#include <math.h>
#include "blumenthal.h"


// utility function
short getSign(double x)
{
	if (x < 0.0)
		return NEG;
	else
		return POS;
}


/*
 * Interpolates the baryonic mass between two array entries. This is done linearly using the logarithms, so
 * a power-law behavior of the function is better interpolated.
 * 
 * Parameter:	r                                Radius in cm, at which the mass is sought
 * 		finalBayronMassProfile		 Baryonic mass profile (cm & g)
 * 		radiusFactor			 Ratio between two successive radius entries in finalBaryonMassProfile[][].
 * 						      This is needed to guess the upper und lower indices.
 * 
 * returns      interpolated mass at the given radius r
 */
double getBaryonMassF(double r, double finalBaryonMassProfile[][2], const double radiusFactor)
{	
	double  n = log(r/finalBaryonMassProfile[0][RADIUS])/log(radiusFactor);
		
	int lowerBound = (int)floor(n);
	int upperBound = (int)ceil(n);
	
	if (lowerBound == upperBound)
		return finalBaryonMassProfile[lowerBound][MASS];
	
 	double lowerMass = finalBaryonMassProfile[lowerBound][MASS];
	double upperMass = finalBaryonMassProfile[upperBound][MASS];
	
	double lowerRadius = finalBaryonMassProfile[lowerBound][RADIUS];
	double upperRadius = finalBaryonMassProfile[upperBound][RADIUS];
	 
	double deltaR = log(r/lowerRadius);
	
	return exp(log(lowerMass)+deltaR*(log(upperMass/lowerMass)/log(upperRadius/lowerRadius)));
}




/*
 * We seek the radius rf (final radius) inside which we have the same mass as our final
 * radius ri (initial radius) with the given initial mass profile and the final baryonic 
 * mass profile. This boils down to the finding of the root of g(r).
 *  
 * Parameters:	 r	radius (the root as a function of r is sought)
 * 		 ri	initial radius for which the final radius is sought
 * 	    Mbi/Mdi     initial dark/baryonic mass interior to ri 
 * 			(note: this is determined by ri, but is given as an argument to minimize calculations)			
 * 	    finalBaryonMassProfile[][2]      array with the final baryonic mass profile (radius, mass)
 * 	    radiusFactor		     ratio between two successive radius entries in finalBaryonMassProfile[][].
 * 					     This is needed to get a guess for the upper und lower indices.
 */
double g(double r,double ri, double Mbi, double Mdi, double finalBaryonMassProfile[][2], const double radiusFactor)
{
	return (ri*(Mbi+Mdi))/(getBaryonMassF(r,finalBaryonMassProfile,radiusFactor)+Mdi)-r;
}

/*
 * The following stuff was needed for more fancy iteration methods (Newton, Halley), but they were abandonded,
 * because the bisection method was fast enough and derivatives of the final baryonic mass would be needed.
 */

 /* 
// 1st derivative of g; needed for the newton iteration
double gd(double r, double ri, double Mi)
{
  return DOWNSCALE*((-ri*Mi*getBaryonMassFd(r))/(SQUARE((getBaryonMassF(r)+DM_FRACTION*Mi)))-1.0);
}
// 2nd derivative of g; is not needed now, but could be used in a more complex iteration method like the halley method.
double gdd(double r, double ri, double Mi)
{
	return DOWNSCALE*((-ri*Mi*getBaryonMassFdd(r))/SQUARE(getBaryonMassF(r)+DM_FRACTION*Mi)
	-(2.0*ri*Mi*SQUARE(getBaryonMassFd(r)))/CUBE(getBaryonMassF(r)+DM_FRACTION*Mi));
}
*/

/*
 * Iteration Function (Bisection)
 * 
 * Finds the corresponding final radius (which corresponds to the root of g(...)) iteratively with the classic Bisection method.
 * 
 * Parameters:	a		initial left inverval bound
 * 				b		initial right interval bound
 * 				ri		intial radius to which the final radius is seeked
 * 				Mbi/Mdi	intial dark/baryonic mass interior to ri (is determined by ri, but is given as an argument
 * 						to minimize calculations)
 * 
 * returns:		The final radius according to the intial radius and mass.
 */
double getRfBiSec(double a, double b, double ri, double Mbi, double Mdi, double finalBaryonMassProfile[][2], double radiusFactor)
{	
	double left = a;
	double right = b;
	
	double mid;
	
	while (right-left > EPSILON_BISEC)
	{
		mid = (left+right)*0.5;
		
				
		double midVal = g(mid,ri,Mbi,Mdi,finalBaryonMassProfile,radiusFactor);
		double leftVal = g(left,ri,Mbi,Mdi,finalBaryonMassProfile,radiusFactor);
		double rightVal = g(right,ri,Mbi,Mdi,finalBaryonMassProfile,radiusFactor);
		
		if(rightVal == 0)
			return right;
		else if (leftVal == 0)
			return left;
		else if (midVal == 0)
			return mid;
			
		if (getSign(midVal) != getSign(rightVal))
		{
			left = mid;
		}
		else if (getSign(midVal) != getSign(leftVal))
		{
			right = mid;
		}
	}

	return (left+right)/2.0;
}


/*
 * Classic Newton Iteration Method
 * 
 * This method sometimes enters non convergent cycles or "overshoots". It was abandonded, because
 * of these problems and the need for the first derivative for the baryonic mass profile.
 * 
 * 
 * 
 * Parameters:	start	intial guess for the root;
 * 						should be as near as possible to the root; in our case ri is used
 * 				ri		intial radius to which the final radius is seeked
 * 				Mi		intial mass interior to ri (is determined by ri, but is given as an argument
 * 						to minimize calculations)
 */
/*
double getRfNewton(double start, double ri, double Mi)
{
	double r = start;
	double r_next = 0.0;
	double r_last;
	
	int i = 0;
  
  while( 1 )
    {
		r_next = r - g(r,ri,Mi)/gd(r,ri,Mi);
		
		if (fabs((r-r_next)/r) <= EPSILON_NEWTON || g(r_next,ri,Mi) == 0)
			break;
						
		// too many iterations or non converging cycle, try another method
		if(i > MAX_NEWTON_ITER || (r_next == r_last))
		{
			// negative radius is physical nonsense, so it is returned as an error signal
			return -1.0;
		}
				
		r_last = r;
		r = r_next;
		i++;
    }
	
  return r_next;
}
*/


/*
 * This function should be used as the interface from outside code.
 * 
 * Parameters:	initialBaryonMassProfile[][2]	Initial baryonic mass profile (radius, mass)
 *				initialDMMassProfile[][2]	     Initial dark matter mass profile (radius, mass)
 * 				finalBaryonMassProfile[][2]	     Final baryonic mass profile (radius, mass)
 * 				radiusFactor			     Ratio between two successive radius entries in finalBaryonMassProfile[][].
 * 								     This is needed to get a guess for the upper und lower indices.
 * 				sizeInitial			     Size of initialBaryonMassProfile[] and initialDMMassProfile[]
 * 				sizeFinal			     Size of finalBaryonMassProfile[]
 * 				retFinalDMMassProfile		     Call by reference array to which the final dark matter mass profile is written
 */

void BlumenthalContraction(double initialRadius[], 
                           double initialBaryonMassProfile[],
  	        	   double initialDMMassProfile[],
			   double finalBaryonRadius[],
                           double finalBaryonMass[],
  			   double radiusFactor,
			   int sizeInitial,
			   int sizeFinal,
                           double finalDMRadius[],
			   double finalDMMass[])
{	
	int i;
	double finalBaryonMassProfile[][2];

	/* Set up Baryon mass 2D array used by interpolation code */
        finalBaryonMassProfile = malloc(sizeof(double)*2*sizeFinal);
        for(i=0; i<sizeFinal; i++) 
        {
          finalBaryonMassProfile[i][RADIUS] = finalBaryonRadius[i];
          finalBaryonMassProfile[i][MASS]   = finalBaryonMass[i];
	}

	for(i=0; i<sizeInitial; i++)
	{
		double ri  = initialRadius[i];
		double Mbi = initialBaryonMassProfile[i];
		double Mdi = initialDMMassProfile[i];
		
		/*
		 * The Interpolation Algorithm seeks for the adjacent array indices for a given radius.
		 * Due to numerical errors, it tries to read the entry above the right intial bound. That's why
		 * we take one step below the maximum value (sizeFinal - 2) as the intial right bound.
		 */
		 
		double rf = getRfBiSec(	finalBaryonMassProfile[0][RADIUS],
								finalBaryonMassProfile[sizeFinal-2][RADIUS],ri,
								Mbi,Mdi,finalBaryonMassProfile,radiusFactor );
		
		// We get the radius, within which is the same amount of dark matter mass as within the initial radius ri
		finalDMRadius[i] = rf;
		finalDMMass[i]   = initialDMMassProfile[i];
	}
        free(finalBaryonMassProfile);
	return;
}
#endif /* DMA */
