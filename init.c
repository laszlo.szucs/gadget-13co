#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <mpi.h>

#include "allvars.h"
#include "proto.h"

/*! \file init.c
 *  \brief Code for initialisation of a simulation from initial conditions
 */


/*! This function reads the initial conditions, and allocates storage for the
 *  tree. Various variables of the particle data are initialised and An intial
 *  domain decomposition is performed. If SPH particles are present, the inial
 *  SPH smoothing lengths are determined.
 */
void init(void)
{
  int i, j;
#ifdef CHEMCOOL
  int invalid_abundance_flag;
  double max_abundance, min_abundance;
  double gamm1, ekn, gamma, abh2, temp;
#ifdef THERMAL_INFO_DUMP
  double dummy, radius, gaccr;
#endif
#endif
  double a3;

  All.Time = All.TimeBegin;

  switch (All.ICFormat)
    {
    case 1:
#if (MAKEGLASS > 1)
      seed_glass();
#else
      read_ic(All.InitCondFile);
#endif
      break;
    case 2:
    case 3:
      read_ic(All.InitCondFile);
      break;
    default:
      if(ThisTask == 0)
	printf("ICFormat=%d not supported.\n", All.ICFormat);
      endrun(0);
    }

  All.Time = All.TimeBegin;
  All.Ti_Current = 0;

  if(All.ComovingIntegrationOn)
    {
      All.Timebase_interval = (log(All.TimeMax) - log(All.TimeBegin)) / TIMEBASE;
      a3 = All.Time * All.Time * All.Time;
    }
  else
    {
      All.Timebase_interval = (All.TimeMax - All.TimeBegin) / TIMEBASE;
      printf("Time max %g, time begin %g, diff %g\n", All.TimeMax, All.TimeBegin, All.TimeMax - All.TimeBegin);
      a3 = 1;
    }

  set_softenings();

  All.NumCurrentTiStep = 0;	/* setup some counters */
  All.SnapshotFileCount = 0;
  if(RestartFlag == 2)
    All.SnapshotFileCount = atoi(All.InitCondFile + strlen(All.InitCondFile) - 3) + 1;

  All.TotNumOfForces = 0;
  All.NumForcesSinceLastDomainDecomp = 0;

#if defined(MAGNETIC) && defined(BSMOOTH)
  All.MainTimestepCounts = 0;
#endif

  if(All.ComovingIntegrationOn)
    if(All.PeriodicBoundariesOn == 1)
      check_omega();

  All.TimeLastStatistics = All.TimeBegin - All.TimeBetStatistics;

  if(All.ComovingIntegrationOn)	/*  change to new velocity variable */
    {
      for(i = 0; i < NumPart; i++)
	for(j = 0; j < 3; j++)
	  P[i].Vel[j] *= sqrt(All.Time) * All.Time;
    }

  for(i = 0; i < NumPart; i++)	/*  start-up initialization */
    {
      for(j = 0; j < 3; j++)
	P[i].GravAccel[j] = 0;
#ifdef PMGRID
      for(j = 0; j < 3; j++)
	P[i].GravPM[j] = 0;
#endif
      P[i].Ti_endstep = 0;
      P[i].Ti_begstep = 0;
#ifdef IGM
      /* XXX: TODO -- allow this to be set to a non-zero value in the input deck */
      P[i].Ti_next_disperse = 0;
#endif

      P[i].OldAcc = 0;
      P[i].GravCost = 1;
      P[i].Potential = 0;
    }

#ifdef IGM
  for(i=0; i<N_gas; i++) {
    SphP[i].NewMetallicity = 0.0;
  }
#endif

#ifdef PMGRID
  All.PM_Ti_endstep = All.PM_Ti_begstep = 0;
#endif

  All.PresentMinStep = TIMEBASE;
#ifdef FLEXSTEPS
  for(i = 0; i < NumPart; i++)	/*  start-up initialization */
    {
      P[i].FlexStepGrp = (int) (TIMEBASE * get_random_number(P[i].ID));
    }
#endif

/* Sanity checks for initial values */ 
#ifdef CHEMCOOL
  invalid_abundance_flag = 0;
  if (ThisTask == 0) {
    /* H2 */
    if (All.InitMolHydroAbund > 0.5 || All.InitMolHydroAbund < 0.0) {
      invalid_abundance_flag = 1;
    }
    /* H+ */
    if (All.InitHPlusAbund > 1.0 || All.InitHPlusAbund < 0.0) {
      invalid_abundance_flag = 1;
    }
    /* HD, D+ */
    max_abundance = GADGET2_MAX(All.InitDIIAbund, All.InitHDAbund);
    min_abundance = GADGET2_MIN(All.InitDIIAbund, All.InitHDAbund);
    if (max_abundance > All.DeutAbund || min_abundance < 0.0) {
      invalid_abundance_flag = 1;
    }
    /* He+, He++ */
    max_abundance = GADGET2_MAX(All.InitHeIIAbund, All.InitHeIIIAbund);
    min_abundance = GADGET2_MIN(All.InitHeIIAbund, All.InitHeIIIAbund);
    if (max_abundance > ABHE || min_abundance < 0.0) {
      invalid_abundance_flag = 1;
    }
    /* C+, CO, HCO+, CH, CH2, CH3+, CHx */
    max_abundance = GADGET2_MAX(All.InitCIIAbund, All.InitCOAbund);
    min_abundance = GADGET2_MIN(All.InitCIIAbund, All.InitCOAbund);
    max_abundance = GADGET2_MAX(All.InitHCOPlusAbund, max_abundance);
    min_abundance = GADGET2_MIN(All.InitHCOPlusAbund, min_abundance);
    max_abundance = GADGET2_MAX(All.InitCHAbund, max_abundance);
    min_abundance = GADGET2_MIN(All.InitCHAbund, min_abundance);
    max_abundance = GADGET2_MAX(All.InitCH2Abund, max_abundance);
    min_abundance = GADGET2_MIN(All.InitCH2Abund, min_abundance);
    max_abundance = GADGET2_MAX(All.InitCH3PlusAbund, max_abundance);
    min_abundance = GADGET2_MIN(All.InitCH3PlusAbund, min_abundance);
    max_abundance = GADGET2_MAX(All.InitCHXAbund, max_abundance);
    min_abundance = GADGET2_MIN(All.InitCHXAbund, min_abundance);
    if (max_abundance > All.CarbAbund || min_abundance < 0.0) {
      invalid_abundance_flag = 1;
    }
    /* O+, OH, H2O, CO, OHx */
    max_abundance = GADGET2_MAX(All.InitOIIAbund, All.InitOHAbund);
    min_abundance = GADGET2_MIN(All.InitOIIAbund, All.InitOHAbund);
    max_abundance = GADGET2_MAX(All.InitH2OAbund, max_abundance);
    min_abundance = GADGET2_MIN(All.InitH2OAbund, min_abundance);
    max_abundance = GADGET2_MAX(All.InitCOAbund, max_abundance);
    min_abundance = GADGET2_MIN(All.InitCOAbund, min_abundance);
    max_abundance = GADGET2_MAX(All.InitOHXAbund, max_abundance);
    min_abundance = GADGET2_MIN(All.InitOHXAbund, min_abundance);
    max_abundance = GADGET2_MAX(All.InitH3OPAbund, max_abundance);
    min_abundance = GADGET2_MIN(All.InitH3OPAbund, min_abundance);
    if (max_abundance > All.OxyAbund || min_abundance < 0.0) {
      invalid_abundance_flag = 1;
    } 
    /* C2 */
    if (All.InitC2Abund > 0.5 * All.CarbAbund || All.InitC2Abund < 0.0) {
      invalid_abundance_flag = 1;
    }
    /* O2, O2+ */
    if (All.InitO2Abund > 0.5 * All.OxyAbund || All.InitO2Abund < 0.0) {
      invalid_abundance_flag = 1;
    }
    if (All.InitO2PAbund > 0.5 * All.OxyAbund || All.InitO2PAbund < 0.0) {
      invalid_abundance_flag = 1;
    }
    /* Si+, Si++ */
    max_abundance = GADGET2_MAX(All.InitSiIIAbund, All.InitSiIIIAbund);
    min_abundance = GADGET2_MIN(All.InitSiIIAbund, All.InitSiIIIAbund);
    if (max_abundance > All.SiAbund || min_abundance < 0.0) {
      invalid_abundance_flag = 1;
    }
    /* M+ */
    if (All.InitMPAbund > All.MAbund || All.InitMPAbund < 0.0) {
      invalid_abundance_flag = 1;
    }
    /* Nitrogen */
    max_abundance = GADGET2_MAX(All.InitN2Abund, All.InitN2HPAbund);
    min_abundance = GADGET2_MIN(All.InitN2Abund, All.InitN2HPAbund);
    if (max_abundance > 0.5 * All.NitroAbund || min_abundance < 0.0) {
      invalid_abundance_flag = 1;
    }
    if (All.InitNOAbund > All.NitroAbund || All.InitNOAbund < 0.0) {
      invalid_abundance_flag = 1;
    }

    if (invalid_abundance_flag) {
      fprintf(stderr, "Initial abundances are invalid!\n");
      endrun(0);
    }
  }
#endif /* CHEMCOOL */

  for(i = 0; i < N_gas; i++)	/* initialize sph_properties */
    {
      for(j = 0; j < 3; j++)
	{
	  SphP[i].VelPred[j] = P[i].Vel[j];
	  SphP[i].HydroAccel[j] = 0;
	}

#ifndef POLYTROPE
      SphP[i].DtEntropy = 0;
#endif
#ifdef CHEMCOOL
      SphP[i].DtEntropyVisc = 0;
      SphP[i].EntropyOut = SphP[i].Entropy;
      SphP[i].ChemistryDisabled = 0;
#endif
      if(RestartFlag == 0)
	{
#ifdef CHEMCOOL
          SphP[i].DustTemp = All.InitDustTemp;
          SphP[i].DustTempOut = All.InitDustTemp;

	  switch (All.ChemistryNetwork) {
	  case 1:
	    SphP[i].TracAbund[IH2]   = All.InitMolHydroAbund;
	    SphP[i].TracAbund[IHP]   = All.InitHPlusAbund;
	    SphP[i].TracAbund[IDP]   = All.InitDIIAbund;
	    SphP[i].TracAbund[IHD]   = All.InitHDAbund;
	    SphP[i].TracAbund[IHEP]  = All.InitHeIIAbund;
	    SphP[i].TracAbund[IHEPP] = All.InitHeIIIAbund;
	    break;
	  case 2:
	    SphP[i].TracAbund[IH2]   = All.InitMolHydroAbund;
	    SphP[i].TracAbund[IHP]   = All.InitHPlusAbund;
	    SphP[i].TracAbund[IDP]   = All.InitDIIAbund;
	    SphP[i].TracAbund[IHD]   = All.InitHDAbund;
	    SphP[i].TracAbund[IHEP]  = All.InitHeIIAbund;
	    SphP[i].TracAbund[IHEPP] = All.InitHeIIIAbund;
            SphP[i].TracAbund[IC]    = All.InitCIIAbund;
            SphP[i].TracAbund[ISi]   = All.InitSiIIAbund;
            SphP[i].TracAbund[IO]    = All.InitOIIAbund;
	    break;
	  case 3:
	    SphP[i].TracAbund[IH2]   = All.InitMolHydroAbund;
	    SphP[i].TracAbund[IHP]   = All.InitHPlusAbund;
	    SphP[i].TracAbund[IDP]   = All.InitDIIAbund;
	    SphP[i].TracAbund[IHD]   = All.InitHDAbund;
	    SphP[i].TracAbund[IHEP]  = All.InitHeIIAbund;
	    SphP[i].TracAbund[IHEPP] = All.InitHeIIIAbund;
            SphP[i].TracAbund[IC]    = All.InitCIIAbund;
            SphP[i].TracAbund[ISi]   = All.InitSiIIAbund;
            SphP[i].TracAbund[IO]    = All.InitOIIAbund;
	    SphP[i].TracAbund[ICO]   = All.InitCOAbund;
	    SphP[i].TracAbund[IC2]   = All.InitC2Abund;
	    SphP[i].TracAbund[IOH]   = All.InitOHAbund;
	    SphP[i].TracAbund[IH2O]  = All.InitH2OAbund;
	    SphP[i].TracAbund[IO2]   = All.InitO2Abund;
	    SphP[i].TracAbund[IHCOP] = All.InitHCOPlusAbund;
	    SphP[i].TracAbund[ICH]   = All.InitCHAbund;
	    SphP[i].TracAbund[ICH2]  = All.InitCH2Abund;
	    SphP[i].TracAbund[ISIPP] = All.InitSiIIIAbund;
	    SphP[i].TracAbund[ICH3P] = All.InitCH3PlusAbund;
	    break;
	  case 4:
	    SphP[i].TracAbund[IH2] = All.InitMolHydroAbund;
	    SphP[i].TracAbund[IHP] = All.InitHPlusAbund;
	    break;
	  case 5:
	    SphP[i].TracAbund[IH2] = All.InitMolHydroAbund;
	    SphP[i].TracAbund[IHP] = All.InitHPlusAbund;
	    SphP[i].TracAbund[ICO] = All.InitCOAbund;
	    break;
          case 6:
	    SphP[i].TracAbund[IH2]  = All.InitMolHydroAbund;
	    SphP[i].TracAbund[IHP]  = All.InitHPlusAbund;
	    SphP[i].TracAbund[ICO]  = All.InitCOAbund;
	    SphP[i].TracAbund[ICOS] = 0.0;
	    break;
	  case 7:
	    SphP[i].TracAbund[IH2]   = All.InitMolHydroAbund;
	    SphP[i].TracAbund[IHP]   = All.InitHPlusAbund;
	    SphP[i].TracAbund[IHEP]  = All.InitHeIIAbund;
            SphP[i].TracAbund[IC]    = All.InitCIIAbund;
            SphP[i].TracAbund[IO]    = All.InitOIIAbund;
	    SphP[i].TracAbund[ICO]   = All.InitCOAbund;
	    SphP[i].TracAbund[IC2]   = All.InitC2Abund;
	    SphP[i].TracAbund[IOH]   = All.InitOHAbund;
	    SphP[i].TracAbund[IH2O]  = All.InitH2OAbund;
	    SphP[i].TracAbund[IO2]   = All.InitO2Abund;
	    SphP[i].TracAbund[IHCOP] = All.InitHCOPlusAbund;
	    SphP[i].TracAbund[ICH]   = All.InitCHAbund;
	    SphP[i].TracAbund[ICH2]  = All.InitCH2Abund;
	    SphP[i].TracAbund[ICH3P] = All.InitCH3PlusAbund;
	    break;
	  case 8:
	    SphP[i].TracAbund[IH2]   = All.InitMolHydroAbund;
	    SphP[i].TracAbund[IHP]   = All.InitHPlusAbund;
	    SphP[i].TracAbund[IHEP]  = All.InitHeIIAbund;
            SphP[i].TracAbund[IC]    = All.InitCIIAbund;
            SphP[i].TracAbund[IO]    = All.InitOIIAbund;
	    SphP[i].TracAbund[ICO]   = All.InitCOAbund;
	    SphP[i].TracAbund[IC2]   = All.InitC2Abund;
	    SphP[i].TracAbund[IOH]   = All.InitOHAbund;
	    SphP[i].TracAbund[IH2O]  = All.InitH2OAbund;
	    SphP[i].TracAbund[IO2]   = All.InitO2Abund;
	    SphP[i].TracAbund[IHCOP] = All.InitHCOPlusAbund;
	    SphP[i].TracAbund[ICH]   = All.InitCHAbund;
	    SphP[i].TracAbund[ICH2]  = All.InitCH2Abund;
	    SphP[i].TracAbund[ICH3P] = All.InitCH3PlusAbund;
            SphP[i].TracAbund[IH3OP] = All.InitH3OPAbund;
            SphP[i].TracAbund[IO2P]  = All.InitO2PAbund;
            SphP[i].TracAbund[INO]   = All.InitNOAbund;
            SphP[i].TracAbund[IN2]   = All.InitN2Abund;
            SphP[i].TracAbund[IN2HP] = All.InitN2HPAbund;
	    break;
          case 15:
            SphP[i].TracAbund[IH2]   = All.InitMolHydroAbund;
            SphP[i].TracAbund[IHP]   = All.InitHPlusAbund;
            SphP[i].TracAbund[IC]    = All.InitCIIAbund;
            SphP[i].TracAbund[ICHX]  = All.InitCHXAbund;
            SphP[i].TracAbund[IOHX]  = All.InitOHXAbund;
            SphP[i].TracAbund[ICO]   = All.InitCOAbund;
            SphP[i].TracAbund[IHCOP] = All.InitHCOPlusAbund;
#ifdef _13CO_
            SphP[i].TracAbund[I13CO]   = All.Init13COAbund;
            SphP[i].TracAbund[I13CP]   = All.Init13CIIAbund;
            SphP[i].TracAbund[I13CHX]  = All.Init13CHXAbund;
            SphP[i].TracAbund[IH13COP] = All.InitH13COPlusAbund;
#endif
            SphP[i].TracAbund[IHEP]  = All.InitHeIIAbund;
            SphP[i].TracAbund[IMP]   = All.InitMPAbund;
	  default:
	    break;
	  }

          for(j=0; j<TRAC_NUM; j++) {
	    SphP[i].TracAbundOut[j] = SphP[i].TracAbund[j];
	  }

#endif /* CHEMCOOL */
	}
        else {
#ifdef CHEMCOOL
	/* Starting from a snapshot. If the dust temp is not present in
	 * the snapshot file, then we need to initialize it here. We also
	 * always need to initialize TracAbundOut and EntropyOut here, as
	 * neither of these are in the snapshot file.
	 */
	  /* if (All.ChemistryNetwork != 4 && All.ChemistryNetwork != 5 && All.ChemistryNetwork != 7) { */
            if (All.ChemistryNetwork != 1 || All.DustToGasRatio == 0) {
              SphP[i].DustTemp    = All.InitDustTemp;
	    }
	  /* }*/
	  for(j=0; j<TRAC_NUM; j++) {
	      SphP[i].TracAbundOut[j] = SphP[i].TracAbund[j];
	  } 
	  SphP[i].EntropyOut = SphP[i].Entropy;
	  SphP[i].DustTempOut = SphP[i].DustTemp;
#endif
        }

#ifdef MAGNETIC
#ifndef EULERPOTENTIALS
      for(j = 0; j < 3; j++)
	{
	  SphP[i].DtB[j] = 0;
	  SphP[i].BPred[j] = SphP[i].B[j];
	}
#endif
#ifdef BINISET
      SphP[i].B[0] = All.BiniX;
      SphP[i].B[1] = All.BiniY;
      SphP[i].B[2] = All.BiniZ;
      SphP[i].BPred[0] = All.BiniX;
      SphP[i].BPred[1] = All.BiniY;
      SphP[i].BPred[2] = All.BiniZ;
#endif
#if defined(TIME_DEP_MAGN_DISP) || defined(TIME_DEP_EULER_DISSIPATION)
#ifdef HIGH_MAGN_DISP_START
      SphP[i].Balpha = All.ArtMagDispConst;
#else
      SphP[i].Balpha = All.ArtMagDispMin;
#endif
      SphP[i].DtBalpha = 0.0;
#endif
#ifdef DIVBCLEANING_DEDNER
      SphP[i].Phi = SphP[i].PhiPred = SphP[i].DtPhi = 0;
#endif
#endif	

#ifdef TIME_DEP_ART_VISC
#ifdef HIGH_ART_VISC_START
      if(HIGH_ART_VISC_START == 0)
	SphP[i].alpha = All.ArtBulkViscConst;
      if(HIGH_ART_VISC_START > 0)
	if(P[i].Pos[0] > HIGH_ART_VISC_START)
	  SphP[i].alpha = All.ArtBulkViscConst;
	else
	  SphP[i].alpha = All.AlphaMin;
      if(HIGH_ART_VISC_START < 0)
	if(P[i].Pos[0] < -HIGH_ART_VISC_START)
	  SphP[i].alpha = All.ArtBulkViscConst;
	else
	  SphP[i].alpha = All.AlphaMin;
#else
      SphP[i].alpha = All.AlphaMin;
#endif
      SphP[i].Dtalpha = 0.0;
#endif

    }

  ngb_treeallocate(MAX_NGB);

  force_treeallocate(All.TreeAllocFactor * All.MaxPart, All.MaxPart);

  All.NumForcesSinceLastDomainDecomp = 1 + All.TotNumPart * All.TreeDomainUpdateFrequency;

  Flag_FullStep = 1;		/* to ensure that Peano-Hilbert order is done */

  domain_Decomposition();	/* do initial domain decomposition (gives equal numbers of particles) */

  ngb_treebuild();		/* will build tree */

  setup_smoothinglengths();

  TreeReconstructFlag = 1;

  /* At this point, the entropy variable normally contains the 
   * internal energy, read in from the initial conditions file, unless the file
   * explicitly signals that the initial conditions contain the entropy directly. 
   * Once the density has been computed, we can convert thermal energy to entropy.
   *
   * N.B. density() is called by setup_smoothinglengths(), so by this point particle
   * densities are known
   */

#ifdef CHEMCOOL
   /* Note: Entropy here is actually still specific energy */
   if (header.flag_entropy_instead_u == 1) {
     fprintf(stderr, "Error: CHEMCOOL needs specific energy!\n");
     endrun(0);
   }

   if (RestartFlag == 0) {
     /* Starting from IC file, so use InitGasTemp if available */
     for(i=0; i<N_gas; i++) {
       if (All.InitGasTemp > 0.) {
         abh2 = All.InitMolHydroAbund;
         temp = All.InitGasTemp;
#ifdef NO_VARIABLE_GAMMA
         gamma = 5./3.;
#else
         CALC_GAMMA_TEMP(&abh2, &temp, &gamma);
#endif
         SphP[i].Gamma = gamma;
         SphP[i].TempOut = temp;
       }
       else {
         abh2 = All.InitMolHydroAbund;
         ekn = SphP[i].Entropy * (1.0 + 4.0 * ABHE) * PROTONMASS / ((1.0 + ABHE - abh2) * BOLTZMANN);
         /* Convert to cgs */
         ekn *= All.UnitEnergy_in_cgs / All.UnitMass_in_g;
#ifdef NO_VARIABLE_GAMMA
         gamma = 5./3.;
#else
         CALC_GAMMA_TEMP(&abh2, &temp, &gamma);
#endif
         SphP[i].Gamma = gamma;
         CALC_TEMP(&abh2, &ekn, &temp);
         SphP[i].TempOut = temp;
       }
       SphP[i].GammaOut = SphP[i].Gamma;
     }
   }
   else {
     /* Restarting from a snapshot, so use actual H2 abundance */
     for(i=0; i<N_gas; i++) {
       abh2 = SphP[i].TracAbund[IH2];
       ekn = SphP[i].Entropy * (1.0 + 4.0 * ABHE) * PROTONMASS / ((1.0 + ABHE - abh2) * BOLTZMANN);
       ekn *= All.UnitEnergy_in_cgs / All.UnitMass_in_g;
#ifdef NO_VARIABLE_GAMMA
       gamma = 5./3.;
#else
       CALC_GAMMA_TEMP(&abh2, &temp, &gamma);
#endif       
       SphP[i].Gamma = gamma;
       SphP[i].GammaOut = SphP[i].Gamma;
       CALC_TEMP(&abh2, &ekn, &temp);
       SphP[i].TempOut = temp;
     }
   }

#endif /* CHEMCOOL */

#ifndef ISOTHERM_EQS
  /* With the polytopic EOS code, we directly specify pressure as a function of density, and
   * so there is no Entropy slot in the IC file
   */
#ifndef POLYTROPE
  if(header.flag_entropy_instead_u == 0)
    for(i = 0; i < N_gas; i++) {
#ifdef CHEMCOOL
      gamm1 = SphP[i].Gamma - 1.0;
      SphP[i].Entropy = gamm1 * SphP[i].Entropy / pow(SphP[i].Density / a3, gamm1);
      SphP[i].EntropyOut = SphP[i].Entropy;
#ifdef SEMI_IMPLICIT
      SphP[i].Ebeg   = SphP[i].Entropy;
      SphP[i].Efinal = SphP[i].Entropy;
#endif
#else
      SphP[i].Entropy = GAMMA_MINUS1 * SphP[i].Entropy / pow(SphP[i].Density / a3, GAMMA_MINUS1);
#endif /* CHEMCOOL */
    }
#endif /* POLYTROPE */
#endif /* ISOTHERM_EQS */

#if defined RAYTRACE && defined CHEMCOOL
  if (All.PhotochemApprox == 2) {
    raytrace();
  }
#endif /* RAYTRACE && CHEMCOOL */

#ifdef THERMAL_INFO_DUMP
  update_global_sink_data();
  for(i = 0; i < N_gas; i++) {
    if (P[i].ID < 0)
      continue;
    COOLI.id_current = P[i].ID;
    radius = sqrt(P[i].Pos[0] * P[i].Pos[0] + P[i].Pos[1] * P[i].Pos[1] + P[i].Pos[2] * P[i].Pos[2]);
    gaccr = fabs(P[i].GravAccel[0]*P[i].Pos[0] + P[i].GravAccel[1]*P[i].Pos[1] + P[i].GravAccel[2]*P[i].Pos[2]);
    gaccr /= radius;
    dummy = GetCoolTime(&SphP[i], radius, gaccr);
    SphP[i].H2LineCool = THERMALINFO.cool_h2_line;
    SphP[i].H2FormHeat = THERMALINFO.heat_h2_form;
    SphP[i].AccHeat = THERMALINFO.heat_accr;
    SphP[i].H2PdissHeat = THERMALINFO.heat_h2_pdiss;
    SphP[i].H2PumpHeat  = THERMALINFO.heat_h2_pump;
    SphP[i].CIICool = THERMALINFO.cool_cplus;
    SphP[i].OxyCool = THERMALINFO.cool_oxy;
    SphP[i].COCool = THERMALINFO.cool_co;
    SphP[i].i13COCool = THERMALINFO.cool_13co;
    SphP[i].i12COCool = THERMALINFO.cool_12co;
    SphP[i].CICool = THERMALINFO.cool_c;
    SphP[i].CosmicRayHeat = THERMALINFO.heat_cray;
    SphP[i].PhotoElecHeat = THERMALINFO.heat_photo_elec;
    SphP[i].GasDustCool = THERMALINFO.cool_gas_dust;
  }
#endif
}


/*! This routine computes the mass content of the box and compares it to the
 *  specified value of Omega-matter.  If discrepant, the run is terminated.
 */
void check_omega(void)
{
  double mass = 0, masstot, omega;
  int i;

  for(i = 0; i < NumPart; i++)
    mass += P[i].Mass;

  MPI_Allreduce(&mass, &masstot, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);

  omega =
    masstot / (All.BoxSize * All.BoxSize * All.BoxSize) / (3 * All.Hubble * All.Hubble / (8 * M_PI * All.G));

  if(fabs(omega - All.Omega0) > 1.0e-3)
    {
      if(ThisTask == 0)
	{
	  printf("\n\nI've found something odd!\n");
	  printf
	    ("The mass content accounts only for Omega=%g,\nbut you specified Omega=%g in the parameterfile.\n",
	     omega, All.Omega0);
	  printf("\nI better stop.\n");

	  fflush(stdout);
	}
      endrun(1);
    }
}



/*! This function is used to find an initial smoothing length for each SPH
 *  particle. It guarantees that the number of neighbours will be between
 *  desired_ngb-MAXDEV and desired_ngb+MAXDEV. For simplicity, a first guess
 *  of the smoothing length is provided to the function density(), which will
 *  then iterate if needed to find the right smoothing length.
 */
void setup_smoothinglengths(void)
{
  int i, no, p;

  if(RestartFlag == 0)
    {

      for(i = 0; i < N_gas; i++)
	{
	  if(P[i].ID < 0) /*SINK*/
	    continue;
	  no = Father[i];

	  while(10 * All.DesNumNgb * P[i].Mass > Nodes[no].u.d.mass)
	    {
	      p = Nodes[no].u.d.father;

	      if(p < 0)
		break;

	      no = p;
	    }
#ifndef TWODIMS
	  SphP[i].Hsml =
	    pow(3.0 / (4 * M_PI) * All.DesNumNgb * P[i].Mass / Nodes[no].u.d.mass, 1.0 / 3) * Nodes[no].len;
#else
	  SphP[i].Hsml =
	    pow(1.0 / (M_PI) * All.DesNumNgb * P[i].Mass / Nodes[no].u.d.mass, 1.0 / 2) * Nodes[no].len;
#endif
	}
    }

  density();

#if defined(MAGNETIC) && defined(BFROMROTA)
  if(RestartFlag == 0)
    {
      if(ThisTask == 0)
	printf("Converting: Vector Potential -> Bfield\n");
      rot_a();
    }
#endif

#if defined(MAGNETIC) && defined(BSMOOTH)
  if(RestartFlag == 0)
    bsmooth();
#endif

}


/*! If the code is run in glass-making mode, this function populates the
 *  simulation box with a Poisson sample of particles.
 */
#if (MAKEGLASS > 1)
void seed_glass(void)
{
  int i, k, n_for_this_task;
  double Range[3], LowerBound[3];
  double drandom, partmass;
  long long IDstart;

  All.TotNumPart = MAKEGLASS;
  partmass = All.Omega0 * (3 * All.Hubble * All.Hubble / (8 * M_PI * All.G))
    * (All.BoxSize * All.BoxSize * All.BoxSize) / All.TotNumPart;

  All.MaxPart = All.PartAllocFactor * (All.TotNumPart / NTask);	/* sets the maximum number of particles that may */

  allocate_memory();

  header.npartTotal[1] = All.TotNumPart;
  header.mass[1] = partmass;

  if(ThisTask == 0)
    {
      printf("\nGlass initialising\nPartMass= %g\n", partmass);
      printf("TotNumPart= %d%09d\n\n",
	     (int) (All.TotNumPart / 1000000000), (int) (All.TotNumPart % 1000000000));
    }

  /* set the number of particles assigned locally to this task */
  n_for_this_task = All.TotNumPart / NTask;

  if(ThisTask == NTask - 1)
    n_for_this_task = All.TotNumPart - (NTask - 1) * n_for_this_task;

  NumPart = 0;
  IDstart = 1 + (All.TotNumPart / NTask) * ThisTask;

  /* split the temporal domain into Ntask slabs in z-direction */

  Range[0] = Range[1] = All.BoxSize;
  Range[2] = All.BoxSize / NTask;
  LowerBound[0] = LowerBound[1] = 0;
  LowerBound[2] = ThisTask * Range[2];

  srand48(ThisTask);

  for(i = 0; i < n_for_this_task; i++)
    {
      for(k = 0; k < 3; k++)
	{
	  drandom = drand48();

	  P[i].Pos[k] = LowerBound[k] + Range[k] * drandom;
	  P[i].Vel[k] = 0;
	}

      P[i].Mass = partmass;
      P[i].Type = 1;
      P[i].ID = IDstart + i;

      NumPart++;
    }
}
#endif
