function avg_av, avarray, npix=npix
  @natconst.pro
	if not keyword_set(npix) then npix=48
	av_mean = alog10(sumarr(exp(avarray/-2.5d0)) / double(npix)) * (-2.5d0)
	return, av_mean
end
