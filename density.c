#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <mpi.h>

#include "allvars.h"
#include "proto.h"


/*! \file density.c 
 *  \brief SPH density computation and smoothing length determination
 *
 *  This file contains the "first SPH loop", where the SPH densities and
 *  some auxiliary quantities are computed.  If the number of neighbours
 *  obtained falls outside the target range, the correct smoothing
 *  length is determined iteratively, if needed.
 */


#ifdef PERIODIC
static double boxSize, boxHalf;

#ifdef LONG_X
static double boxSize_X, boxHalf_X;
#else
#define boxSize_X boxSize
#define boxHalf_X boxHalf
#endif
#ifdef LONG_Y
static double boxSize_Y, boxHalf_Y;
#else
#define boxSize_Y boxSize
#define boxHalf_Y boxHalf
#endif
#ifdef LONG_Z
static double boxSize_Z, boxHalf_Z;
#else
#define boxSize_Z boxSize
#define boxHalf_Z boxHalf
#endif
#endif


/*! This function computes the local density for each active SPH particle,
 *  the number of neighbours in the current smoothing radius, and the
 *  divergence and curl of the velocity field.  The pressure is updated as
 *  well.  If a particle with its smoothing region is fully inside the
 *  local domain, it is not exported to the other processors. The function
 *  also detects particles that have a number of neighbours outside the
 *  allowed tolerance range. For these particles, the smoothing length is
 *  adjusted accordingly, and the density computation is executed again.
 *  Note that the smoothing length is not allowed to fall below the lower
 *  bound set by MinGasHsml.
 */
void density(void)
{
  long long ntot, ntotleft;
  int *noffset, *nbuffer, *nsend, *nsend_local, *numlist, *ndonelist;
  int i, j, n, ndone, npleft, maxfill, source, iter = 0;
  int level, ngrp, sendTask, recvTask, place, nexport;
#ifdef CHEMCOOL
  double new_entropy, new_gamma, rho;
#endif
#ifdef IGM
  int metal_disperse;
  long long ntotsave;
#endif
  double dt_entr, tstart, tend, tstart_ngb = 0, tend_ngb = 0;
#ifdef SEMI_IMPLICIT
  double dt_semi, dt_full, entropy_new, entropy_asym;
#endif
  double sumt, sumcomm, timengb, sumtimengb;
  double timecomp = 0, timeimbalance = 0, timecommsumm = 0, sumimbalance;
  MPI_Status status;

#ifdef PERIODIC
  boxSize = All.BoxSize;
  boxHalf = 0.5 * All.BoxSize;
#ifdef LONG_X
  boxHalf_X = boxHalf * LONG_X;
  boxSize_X = boxSize * LONG_X;
#endif
#ifdef LONG_Y
  boxHalf_Y = boxHalf * LONG_Y;
  boxSize_Y = boxSize * LONG_Y;
#endif
#ifdef LONG_Z
  boxHalf_Z = boxHalf * LONG_Z;
  boxSize_Z = boxSize * LONG_Z;
#endif
#endif

#if defined(EULERPOTENTIALS) || defined(TRACEDIVB) 
  double efak, efak2;

  if(All.ComovingIntegrationOn)
    efak = 1. / All.Time / All.HubbleParam;
  else
    efak = 1;
#endif

  noffset = malloc(sizeof(int) * NTask);	/* offsets of bunches in common list */
  nbuffer = malloc(sizeof(int) * NTask);
  nsend_local = malloc(sizeof(int) * NTask);
  nsend = malloc(sizeof(int) * NTask * NTask);
  ndonelist = malloc(sizeof(int) * NTask);

  for(n = 0, NumSphUpdate = 0; n < N_gas; n++)
    {
      if(P[n].ID < 0) /*SINK*/
	continue;
      SphP[n].Left = SphP[n].Right = 0;

      if(P[n].Ti_endstep == All.Ti_Current)
	NumSphUpdate++;
    }

  numlist = malloc(NTask * sizeof(int) * NTask);
  MPI_Allgather(&NumSphUpdate, 1, MPI_INT, numlist, 1, MPI_INT, MPI_COMM_WORLD);
  for(i = 0, ntot = 0; i < NTask; i++)
    ntot += numlist[i];
  free(numlist);

#ifdef IGM
  ntotsave = ntot;
  for(metal_disperse = 0; metal_disperse < 2; metal_disperse++) {
    /* Don't disperse metals on first timestep */
    if (All.Ti_Current == 0 && metal_disperse == 1)
      continue;
    ntot = ntotsave;
#endif

  /* we will repeat the whole thing for those particles where we didn't
   * find enough neighbours
   */
  do
    {
      i = 0;			/* begin with this index */
      ntotleft = ntot;		/* particles left for all tasks together */

      while(ntotleft > 0)
	{
	  for(j = 0; j < NTask; j++)
	    nsend_local[j] = 0;

	  /* do local particles and prepare export list */
	  tstart = second();
	  for(nexport = 0, ndone = 0; i < N_gas && nexport < All.BunchSizeDensity - NTask; i++) {
	    if(P[i].ID < 0) /*SINK*/
	      continue;
	    if(P[i].Ti_endstep == All.Ti_Current)
	      {
		ndone++;

		for(j = 0; j < NTask; j++)
		  Exportflag[j] = 0;

#ifdef IGM
                if (metal_disperse == 0) {
                  density_evaluate(i, 0, 0);
                }
                else {
                  if (All.Ti_Current >= P[i].Ti_next_disperse && 
                      SphP[i].Metallicity > 0.0) {
                    density_evaluate(i, 0, 1);
                  }
                }
#else
                density_evaluate(i, 0);
#endif
		for(j = 0; j < NTask; j++)
		  {
		    if(Exportflag[j])
		      {
			DensDataIn[nexport].Pos[0] = P[i].Pos[0];
			DensDataIn[nexport].Pos[1] = P[i].Pos[1];
			DensDataIn[nexport].Pos[2] = P[i].Pos[2];
			DensDataIn[nexport].Vel[0] = SphP[i].VelPred[0];
			DensDataIn[nexport].Vel[1] = SphP[i].VelPred[1];
			DensDataIn[nexport].Vel[2] = SphP[i].VelPred[2];
			DensDataIn[nexport].Hsml = SphP[i].Hsml;
#ifdef IGM
                        DensDataIn[nexport].Density     = SphP[i].Density;
                        DensDataIn[nexport].Metallicity = SphP[i].Metallicity;
                        DensDataIn[nexport].Mass        = P[i].Mass;
#endif
#ifdef EULERPOTENTIALS
       	                DensDataIn[nexport].EulerA = SphP[i].EulerA;
	                DensDataIn[nexport].EulerB = SphP[i].EulerB;
#endif
#ifdef DIVBCLEANING_DEDNER
                   	DensDataIn[nexport].Jump_Entr =  SphP[i].Jump_Entr;
#endif

#ifdef TRACEDIVB
                        DensDataIn[nexport].BPred[0] = SphP[i].BPred[0];
	                DensDataIn[nexport].BPred[1] = SphP[i].BPred[1];
	                DensDataIn[nexport].BPred[2] = SphP[i].BPred[2];
#endif
			DensDataIn[nexport].Index = i;
			DensDataIn[nexport].Task = j;
			nexport++;
			nsend_local[j]++;
		      }
		  }
	      }
	  }
	  tend = second();
	  timecomp += timediff(tstart, tend);

	  qsort(DensDataIn, nexport, sizeof(struct densdata_in), dens_compare_key);

	  for(j = 1, noffset[0] = 0; j < NTask; j++)
	    noffset[j] = noffset[j - 1] + nsend_local[j - 1];

	  tstart = second();

	  MPI_Allgather(nsend_local, NTask, MPI_INT, nsend, NTask, MPI_INT, MPI_COMM_WORLD);

	  tend = second();
	  timeimbalance += timediff(tstart, tend);


	  /* now do the particles that need to be exported */

	  for(level = 1; level < (1 << PTask); level++)
	    {
	      tstart = second();
	      for(j = 0; j < NTask; j++)
		nbuffer[j] = 0;
	      for(ngrp = level; ngrp < (1 << PTask); ngrp++)
		{
		  maxfill = 0;
		  for(j = 0; j < NTask; j++)
		    {
		      if((j ^ ngrp) < NTask)
			if(maxfill < nbuffer[j] + nsend[(j ^ ngrp) * NTask + j])
			  maxfill = nbuffer[j] + nsend[(j ^ ngrp) * NTask + j];
		    }
		  if(maxfill >= All.BunchSizeDensity)
		    break;

		  sendTask = ThisTask;
		  recvTask = ThisTask ^ ngrp;

		  if(recvTask < NTask)
		    {
		      if(nsend[ThisTask * NTask + recvTask] > 0 || nsend[recvTask * NTask + ThisTask] > 0)
			{
			  /* get the particles */
			  MPI_Sendrecv(&DensDataIn[noffset[recvTask]],
				       nsend_local[recvTask] * sizeof(struct densdata_in), MPI_BYTE,
				       recvTask, TAG_DENS_A,
				       &DensDataGet[nbuffer[ThisTask]],
				       nsend[recvTask * NTask + ThisTask] * sizeof(struct densdata_in),
				       MPI_BYTE, recvTask, TAG_DENS_A, MPI_COMM_WORLD, &status);
			}
		    }

		  for(j = 0; j < NTask; j++)
		    if((j ^ ngrp) < NTask)
		      nbuffer[j] += nsend[(j ^ ngrp) * NTask + j];
		}
	      tend = second();
	      timecommsumm += timediff(tstart, tend);


	      tstart = second();
	      for(j = 0; j < nbuffer[ThisTask]; j++)
		{
#ifdef IGM
                if (metal_disperse == 0) {
                  density_evaluate(j, 1, 0);
                }
                else {
                  if (All.Ti_Current >= P[i].Ti_next_disperse && 
                      SphP[i].Metallicity > 0.0) {
                    density_evaluate(j, 1, 1);
                  }
                }
#else
                density_evaluate(j, 1);
#endif
		}
	      tend = second();
	      timecomp += timediff(tstart, tend);

	      /* do a block to explicitly measure imbalance */
	      tstart = second();
	      MPI_Barrier(MPI_COMM_WORLD);
	      tend = second();
	      timeimbalance += timediff(tstart, tend);

#ifdef IGM
              if (metal_disperse == 0) {
#endif
	      /* get the result */
	      tstart = second();
	      for(j = 0; j < NTask; j++)
		    nbuffer[j] = 0;
	      for(ngrp = level; ngrp < (1 << PTask); ngrp++)
		{
		  maxfill = 0;
		  for(j = 0; j < NTask; j++)
		    {
		      if((j ^ ngrp) < NTask)
			if(maxfill < nbuffer[j] + nsend[(j ^ ngrp) * NTask + j])
			  maxfill = nbuffer[j] + nsend[(j ^ ngrp) * NTask + j];
		    }
		  if(maxfill >= All.BunchSizeDensity)
		    break;

		  sendTask = ThisTask;
		  recvTask = ThisTask ^ ngrp;

		  if(recvTask < NTask)
		    {
		      if(nsend[ThisTask * NTask + recvTask] > 0 || nsend[recvTask * NTask + ThisTask] > 0)
			{
			  /* send the results */
			  MPI_Sendrecv(&DensDataResult[nbuffer[ThisTask]],
				       nsend[recvTask * NTask + ThisTask] * sizeof(struct densdata_out),
				       MPI_BYTE, recvTask, TAG_DENS_B,
				       &DensDataPartialResult[noffset[recvTask]],
				       nsend_local[recvTask] * sizeof(struct densdata_out),
				       MPI_BYTE, recvTask, TAG_DENS_B, MPI_COMM_WORLD, &status);

			  /* add the result to the particles */
			  for(j = 0; j < nsend_local[recvTask]; j++)
			    {
			      source = j + noffset[recvTask];
			      place = DensDataIn[source].Index;

			      SphP[place].NumNgb += DensDataPartialResult[source].Ngb;
			      SphP[place].Density += DensDataPartialResult[source].Rho;
			      SphP[place].DivVel += DensDataPartialResult[source].Div;

			      SphP[place].DhsmlDensityFactor += DensDataPartialResult[source].DhsmlDensity;

			      SphP[place].Rot[0] += DensDataPartialResult[source].Rot[0];
			      SphP[place].Rot[1] += DensDataPartialResult[source].Rot[1];
			      SphP[place].Rot[2] += DensDataPartialResult[source].Rot[2];

#ifdef TRACEDIVB
 		              SphP[place].divB += DensDataPartialResult[source].divB;
#endif

#ifdef DIVBCLEANING_DEDNER
      		              SphP[place].Max_Jump_Entr = 
		  	          DensDataPartialResult[source].Max_Jump_Entr > SphP[place].Max_Jump_Entr ? DensDataPartialResult[source].Max_Jump_Entr : SphP[place].Max_Jump_Entr;
#endif


#ifdef EULERPOTENTIALS
		              SphP[place].dEulerA[0] += DensDataPartialResult[source].dEulerA[0];
	           	      SphP[place].dEulerA[1] += DensDataPartialResult[source].dEulerA[1];
		              SphP[place].dEulerA[2] += DensDataPartialResult[source].dEulerA[2];
		              SphP[place].dEulerB[0] += DensDataPartialResult[source].dEulerB[0];
		              SphP[place].dEulerB[1] += DensDataPartialResult[source].dEulerB[1];
		              SphP[place].dEulerB[2] += DensDataPartialResult[source].dEulerB[2];
#endif
			    }
			}
		    }

		  for(j = 0; j < NTask; j++)
		    if((j ^ ngrp) < NTask)
		      nbuffer[j] += nsend[(j ^ ngrp) * NTask + j];
		}
	      tend = second();
	      timecommsumm += timediff(tstart, tend);
#ifdef IGM
              }
#endif
	      level = ngrp - 1;
	    }

	  MPI_Allgather(&ndone, 1, MPI_INT, ndonelist, 1, MPI_INT, MPI_COMM_WORLD);
	  for(j = 0; j < NTask; j++)
	    ntotleft -= ndonelist[j];
	}



      /* do final operations on results */
      tstart = second();
#ifdef IGM
      if (metal_disperse == 0) {
#endif
      for(i = 0, npleft = 0; i < N_gas; i++)
	{
	  if(P[i].ID < 0) /*SINK*/
	    continue;
	  if(P[i].Ti_endstep == All.Ti_Current)
	    {
	      {
		SphP[i].DhsmlDensityFactor =
		  1 / (1 + SphP[i].Hsml * SphP[i].DhsmlDensityFactor / (NUMDIMS * SphP[i].Density));

		SphP[i].CurlVel = sqrt(SphP[i].Rot[0] * SphP[i].Rot[0] +
				       SphP[i].Rot[1] * SphP[i].Rot[1] +
				       SphP[i].Rot[2] * SphP[i].Rot[2]) / SphP[i].Density;

		SphP[i].DivVel /= SphP[i].Density;

#ifdef TRACEDIVB
		SphP[i].divB /= SphP[i].Density;
#endif

#ifdef EULERPOTENTIALS
		SphP[i].dEulerA[0] *= efak / SphP[i].Density;
	        SphP[i].dEulerA[1] *= efak / SphP[i].Density;
	        SphP[i].dEulerA[2] *= efak / SphP[i].Density;
	        SphP[i].dEulerB[0] *= efak / SphP[i].Density;
	        SphP[i].dEulerB[1] *= efak / SphP[i].Density;
	        SphP[i].dEulerB[2] *= efak / SphP[i].Density;

                SphP[i].BPred[0] = SphP[i].dEulerA[1] * SphP[i].dEulerB[2] - SphP[i].dEulerA[2] * SphP[i].dEulerB[1];
                SphP[i].BPred[1] = SphP[i].dEulerA[2] * SphP[i].dEulerB[0] - SphP[i].dEulerA[0] * SphP[i].dEulerB[2];
                SphP[i].BPred[2] = SphP[i].dEulerA[0] * SphP[i].dEulerB[1] - SphP[i].dEulerA[1] * SphP[i].dEulerB[0];
#endif

#ifdef POLYTROPE
                SphP[i].Pressure = get_pressure(SphP[i].Density);
                if (SphP[i].Pressure <= 0.0) {
                  printf("Error in density: particle %d has a negative pressure!\n", P[i].ID);
                  printf("p = %g\n");
                  endrun(90211);
                }
#else /* POLYTROPE */
		dt_entr = (All.Ti_Current - (P[i].Ti_begstep + P[i].Ti_endstep) / 2) * All.Timebase_interval;
#ifdef CHEMCOOL
#ifdef SEMI_IMPLICIT
                dt_full = (P[i].Ti_endstep - P[i].Ti_begstep) * All.Timebase_interval;
                dt_semi = (All.Ti_Current  - P[i].Ti_begstep) * All.Timebase_interval;
                if (dt_full == 0.0) {
                  entropy_new = SphP[i].Entropy;
		}
                else if (dt_full < 1e-4 * SphP[i].DtTherm) {
		  /* Approximate exponential terms */
                  entropy_new = SphP[i].Ebeg * (1 - dt_semi / dt_full) + SphP[i].Efinal * dt_semi / dt_full;
		}
		else {
                  entropy_asym = (SphP[i].Efinal - SphP[i].Ebeg * exp(-dt_full / SphP[i].DtTherm)) 
                               / (1.0 - exp(-dt_full / SphP[i].DtTherm));
                  entropy_new  = SphP[i].Ebeg * exp(-dt_semi / SphP[i].DtTherm) + entropy_asym * (1.0 - exp(-dt_semi / SphP[i].DtTherm));
		}
		SphP[i].Pressure = entropy_new * pow(SphP[i].Density, SphP[i].Gamma);
#else
                new_entropy = SphP[i].Entropy + SphP[i].DtEntropy * dt_entr;
                rho = SphP[i].Density;
                if (All.ComovingIntegrationOn) {
		  rho = rho / (All.Time * All.Time * All.Time);
		}
                if (SphP[i].Gamma < 1.0 && All.Time != All.TimeBegin) {
		  /* The All.Time check stops us from crashing at startup, when we call density() before Gamma has been set */
                  printf("Weird gamma!\n");
                  printf("Particle index %d, particle ID %d, entropy %g, density %g, gamma %g\n", i, P[i].ID, SphP[i].Density, 
                         SphP[i].Entropy, SphP[i].Gamma);
                  endrun(101);
		}
                if (new_entropy < 0.0) {
                  printf("Particle %d has a negative entropy: ent = %g, dtent = %g, dt = %g\n", P[i].ID, SphP[i].Entropy, SphP[i].DtEntropy, dt_entr);
                  endrun(102);
		}
#ifdef NO_VARIABLE_GAMMA
                new_gamma = 5./3.;
#else
                if (SphP[i].Gamma >= 1.0) {
                  new_gamma = calc_gamma_from_entropy(new_entropy, rho, 
                              SphP[i].TracAbund[IH2], SphP[i].Gamma);
		}
		else {
		  new_gamma = SphP[i].Gamma;
		}
#endif
		SphP[i].Pressure = new_entropy * pow(SphP[i].Density, new_gamma);
                SphP[i].Gamma = new_gamma;
#endif /* SEMI_IMPLICIT */
#else /* CHEMCOOL */
		SphP[i].Pressure =
		  (SphP[i].Entropy + SphP[i].DtEntropy * dt_entr) * pow(SphP[i].Density, GAMMA);
#endif /* CHEMCOOL */
                if (SphP[i].Pressure <= 0.0) {
                  printf("Error in density: particle %d has a negative pressure!\n", P[i].ID);
                  printf("p = %g, ent = %g, dent_dt = %g, dt_entr = %g\n", SphP[i].Pressure, SphP[i].Entropy, SphP[i].DtEntropy, dt_entr);
#ifdef CHEMCOOL
#ifdef SEMI_IMPLICIT
                  printf("dt_therm = %g, ebeg = %g, efinal = %g, easym = %g, density = %g, gamma = %g\n", SphP[i].DtTherm, SphP[i].Ebeg, 
                         SphP[i].Efinal, entropy_asym, SphP[i].Density, SphP[i].Gamma);
#else
                  printf("density = %g, gamma = %g\n", SphP[i].Density, SphP[i].Gamma);
#endif
#else /* CHEMCOOL */
                  printf("density = %g, gamma = %g\n", SphP[i].Density, GAMMA);
#endif /* CHEMCOOL */
                  endrun(90211);
                }
#endif /* POLYTROPE */
	      }


             /*Check for errors in the smoothing length...*/

             if (SphP[i].Hsml <= 0)
               {
                 printf("Smoothing length problem for particle %d %g \n", P[i].ID, SphP[i].Hsml);
               }

	      /* now check whether we had enough neighbours */

	      if(SphP[i].NumNgb < (All.DesNumNgb - All.MaxNumNgbDeviation) ||
		 (SphP[i].NumNgb > (All.DesNumNgb + All.MaxNumNgbDeviation)
		  && SphP[i].Hsml > (1.01 * All.MinGasHsml)))
		{
		  /* need to redo this particle */
		  npleft++;

		  if(SphP[i].Left > 0 && SphP[i].Right > 0)
		    if((SphP[i].Right - SphP[i].Left) < 1.0e-3 * SphP[i].Left)
		      {
			/* this one should be ok */
			npleft--;
			P[i].Ti_endstep = -P[i].Ti_endstep - 1;	/* Mark as inactive */
			continue;
		      }

		  if(SphP[i].NumNgb < (All.DesNumNgb - All.MaxNumNgbDeviation))
		    SphP[i].Left = dmax(SphP[i].Hsml, SphP[i].Left);
		  else
		    {
		      if(SphP[i].Right != 0)
			{
			  if(SphP[i].Hsml < SphP[i].Right)
			    SphP[i].Right = SphP[i].Hsml;
			}
		      else
			SphP[i].Right = SphP[i].Hsml;
		    }

		  if(iter >= MAXITER - 10)
		    {
		      printf
			("i=%d task=%d ID=%d Hsml=%g Left=%g Right=%g Ngbs=%g Right-Left=%g\n   pos=(%g|%g|%g)\n",
			 i, ThisTask, (int) P[i].ID, SphP[i].Hsml, SphP[i].Left, SphP[i].Right,
			 (float) SphP[i].NumNgb, SphP[i].Right - SphP[i].Left, P[i].Pos[0], P[i].Pos[1],
			 P[i].Pos[2]);
		      fflush(stdout);
		    }

		  if(SphP[i].Right > 0 && SphP[i].Left > 0)
		    SphP[i].Hsml = pow(0.5 * (pow(SphP[i].Left, 3) + pow(SphP[i].Right, 3)), 1.0 / 3);
		  else
		    {
		      if(SphP[i].Right == 0 && SphP[i].Left == 0)
			endrun(8188);	/* can't occur */

		      if(SphP[i].Right == 0 && SphP[i].Left > 0)
			{
			  if(P[i].Type == 0 && fabs(SphP[i].NumNgb - All.DesNumNgb) < 0.5 * All.DesNumNgb)
			    {
			      SphP[i].Hsml *=
				1 - (SphP[i].NumNgb -
				     All.DesNumNgb) / (NUMDIMS * SphP[i].NumNgb) * SphP[i].DhsmlDensityFactor;
			    }
			  else
			    SphP[i].Hsml *= 1.26;
			}

		      if(SphP[i].Right > 0 && SphP[i].Left == 0)
			{
			  if(P[i].Type == 0 && fabs(SphP[i].NumNgb - All.DesNumNgb) < 0.5 * All.DesNumNgb)
			    {
			      SphP[i].Hsml *=
				1 - (SphP[i].NumNgb -
				     All.DesNumNgb) / (NUMDIMS * SphP[i].NumNgb) * SphP[i].DhsmlDensityFactor;
			    }
			  else
			    SphP[i].Hsml /= 1.26;
			}
		    }

		  if(SphP[i].Hsml < All.MinGasHsml)
		    SphP[i].Hsml = All.MinGasHsml;
		}
	      else
		P[i].Ti_endstep = -P[i].Ti_endstep - 1;	/* Mark as inactive */
	    }
	}
#ifdef IGM
      }
      else {
       ntot = 0;
      }
#endif
      tend = second();
      timecomp += timediff(tstart, tend);

#ifdef IGM
      if (metal_disperse == 0) {
#endif
      numlist = malloc(NTask * sizeof(int) * NTask);
      MPI_Allgather(&npleft, 1, MPI_INT, numlist, 1, MPI_INT, MPI_COMM_WORLD);
      for(i = 0, ntot = 0; i < NTask; i++)
	ntot += numlist[i];
      free(numlist);

      if(ntot > 0)
	{
	  if(iter == 0)
	    tstart_ngb = second();

	  iter++;

	  if(iter > 0 && ThisTask == 0)
	    {
	      printf("ngb iteration %d: need to repeat for %d%09d particles.\n", iter,
		     (int) (ntot / 1000000000), (int) (ntot % 1000000000));
	      fflush(stdout);
	    }

	  if(iter > MAXITER)
	    {
	      printf("failed to converge in neighbour iteration in density()\n");
	      fflush(stdout);
	      endrun(1155);
	    }
	}
      else
	tend_ngb = second();
#ifdef IGM
      }
#endif
    }
  while(ntot > 0);


  /* mark as active again */
  for(i = 0; i < NumPart; i++)
    {
      if(P[i].Type == 0 && P[i].ID < 0) /*SINK*/
        continue;
#ifdef IGM
      if (P[i].Type == 0 && metal_disperse == 1) {
        SphP[i].Metallicity = SphP[i].NewMetallicity;
        SphP[i].NewMetallicity = 0.;
      }
#endif
      if(P[i].Ti_endstep < 0)
        P[i].Ti_endstep = -P[i].Ti_endstep - 1;
    }
#ifdef IGM
  }
#endif
  free(ndonelist);
  free(nsend);
  free(nsend_local);
  free(nbuffer);
  free(noffset);


  /* collect some timing information */
  if(iter > 0)
    timengb = timediff(tstart_ngb, tend_ngb);
  else
    timengb = 0;

  MPI_Reduce(&timengb, &sumtimengb, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
  MPI_Reduce(&timecomp, &sumt, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
  MPI_Reduce(&timecommsumm, &sumcomm, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
  MPI_Reduce(&timeimbalance, &sumimbalance, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);

  if(ThisTask == 0)
    {
      All.CPU_HydCompWalk += sumt / NTask;
      All.CPU_HydCommSumm += sumcomm / NTask;
      All.CPU_HydImbalance += sumimbalance / NTask;
      All.CPU_EnsureNgb += sumtimengb / NTask;
    }
}



/*! This function represents the core of the SPH density computation. The
 *  target particle may either be local, or reside in the communication
 *  buffer.
 */
#ifdef IGM
void density_evaluate(int target, int mode, int metal_disperse)
#else
 void density_evaluate(int target, int mode)
#endif
{
  int j, n, startnode, numngb, numngb_inbox;
  double h, h2, fac, hinv, hinv3, hinv4;
  double rho, divv, wk, dwk;
  double dx, dy, dz, r, r2, u, mass_j;
  double dvx, dvy, dvz, rotv[3];
  double weighted_numngb, dhsmlrho;
  FLOAT *pos, *vel;
#ifdef IGM
  double frac, dZ, metal_mass;
#endif
  static FLOAT veldummy[3] = { 0, 0, 0 };

#ifdef EULERPOTENTIALS
  double deulera[3], deulerb[3], eulera, eulerb, dea, deb;
  deulera[0] = deulera[1] = deulera[2] = deulerb[0] = deulerb[1] = deulerb[2] = 0;
#endif

#if defined(TRACEDIVB)
  FLOAT *bflt;
  double dbx, dby, dbz;
#endif

#ifdef TRACEDIVB
  double divB = 0;
#endif

#ifdef DIVBCLEANING_DEDNER
  double max_jump_entr;
#endif

  if(mode == 0)
    {
      pos = P[target].Pos;
      //vel = SphP[target].VelPred;
      h = SphP[target].Hsml;
      if(P[target].Type == 0)
        {
          vel = SphP[target].VelPred;
#ifdef TRACEDIVB
	  bflt = SphP[target].BPred;
#endif
#ifdef EULERPOTENTIALS
	  eulera = SphP[target].EulerA;
	  eulerb = SphP[target].EulerB;
#endif
        }
      else
	vel = veldummy;
#ifdef DIVBCLEANING_DEDNER
      max_jump_entr = SphP[target].Jump_Entr;
#endif
    }
  else
    {
      pos = DensDataGet[target].Pos;
      vel = DensDataGet[target].Vel;
      h   = DensDataGet[target].Hsml;
#ifdef TRACEDIVB
      bflt = DensDataGet[target].BPred;
#endif
#ifdef EULERPOTENTIALS
      eulera = DensDataGet[target].EulerA;
      eulerb = DensDataGet[target].EulerB;
#endif
#ifdef DIVBCLEANING_DEDNER
      max_jump_entr = DensDataGet[target].Jump_Entr;
#endif
    }

  h2 = h * h;
  hinv = 1.0 / h;
#ifndef  TWODIMS
  hinv3 = hinv * hinv * hinv;
#else
  hinv3 = hinv * hinv / boxSize_Z;
#endif
  hinv4 = hinv3 * hinv;

  rho = divv = rotv[0] = rotv[1] = rotv[2] = 0;
  weighted_numngb = 0;
  dhsmlrho = 0;

  startnode = All.MaxPart;
  numngb = 0;
  do
    {
      numngb_inbox = ngb_treefind_variable(&pos[0], h, &startnode);

      for(n = 0; n < numngb_inbox; n++)
	{
	  j = Ngblist[n];

	  if(P[j].ID < 0)
	    continue;

	  dx = pos[0] - P[j].Pos[0];
	  dy = pos[1] - P[j].Pos[1];
	  dz = pos[2] - P[j].Pos[2];

#ifdef PERIODIC			/*  now find the closest image in the given box size  */
	  if(dx > boxHalf_X)
	    dx -= boxSize_X;
	  if(dx < -boxHalf_X)
	    dx += boxSize_X;
	  if(dy > boxHalf_Y)
	    dy -= boxSize_Y;
	  if(dy < -boxHalf_Y)
	    dy += boxSize_Y;
	  if(dz > boxHalf_Z)
	    dz -= boxSize_Z;
	  if(dz < -boxHalf_Z)
	    dz += boxSize_Z;
#endif
	  r2 = dx * dx + dy * dy + dz * dz;

	  if(r2 < h2)
	    {
	      numngb++;

	      r = sqrt(r2);

	      u = r * hinv;
/*
	      if(u < 0.5)
		{
		  wk = hinv3 * (KERNEL_COEFF_1 + KERNEL_COEFF_2 * (u - 1.0) * u * u);
		  dwk = hinv4 * u * (KERNEL_COEFF_3 * u - KERNEL_COEFF_4);
		}
	      else
		{
		  wk = hinv3 * KERNEL_COEFF_5 * (1.0 - u) * (1.0 - u) * (1.0 - u);
		  dwk = hinv4 * KERNEL_COEFF_6 * (1.0 - u) * (1.0 - u);
		}
*/
#if defined(CORE_TRIANGLE_KERNEL)
	if(u < 0.33333333)
          {
            wk = hinv3 * 2.535349981 * (-2.0 * u + 1.22222222);
            dwk = hinv4 * (-5.070699962);
          }
        else if( (u > 0.33333333) && (u < 0.5) )
          {
            wk = hinv3 * (2.535349981 + 15.21209989 * (u - 1.0) * u * u);
            dwk = hinv4 * u * ( 45.63629966 * u - 30.42419977); 
          }
        else
          {
            wk = hinv3 * 5.070699962 * (1.0 - u) * (1.0 - u) * (1.0 - u);
            dwk = hinv4 * (-15.21209989) * (1.0 - u) * (1.0 - u);
          }
#elif defined(QUINTIC_KERNEL)

	if(u < 0.33333333)
          {
            wk = hinv3 * 16.32599123 * ( pow(1.0-u,5.0) - 4.66666666*pow(0.66666666 - u,5.0) + 6.33333333*pow(0.33333333 - u,5.0) );
            dwk = hinv4 * 16.32599123 * ( -5.0*pow(1.0-u,4.0) + 23.33333333*pow(0.66666666 - u,4.0) - 31.66666666*pow(0.33333333 - u,4.0) );
          }
        else if( (u > 0.33333333) && (u < 0.66666666) )
          {
            wk = hinv3 * 16.32599123 * ( pow(1.0-u,5.0) - 4.66666666*pow(0.66666666 - u,5.0) );
            dwk = hinv4 * 16.32599123 * ( -5.0*pow(1.0-u,4.0) + 23.33333333*pow(0.66666666 - u,4.0) );
          }
        else
          {
            wk = hinv3 * 16.32599123 * ( pow(1.0-u,5.0) );
            dwk = hinv4 * 16.32599123 * ( -5.0*pow(1.0-u,4.0) );
          }
#else
        if(u < 0.5)
          {
            wk = hinv3 * (KERNEL_COEFF_1 + KERNEL_COEFF_2 * (u - 1.0) * u * u);
            dwk = hinv4 * u * (KERNEL_COEFF_3 * u - KERNEL_COEFF_4);
          }
        else
          {
            wk = hinv3 * KERNEL_COEFF_5 * (1.0 - u) * (1.0 - u) * (1.0 - u);
            dwk = hinv4 * KERNEL_COEFF_6 * (1.0 - u) * (1.0 - u);
          }
#endif
             mass_j = P[j].Mass;
#ifdef IGM
             if (metal_disperse == 1) {
               if (mode == 0) {
                  frac = mass_j * wk / SphP[target].Density;
                  dZ = frac * SphP[target].Metallicity;
                  metal_mass = dZ * P[target].Mass;
               }
               else {
                 frac = mass_j * wk / DensDataGet[target].Density;
                 dZ = frac * DensDataGet[target].Metallicity;
                 metal_mass = dZ * DensDataGet[target].Mass; 
	       }
               SphP[j].NewMetallicity += metal_mass / P[j].Mass;
	     }
	     else {
#endif
	      rho += mass_j * wk;
	      weighted_numngb += NORM_COEFF * wk / hinv3;

	      dhsmlrho += -mass_j * (NUMDIMS * hinv * wk + u * dwk);

	      if(r > 0)
		{
		  fac = mass_j * dwk / r;

		  dvx = vel[0] - SphP[j].VelPred[0];
		  dvy = vel[1] - SphP[j].VelPred[1];
		  dvz = vel[2] - SphP[j].VelPred[2];

		  divv -= fac * (dx * dvx + dy * dvy + dz * dvz);

		  rotv[0] += fac * (dz * dvy - dy * dvz);
		  rotv[1] += fac * (dx * dvz - dz * dvx);
		  rotv[2] += fac * (dy * dvx - dx * dvy);
		}
	      /* XXX: Shouldn't this next be inside the if-statement too? */
#ifdef TRACEDIVB
		dbx = bflt[0] - SphP[j].BPred[0];
		dby = bflt[1] - SphP[j].BPred[1];
	        dbz = bflt[2] - SphP[j].BPred[2];
	        divB -= fac * (dx * dbx + dy * dby + dz * dbz);
#endif
#ifdef DIVBCLEANING_DEDNER
      		max_jump_entr = max_jump_entr > SphP[j].Jump_Entr ? max_jump_entr : SphP[j].Jump_Entr;
#endif
#ifdef EULERPOTENTIALS
	        dea = eulera - SphP[j].EulerA;
	        deb = eulerb - SphP[j].EulerB;
#ifdef PERIODIC
		/* XXX: Why do we only update deb here? */
		/* XXX: Actually, why are we updating deb here by adding/subtracting a distance? I don't follow the intent of this
		 *      code at all!
		 */
//              dea = (dea > 0.0000813*boxHalf_X) ? (dea - 0.0000813*boxSize_X) : ((dea < -0.0000813*boxHalf_X) ? (dea + 0.0000813*boxSize_X) : dea);
//              deb = (deb > boxHalf_Y) ? (deb - boxSize_Y) : ((deb < -boxHalf_Y) ? (deb + boxSize_Y) : deb);
                deb = (deb > boxHalf_Z) ? (deb - boxSize_Z) : ((deb < -boxHalf_Z) ? (deb + boxSize_Z) : deb);
#endif
                deulera[0] -= fac * dx * dea;
	        deulera[1] -= fac * dy * dea;
		deulera[2] -= fac * dz * dea;
	        deulerb[0] -= fac * dx * deb;
	        deulerb[1] -= fac * dy * deb;
	        deulerb[2] -= fac * dz * deb;
#endif
#ifdef IGM
                }
#endif
	    }
	}
    }
  while(startnode >= 0);

#ifdef IGM
  if (metal_disperse == 0) {
#endif
  if(mode == 0)
    {
      SphP[target].NumNgb = weighted_numngb;
      SphP[target].Density = rho;
      SphP[target].DivVel = divv;
      SphP[target].DhsmlDensityFactor = dhsmlrho;
      SphP[target].Rot[0] = rotv[0];
      SphP[target].Rot[1] = rotv[1];
      SphP[target].Rot[2] = rotv[2];

#ifdef TRACEDIVB
	  SphP[target].divB = divB;
#endif
#ifdef EULERPOTENTIALS
	  SphP[target].dEulerA[0] = deulera[0];
	  SphP[target].dEulerA[1] = deulera[1];
	  SphP[target].dEulerA[2] = deulera[2];
	  SphP[target].dEulerB[0] = deulerb[0];
	  SphP[target].dEulerB[1] = deulerb[1];
	  SphP[target].dEulerB[2] = deulerb[2];
#endif
#ifdef DIVBCLEANING_DEDNER
      	  SphP[target].Max_Jump_Entr = max_jump_entr;
#endif
    }
  else
    {
      DensDataResult[target].Rho = rho;
      DensDataResult[target].Div = divv;
      DensDataResult[target].Ngb = weighted_numngb;
      DensDataResult[target].DhsmlDensity = dhsmlrho;
      DensDataResult[target].Rot[0] = rotv[0];
      DensDataResult[target].Rot[1] = rotv[1];
      DensDataResult[target].Rot[2] = rotv[2];
#ifdef TRACEDIVB
      DensDataResult[target].divB = divB;
#endif

#ifdef EULERPOTENTIALS
      DensDataResult[target].dEulerA[0] = deulera[0];
      DensDataResult[target].dEulerA[1] = deulera[1];
      DensDataResult[target].dEulerA[2] = deulera[2];
      DensDataResult[target].dEulerB[0] = deulerb[0];
      DensDataResult[target].dEulerB[1] = deulerb[1];
      DensDataResult[target].dEulerB[2] = deulerb[2];
#endif
#ifdef DIVBCLEANING_DEDNER
      DensDataResult[target].Max_Jump_Entr = max_jump_entr;
#endif
    }
#ifdef IGM
  }
#endif
}




/*! This routine is a comparison kernel used in a sort routine to group
 *  particles that are exported to the same processor.
 */
int dens_compare_key(const void *a, const void *b)
{
  if(((struct densdata_in *) a)->Task < (((struct densdata_in *) b)->Task))
    return -1;

  if(((struct densdata_in *) a)->Task > (((struct densdata_in *) b)->Task))
    return +1;

  return 0;
}
