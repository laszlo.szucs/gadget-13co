
#ifdef HISTORY
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <float.h>
#include <mpi.h>

#include "allvars.h"
#include "proto.h"

void dump_history (int index) {
  int i, ID;
  FLOAT time, density, temperature;
  FLOAT gamma_old, gamma_current, entropy, energy, energy_cgs;
  FLOAT n, n_cgs, ntot, abh2, ekn;

  /* Dump time, particle ID, density, temperature (or entropy), abundances, column densities
   * All data is dumped in code units
   */
  time    = All.Time;
  ID      = P[index].ID;
  density = SphP[index].Density;
  fprintf(FdHist, "%d  %g  %g  ", ID, time, density);

#ifndef CHEMCOOL
  fprintf(FdHist, "%g  ", SphP[index].Entropy);
#else
  entropy = SphP[index].Entropy;

  /* Ensure consistent gamma */
#ifdef NO_VARIABLE_GAMMA
  gamma_current = 5./3.;
#else
  gamma_old = SphP[index].Gamma;
  gamma_current = calc_gamma_from_entropy(entropy, density, SphP[index].TracAbund[IH2], gamma_old);
#endif

  energy = entropy * pow(density, gamma_current) / (gamma_current - 1.0);
  n = density / ((1.0 + 4.0 * ABHE) * PROTONMASS);

  /* Convert to cgs */
  energy_cgs = energy * All.UnitEnergy_in_cgs / pow(All.UnitLength_in_cm, 3);
  n_cgs = n * All.UnitDensity_in_cgs;

  abh2 = SphP[index].TracAbund[IH2];

  /* NB Valid for all chemistry networks */
  ntot = (1.0 + ABHE - abh2 + SphP[index].TracAbund[IHP]) * n_cgs;
  ekn  = energy_cgs / (ntot * BOLTZMANN);
  CALC_TEMP(&abh2, &ekn, &temperature);

  fprintf(FdHist, "%g  ", temperature);

  fprintf(FdHist, "%g  ", SphP[index].DustTemp);

  for (i=0; i<TRAC_NUM; i++) {
    fprintf(FdHist, "%g  ", SphP[index].TracAbund[i]);
  }
#endif /* !CHEMCOOL */
#ifdef TREE_RAD
  for (i=0; i<NPIX; i++) {
    fprintf(FdHist, "%g  ", SphP[index].Projection[i]);
  }
#endif
#ifdef TREE_RAD_H2
  for (i=0; i<NPIX; i++) {
    fprintf(FdHist, "%g  ", SphP[index].ProjectionH2[i]);
  }
  for (i=0; i<NPIX; i++) {
    fprintf(FdHist, "%g  ", SphP[index].ProjectionCO[i]);
  }
#ifdef _13CO_
  for (i=0; i<NPIX; i++) {
    fprintf(FdHist, "%g  ", SphP[index].Projection13CO[i]);
  }
#endif
#endif
  fprintf(FdHist, "%g  ", SphP[index].Hsml); /* Write out the smoothing length*/
  fprintf(FdHist, "\n");
  return;
}



void history_init(void)
{
  int i, j, np, id;
  int *id_list;
  char buf[200];

  if (ThisTask == 0) {

  /* Open ID list file -- i.e. list of IDs for which we want to dump histories */
    if(!(FdIDList = fopen("id-list.txt", "r")))
      {
        printf("error in opening file 'id-list.txt'\n");
        endrun(1);
      }

  /* Get size of ID list and allocate storage */
    fscanf(FdIDList, "%d", &np);
  }

  MPI_Bcast(&np, 1, MPI_INT, 0, MPI_COMM_WORLD);

  id_list = malloc(sizeof(int) * np);

  if (ThisTask == 0) {
    /* Read SORTED list of IDs */
    for (i=0; i<np; i++) {
      fscanf(FdIDList, "%d", &id);
      id_list[i] = id;
      printf("ID = %d\n", id);
      fflush(stdout);
    }
    fclose(FdIDList);
    printf("finished reading IDs\n");
  }
  MPI_Bcast(id_list, np, MPI_INT, 0, MPI_COMM_WORLD);

  /* Loop over particles, set history output flag */

  for (i = 0; i < N_gas; i++) {
    SphP[i].history_output_required = 0;
    for (j = 0; j < np; j++) {
      if (P[i].ID == id_list[j]) {
	SphP[i].history_output_required = 1;
        break;
      }
      else if (P[i].ID < id_list[j]) {
	break;
      }
    }
  }

  sprintf(buf, "history.%d", ThisTask);
  /* Open history output list */
  if (!(FdHist = fopen(buf, "a"))) {
      printf("error in opening history output file, Task = %d\n", ThisTask);
      endrun(1);
  }

  free(id_list);
  printf("Done history init\n");
  fflush(stdout);
  return;
}




#endif /* HISTORY */
