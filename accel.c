#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <mpi.h>

#include "allvars.h"
#include "proto.h"

/*! \file accel.c
 *  \brief driver routine to carry out force computation
 */


/*! This routine computes the accelerations for all active particles.
 *  First, the long-range PM force is computed if the TreePM algorithm is
 *  used and a "big" PM step is done.  Next, the gravitational tree forces
 *  are computed. This also constructs the tree, if needed.
 *
 *  If gas particles are present, the density-loop for active SPH particles
 *  is carried out. This includes an iteration on the correct number of
 *  neighbours.  Finally, the hydrodynamical forces are added.
 */
void compute_accelerations(int mode)
{
  double tstart, tend;
  int i;
#if defined TREE_RAD || defined TREE_RAD_H2  
  int update_time;
#endif
  if(ThisTask == 0)
    {
      printf("Start force computation...\n");
      fflush(stdout);
    }

#ifdef PMGRID
  if(All.PM_Ti_endstep == All.Ti_Current)
    {
      tstart = second();
      long_range_force();
      tend = second();
      All.CPU_PM += timediff(tstart, tend);
    }
#endif

  tstart = second();		/* measure the time for the full force computation */

  gravity_tree();		/* computes gravity accel. */

  if(All.TypeOfOpeningCriterion == 1 && All.Ti_Current == 0)
    gravity_tree();		/* For the first timestep, we redo it
				 * to allow usage of relative opening
				 * criterion for consistent accuracy.
				 */
  tend = second();
  All.CPU_Gravity += timediff(tstart, tend);

#ifdef FORCETEST
  gravity_forcetest();
#endif

#ifdef DMPROFILE
  dm_profile();                  /* Static DM potential */
#endif

#ifdef GALACTIC_POTENTIAL
  galactic_potential();
#endif

  if(All.TotN_gas > 0)
    {
      if(ThisTask == 0)
	{
	  printf("Start density computation...\n");
	  fflush(stdout);
	}

      tstart = second();
      density();		/* computes density, and pressure */
      tend = second();
      All.CPU_Hydro += timediff(tstart, tend);

      tstart = second();
      force_update_hmax();      /* tell the tree nodes the new SPH smoothing length such that they are guaranteed to hold the correct max(Hsml) */
      tend = second();
      All.CPU_Predict += timediff(tstart, tend);


      if(ThisTask == 0)
	{
	  printf("Start hydro-force computation...\n");
	  fflush(stdout);
	}

      tstart = second();
      hydro_force();		/* adds hydrodynamical accelerations and computes viscous entropy injection  */
      tend = second();
      All.CPU_Hydro += timediff(tstart, tend);

#if defined(TREE_RAD) || defined(TREE_RAD_H2)
#ifdef TIME_UPDATE_COLUMN
      /* Now that the particles have been through the tree, they should have a
         new value of the column densities. These are kept for the next h/vsig
         before being updated once more. */
      if (ThisTask == 0 ) {
         printf("TREE_RAD Allocating new update time for active particles \n");
       }
      for(i = 0; i < N_gas; i++) {
         if (i == 0) {
            printf("TREE_RAD: time assigned %d particle time %d current code time %d \n", (int) (SphP[i].Hsml/SphP[i].MaxSignalVel/All.Timebase_interval), P[i].Ti_endstep, All.Ti_Current);
         }
         if( P[i].Ti_endstep >= SphP[i].time_update_column ) {
            update_time = (int) (SphP[i].Hsml/SphP[i].MaxSignalVel/All.Timebase_interval);
            SphP[i].time_update_column = P[i].Ti_endstep + update_time;
         }
      }
#endif
#endif
    }

  if(ThisTask == 0)
    {
      printf("force computation done.\n");
      fflush(stdout);
    }
}
