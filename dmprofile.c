#ifdef DMPROFILE

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <mpi.h>
#include <unistd.h>
#include <limits.h>

#include "allvars.h"
#include "proto.h"


/*  Adds a contribution to the gravitational accelerations
    from a static DM radial distribution. At the moment
    it reads in a radial mass profile and calculates the 
    accels based on the interior mass. Obviously this can be
    altered in the future to do analytic potentials too. The
    radial mass profile is stored in logarithmically spaced
    bins in this  implementation.
    
    We assume that G in code units is 1 and that everything has
    been centred on the densest particle/center of mass...
*/
     
void dm_profile(void)
{
   int i, ishell;
   int nshells;
   int ibin;
   double rmax, rmin;
   double a = 0.263492;
   double b = -1.34905;
   double c = 3.75898;
   double d = -0.815028;
   double logr, logm;
   double menc_i;
   double gacc_rad;
   double dx, dy, dz, rad;
   static int ifirsttime = 1;
   char buf[200];
   FILE *fd;

   if (ThisTask == 0)
      printf("Adding gacc contributions from dark matter profile... \n");


   /* Loop around particles and get accels... */

   for (i=0; i < N_gas; i++) {
      if ( P[i].Ti_endstep == All.Ti_Current ) {
         /* Find shell. Note that if r<rmin, we assume NO dm mass interior! */ 
   
         if (P[i].Type != 0 || P[i].ID < 0) continue;
         dx = P[i].Pos[0];
         dy = P[i].Pos[1];
         dz = P[i].Pos[2];
         rad = sqrt(dx*dx + dy*dy + dz*dz);
         logr = log10(rad);
         logm = a*pow(logr, 3) + b*pow(logr, 2) + c*logr + d;
         menc_i = pow(10.0, logm);
         /* Calculate the accelerations. */
      
         gacc_rad = -menc_i/rad/rad;
        /* if (i == 1000) 
            printf("Particle %d before DM %g %g %g %g %g %d\n", P[i].ID, P[i].GravAccel[0], P[i].GravAccel[1], P[i].GravAccel[2], menc_i, rad, ibin);
         */   

         P[i].GravAccel[0] += gacc_rad*dx/rad;
         P[i].GravAccel[1] += gacc_rad*dy/rad;
         P[i].GravAccel[2] += gacc_rad*dz/rad;
 
         /* if (i == 1000) 
            printf("Particle %d after DM %g %g %g \n", P[i].ID, P[i].GravAccel[0], P[i].GravAccel[1], P[i].GravAccel[2]);
         */
      }
   }
  
   if (ThisTask == 0)
      printf("Done with radial dark matter contributions \n");

}
#endif
