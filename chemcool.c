
#ifdef CHEMCOOL

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <mpi.h>
#include "allvars.h"
#include "proto.h"
#include "f2c.h"

#ifdef PERIODIC
static double boxSize, boxHalf;

#ifdef LONG_X
static double boxSize_X, boxHalf_X;
#else
#define boxSize_X boxSize
#define boxHalf_X boxHalf
#endif
#ifdef LONG_Y
static double boxSize_Y, boxHalf_Y;
#else
#define boxSize_Y boxSize
#define boxHalf_Y boxHalf
#endif
#ifdef LONG_Z
static double boxSize_Z, boxHalf_Z;
#else
#define boxSize_Z boxSize
#define boxHalf_Z boxHalf
#endif
#endif

#define NGB_PERIODIC_X(x) (xtmp=(x),(xtmp>boxHalf_X)?(xtmp-boxSize_X):((xtmp<-boxHalf_X)?(xtmp+boxSize_X):xtmp))
#define NGB_PERIODIC_Y(x) (xtmp=(x),(xtmp>boxHalf_Y)?(xtmp-boxSize_Y):((xtmp<-boxHalf_Y)?(xtmp+boxSize_Y):xtmp))
#define NGB_PERIODIC_Z(x) (xtmp=(x),(xtmp>boxHalf_Z)?(xtmp-boxSize_Z):((xtmp<-boxHalf_Z)?(xtmp+boxSize_Z):xtmp))


/*initialization of chemcool*/
void chemcool_init(void)
{
  if(ThisTask==0)
    {
      printf("initialize cooling functions...\n");
      fflush(stdout);
    }

  COOLINMO();
  CHEMINMO();
  INIT_TOLERANCES();
  LOAD_H2_TABLE();
  INIT_TEMPERATURE_LOOKUP();
#ifdef DEBUG_SIGMA_PCC
  OPEN_DEBUG_OUTPUT();
#endif
  if(ThisTask==0)
    {
      printf("initialization of cooling functions finished.\n");
      fflush(stdout);
    }
}

/* Compute new entropy and abundances at end of timestep dt. 
 * Mode = 0 ==> update TracAbundOut, EntropyOut, GammaOut, TempOut, DustTempOut
 * Mode = 1 ==> update TracAbund, Entropy, Gamma, DustTemp
 * Mode = 2 ==> update nothing
 * Mode = 3 ==> update Entropy
 */
double do_chemcool_step(double dt, struct sph_particle_data *current_particle, double radius, double gaccr, int mode)
{
  double entropy_init, entropy_after_visc_update, entropy_new;
  double rho, dl, timestep, divv, energy, gamma, ekn, energy_cgs;
  double old_gamma, ctime;
  double temp;
  double energy_cgs_init;
  double hubble_a = 1;
  double yn, a3inv, abh2, abhd, abco;
  double abundances[TRAC_NUM];
  double pdv_heating;
  double column_est;
  double mcloud, rcloud, mgrav, rext;
  double rpar[NRPAR], y[NSPEC], ydot[NSPEC], t_start, shield[3];
  int nsp, ipar[NIPAR];
  /* Used for accretion luminosity feedback */
  double sink_mass, sink_mdot, stellar_radius, accretion_luminosity;
  double sink_dx, sink_dy, sink_dz, mydx, mydy, mydz, dist, dist2, dx, dy, dz;
  int index;
  /* End of feedback variables */
#ifdef RAYTRACE
  double col_tot[6], col_H2[6], col_CO[6];
#endif
#if defined TREE_RAD || defined TREE_RAD_H2
  double columni;
#endif
#ifdef TREE_RAD
  double NH;
#endif
#ifdef TREE_RAD_H2
  double NH2;
  double NCO;
#ifdef _13CO_
  double N13CO;
#endif
#endif
  int i, ID;

  /* Set local length-scale */
  dl = current_particle->Hsml;
  if (All.PhotochemApprox == 3 && radius > current_particle->Hsml) {
    dl = radius;
  }
  
  if (All.PhotochemApprox == 4 && radius < current_particle->Hsml) {
      radius = dl;
  }

  /* XXX: CHECKME - correct units for comoving sims? */
  if (All.ComovingIntegrationOn) { /* comoving variables */
    a3inv    =  1 / (All.Time*All.Time*All.Time); 
    hubble_a = All.Omega0 / (All.Time * All.Time * All.Time)
	+ (1 - All.Omega0 - All.OmegaLambda) / (All.Time * All.Time) 
        + All.OmegaLambda;
    hubble_a = All.Hubble * All.HubbleParam * sqrt(hubble_a);
    rho      = current_particle->Density * a3inv;
    dl       *= All.Time;
    radius   *= All.Time;
    timestep = dt / hubble_a;
    divv     = 3.0*hubble_a + current_particle->DivVel / sqrt(All.Time); /* XXX: check this */
    COOLR.redshift = (1.0 / All.Time) - 1.0;
  }
  else {
    rho      = current_particle->Density;
    /* We assume that All.Time = 0 at All.InitRedshift, and that
     * all redshifts of interest are >> 1, so we can use a simple
     * approximation for the lookback times; if this isn't the case,
     * then we should probably be using comoving coordinates anyway.
     * Note that we assume that the redshift is fixed for the duration 
     * of the timestep (or in other words that dt << t_Hubble)
     */
     COOLR.redshift = pow((3. * All.Hubble * All.HubbleParam *
			    sqrt(All.Omega0) * All.Time / 2.)  + 
			    pow(1 + All.InitRedshift, -1.5), -2./3.) - 1;
    timestep = dt;
    divv     = current_particle->DivVel;
  }  

  /* Set correct dust temperature in coolr common block */
  COOLR.tdust = current_particle->DustTemp;

  /* At the moment, the entropy update due to viscous forces is operator
   * split from the entropy update due to cooling & chemistry
   */
  entropy_init              = current_particle->Entropy;
  /* For networks 1, 4, 5, 6 use an unsplit treatment */
  if (All.ChemistryNetwork == 1 || All.ChemistryNetwork == 4 || All.ChemistryNetwork == 5 || All.ChemistryNetwork == 6) {
    entropy_after_visc_update = entropy_init;
  }
  else {
     entropy_after_visc_update = entropy_init + dt * current_particle->DtEntropyVisc;
  }

#ifdef NO_VARIABLE_GAMMA
  gamma = old_gamma = 5./3.;
#else
  old_gamma  = current_particle->Gamma;
  gamma      = calc_gamma_from_entropy(entropy_after_visc_update, rho, current_particle->TracAbund[IH2], old_gamma);
  if (gamma < 1.0) {
    printf("Tiny gamma! old gamma %g, gamma %g, entropy_init %g, entropy_current %g, H2 fraction %g, density %g\n", old_gamma, gamma,
           entropy_init, entropy_after_visc_update, current_particle->TracAbund[IH2], rho);
  }
#endif
  /* 'energy' is internal energy density [in code units] */ 
  energy = entropy_after_visc_update * pow(rho, gamma) / (gamma - 1.0);
  if (energy < rho * All.MinEgySpec) {
      energy = rho * All.MinEgySpec;
  }

#ifdef DMA
  /* XXX: this is rubbish -- we need to use the RFinalDM array for the shell radii,
   *      and hence our lookup here won't work
   */

  /* Get DM density for this particle */
  if (radius < All.RValues[0]) {
    /* We're in the central bin, so just use the local mean density, 
       as we have no information on the profile within this bin */
    dm_density = All.DMDensity[0];
  }
  else if (radius > All.Rvalues[NRSHELLS-2]) {
    /* Can't happen? */
    dm_density = All.DMDensity[NRSHELLS-1];
  }
  else {
    logrbin = log10(All.RadiusFactor);
    logr    = log10(radius);
    ir      = (logr - log10(All.hmin)) / logrbin;
    rmid    = log10(All.RValues[ir-1]) + 0.5 * logrbin;
    if (logr > rmid) {
      d1 = log10(All.DMDensity[ir]);
      d2 = log10(All.DMDensity[ir+1]);
      dm_density = d1 + (d2 - d1) * (logr - rmid) / logrbin;
      dm_density = pow(10, dm_density);
    }
    else {
      d1 = log10(All.DMDensity[ir-1]);
      d2 = log10(All.DMDensity[ir]);
      dm_density = d1 + (d2 - d1) * (rmid - logr) / logrbin;
      dm_density = pow(10, dm_density);
    }
  }
#ifdef DMA_DEBUG
  printf("Particle %d, radius %g, DM density %g\n", ID, radius, dm_density);
#endif
#endif /* DMA */

  /* Convert to cgs units */
  rho      *= All.UnitDensity_in_cgs;
  timestep *= All.UnitTime_in_s;
  energy   *= All.UnitEnergy_in_cgs / pow(All.UnitLength_in_cm, 3);
  energy_cgs_init = energy;
  dl       *= All.UnitLength_in_cm;
  divv     *= All.UnitVelocity_in_cm_per_s / All.UnitLength_in_cm;
  for (i=0; i<TRAC_NUM; i++) {
    abundances[i] = current_particle->TracAbund[i];
  }
  yn        = rho / ((1.0 + 4.0 * ABHE) * PROTONMASS);
  radius   *= All.UnitLength_in_cm;
  gaccr    *= (All.UnitLength_in_cm/All.UnitTime_in_s/All.UnitTime_in_s);
  mcloud    = All.TotalMass*All.UnitMass_in_g;
  rcloud    = All.RMaxCloud*All.UnitLength_in_cm;

  ID = COOLI.id_current;

#ifdef TREE_RAD
  for (i = 0; i < NPIX; i++) {
    columni = current_particle->Projection[i] * All.UnitDensity_in_cgs * All.UnitLength_in_cm;
    NH     = columni / ((1.0 + 4.0 * ABHE) * PROTONMASS);
    PROJECT.column_density_projection[i] = NH;
  }
#endif

#ifdef TREE_RAD_H2
  for (i = 0; i < NPIX; i++) {
    columni = current_particle->ProjectionH2[i] * All.UnitDensity_in_cgs * All.UnitLength_in_cm;
    NH2     = columni / (2.0 * PROTONMASS);
    PROJECT.column_density_projection_h2[i] = NH2;
  }

#if CHEMISTRYNETWORK != 1 && CHEMISTRYNETWORK != 4
  for (i = 0; i < NPIX; i++) {
    columni = current_particle->ProjectionCO[i] * All.UnitDensity_in_cgs * All.UnitLength_in_cm;
    NCO     = columni / (28.0 * PROTONMASS);
    PROJECT.column_density_projection_co[i] = NCO;
#ifdef _13CO_
    columni = current_particle->Projection13CO[i] * All.UnitDensity_in_cgs * All.UnitLength_in_cm;
    N13CO     = columni / (29.0 * PROTONMASS);
    PROJECT.column_density_projection_13co[i] = N13CO;
#endif
  }
#else
  /* No CO for these networks */
  for (i = 0; i < NPIX; i++) {
    PROJECT.column_density_projection_co[i] = 0.0;
#ifdef _13CO_
    PROJECT.column_density_projection_13co[i] = 0.0;
#endif
  }
#endif
#endif /* TREE_RAD_H2 */

#ifdef DMA
  dm_density *= All.UnitDensity_in_cgs;
  COOLR.dm_density = dm_density;
#endif

#if 1
  if (ID == 1418983) {
    printf("Init entropy %g, energy %g, init gamma %g, dt %g, mode %d\n", entropy_init, energy, 
           current_particle->Gamma, timestep, mode);
    printf("Init dl, divv, rho: %g, %g, %g, mode %d\n", dl, divv, rho, mode);
#if CHEMISTRYNETWORK == 5
    printf("Init abundances: %g, %g, %g, mode %d\n", abundances[0], abundances[1], abundances[2], mode);
#else
    printf("Init abundances: %g, %g, %g, %g, mode %d\n", abundances[0], abundances[1], abundances[2], abundances[3], mode);
#endif
    printf("Init dust temp %g, mode %d\n", current_particle->DustTemp, mode);
  }
#endif
  
#ifdef DEBUG_SIGMA_PCC
  ID = COOLI.id_current;
  if ( ID == 1 ) {
     printf("Mcloud %g \n", mcloud);
  }
#endif

  /* Switch off chemistry for high-density particles */
  if (current_particle->ChemistryDisabled == 1) {
    COOLI.no_chem = 1;
  }
  else if (All.ChemistryNetwork == 1 && yn > 1e19) {
    COOLI.no_chem = 1;
    current_particle->ChemistryDisabled = 1;
  }
  else {
    COOLI.no_chem = 0;
  }

  /* When NOT using the raytrace, or the local approx for the shielding, use this bit to 
  calculate the column density for the particle and pass it to 'column_est'. 
  Note, this is used for options All.PhotochemApprox = 4, 5, and 6.
  This is then fed through the fortran until it ends up in calc_photo.F */

  column_est = 0.0;

  if (All.PhotochemApprox == 4 ) {
     mgrav = fabs(gaccr)*radius*radius/GRAVITY;
     rext = dmax(rcloud-radius, dl);
     if (mgrav > mcloud ) {
        column_est = 0.0;
     }
     else {
       column_est = (mcloud - mgrav)/(4.18879*fabs( pow(rcloud+0.5*dl, 3) - pow(radius, 3)));
       column_est *= rext;
     }
  }
  else if ( All.PhotochemApprox == 5 ) {
#ifdef RADCOL
     column_est = current_particle->RadCol;
     column_est *= (All.UnitMass_in_g/All.UnitLength_in_cm/All.UnitLength_in_cm);
#ifdef DEBUGDUSTTMP
     if ( yn>1.0e8  ) {
        printf("CHEMCOOL DEBUG (yn, column_est): %g %g \n ", yn, column_est/1.4/PROTONMASS);
        endrun(666);
     }
#endif
#else
  printf("Need to compile with RADCOL option if you're using PhotochemApprox = 5!\n");
  endrun(666);
#endif
  }
  if (All.ChemistryNetwork == 1 || All.ChemistryNetwork == 4 || All.ChemistryNetwork == 5 || All.ChemistryNetwork == 6) { 
    pdv_heating = current_particle->DtEntropyVisc * pow(rho, gamma) / (gamma - 1.0);
    pdv_heating *= All.UnitEnergy_in_cgs / (All.UnitTime_in_s * pow(All.UnitLength_in_cm, 3));
    if (All.ComovingIntegrationOn) {
      pdv_heating *= hubble_a;
    }
    COOLR.pdv_term = pdv_heating;
  }
  ID = COOLI.id_current;

  /* Radiative feedback from sinks -- XXX: not comoving yet */
  if (All.RadHeatFlag == 1) {

    if (ID == 1000) {
      printf("RadHeat: Particle1000 in rad heat area\n");
    }


    if (All.TotN_sinks == 0) {
      RAD_SOURCE_DATA.nradsource = 0;
    }
    else {
      RAD_SOURCE_DATA.nradsource = All.TotN_sinks;
       if (ID == 1000) {
	 printf("RadHeat: Rad source present\n");
       }
      for (i=0; i<All.TotN_sinks; i++) {
        sink_mass = All.SinkStoreMass[i] * All.UnitMass_in_g;
	/*        sink_mdot = All.SinkStoreMdot[i] * All.UnitMass_in_g / All.UnitTime_in_s; */
	/* Mdot code not yet written. In the mean time, for testing purposes, we simply assume
	 * a constant mass accretion rate of 1e-3 M_sun/yr for all sources
	 */
        sink_mdot = 1.e-2 * SOLAR_MASS / (86400 * 365.25);
        /*sink_mdot = 0. * SOLAR_MASS / (86400 * 365.25);*/
        stellar_radius = compute_stellar_radius(sink_mass, sink_mdot); /* Already in cgs */
        accretion_luminosity = compute_luminosity(sink_mass, sink_mdot, stellar_radius);
	RAD_SOURCE_DATA.rad_source_luminosities[i] = accretion_luminosity;

	/* crjs	if (sink_mass==0.){
	  printf("Radius=0\n");
	  printf("radius %g, mass %g, mdot %g \n", stellar_radius,sink_mass,sink_mdot);
	  fflush(stdout);
	  }*/


	/*	printf("RadHeat: Particle heated by Luminosity",ID,accretion_luminosity);*/

        sink_dx = All.SinkStoreXyz[i][0];
        sink_dy = All.SinkStoreXyz[i][1];
        sink_dz = All.SinkStoreXyz[i][2];

        index = COOLI.index_current;
        mydx = P[index].Pos[0];
        mydy = P[index].Pos[1];
        mydz = P[index].Pos[2];

        dx = sink_dx - mydx;
        dy = sink_dy - mydy;
        dz = sink_dz - mydz;
#ifdef PERIODIC
        if(dx > boxHalf_X)
          dx -= boxSize_X;
	if(dx < -boxHalf_X)
          dx += boxSize_X;
	if(dy > boxHalf_Y)
	  dy -= boxSize_Y;
	if(dy < -boxHalf_Y)
	  dy += boxSize_Y;
	if(dz > boxHalf_Z)
	  dz -= boxSize_Z;
	if(dz < -boxHalf_Z)
	  dz += boxSize_Z;
#endif
        dist2 = dx * dx + dy * dy + dz * dz;
        dist = sqrt(dist2) * All.UnitLength_in_cm;
	RAD_SOURCE_DATA.rad_source_distances[i] = dist;

#ifdef DEBUG_FEEDBACK
	/*	if (ID==1866845){*/
	if (sink_mass==0.||sink_mdot==0.){
	  printf("ID= %d \n",ID);
	  printf("Task %d, sink number %d, sink_mass %g, sink_mdot %g, stellar_radius %g, luminosity %g\n", ThisTask, i,
               sink_mass, sink_mdot, stellar_radius, accretion_luminosity);
	  printf("Task %d, sink number %d, sink x,y,z = (%g, %g, %g), distance %g\n", ThisTask, i, sink_dx, sink_dy, sink_dz,
               dist);
	  printf("Task %d, index %d, my x,y,z = (%g, %g, %g)\n", ThisTask, index, mydx, mydy, mydz);
	  fflush(stdout);
	}
#endif

      }
    }
  }

#ifdef RAYTRACE
  for (i=0; i<6; i++) {
    col_tot[i] = current_particle->TotalColumnDensity[i] / (All.UnitLength_in_cm * All.UnitLength_in_cm);
    col_H2[i]  = current_particle->H2ColumnDensity[i]    / (All.UnitLength_in_cm * All.UnitLength_in_cm);
#ifdef CO_SHIELDING
    col_CO[i]  =  current_particle->COColumnDensity[i]    / (All.UnitLength_in_cm * All.UnitLength_in_cm);
#endif
  }
#endif

#if CHEMISTRYNETWORK != 1
    rpar[0] = yn;
    rpar[1] = dl;
    rpar[2] = divv;
#ifdef RAYTRACE
    for (i=0; i<6; i++) {
      rpar[4+i] = col_tot[i];
      rpar[10+i] = col_H2[i];
    }
#ifdef CO_SHIELDING
    rpar[16+i] = col_CO[i];
#endif
#endif /* RAYTRACE */
    abh2 = abundances[IH2];
    abhd = 0.0;
#if CHEMISTRYNETWORK != 4
    abco = abundances[ICO];
#else
    abco = 0.0
#endif
/* For outputing the time with the sheilding data */
    ctime = All.Time * All.UnitTime_in_s;  
    ekn  = energy / (BOLTZMANN * (1.0 + ABHE - abh2) * yn);
    CALC_TEMP(&abh2, &ekn, &temp);
    shield[0] = 0.0;
    shield[1] = 0.0;
    shield[2] = 0.0;
    CALC_PHOTO_WRAPPER(&temp, rpar, &abh2, &abhd, &abco, &ctime, shield);
    current_particle->H2ShieldFactor = shield[0];
    current_particle->COShieldFactor = shield[1];
    current_particle->AvEff = shield[2];
#endif /* NETWORK != 1 */

#ifdef RAYTRACE
#ifdef CO_SHIELDING
  EVOLVE_ABUNDANCES(&timestep, &dl, &yn, &divv, &energy, abundances, col_tot, col_H2, col_CO);
#else
  EVOLVE_ABUNDANCES(&timestep, &dl, &yn, &divv, &energy, abundances, col_tot, col_H2);
#endif /* CO_SHIELDING */
#else
  EVOLVE_ABUNDANCES(&timestep, &dl, &yn, &divv, &energy, abundances, &column_est);
#endif /* RAYTRACE */

  /* Compute our new value of gamma */

  abh2 = abundances[IH2];
  if (abh2 < 0.01) {
    gamma = 5./3.;
  }
#ifdef NO_VARIABLE_GAMMA
  gamma = 5./3.;
#else
  else {
   /* We assume that if our H2 fraction is non-negligible, then our
    * electron fraction is negligible */
   ekn  = energy / (BOLTZMANN * (1.0 + ABHE - abh2) * yn);
   CALC_GAMMA(&abh2, &ekn, &gamma);
  }
#endif

  /* Compute final temperature */
  ekn  = energy / (BOLTZMANN * (1.0 + ABHE - abh2) * yn);
  CALC_TEMP(&abh2, &ekn, &temp);

#if 1
  if (ID == 1418983) {
    printf("Final energy %g, final gamma %g, temp %g, mode %d\n", energy, gamma, temp, mode);
#if CHEMISTRYNETWORK == 5
    printf("Final abundances: %g, %g, %g, mode %d\n", abundances[0], abundances[1], abundances[2], mode);
#else
    printf("Final abundances: %g, %g, %g, %g, mode %d\n", abundances[0], abundances[1], abundances[2], abundances[3], mode);
#endif
    printf("Final dust temp %g, mode %d\n", COOLR.tdust, mode);
  }
#endif

  /* Update dust temperature */
  current_particle->DustTemp = COOLR.tdust;

#ifdef SIMON_DEBUG
  if (current_particle->Density > 0.1) {
    printf("Particle %d, density %g, dust temp %g\n", ID, current_particle->Density, current_particle->DustTemp);
  }
#endif

  /* Convert back to code units from cgs */
  energy_cgs = energy;
  energy *= pow(All.UnitLength_in_cm, 3) / All.UnitEnergy_in_cgs;
  rho    /= All.UnitDensity_in_cgs;

  entropy_new = (gamma - 1.0) * energy / pow(rho, gamma);

  if (ID == 1418983) {
    printf("Final entropy %g, mode %d\n", entropy_new, mode);
  }

  /* Mode 0: store new abundances, new entropy in TracAbundOut[], EntropyOut.
   * Don't update actual values.
   *
   * Mode 1: store new abundances, new entropy in TracAbund[], Entropy.
   * Store updated value of Gamma.
   */
  if (mode == 0) {
    for (i=0; i<TRAC_NUM; i++) {
      current_particle->TracAbundOut[i] = abundances[i];
    }
    current_particle->EntropyOut = entropy_new;
    current_particle->GammaOut   = gamma;
    current_particle->TempOut    = temp;
  }
  else if (mode == 1) {
    for (i=0; i<TRAC_NUM; i++) {
      current_particle->TracAbund[i] = abundances[i];
    }
    current_particle->Gamma   = gamma;
    current_particle->Entropy = entropy_new;
  }
  else if (mode == 2) {
    /* Calculate a new estimate of the cooling time */
    if (entropy_new < 0.8 * entropy_init || 
        entropy_new > 1.2 * entropy_init) {
      /* If our energy changed by more than 20%, then we probably did need to
       * take a small timestep, so we return an estimate of the timestep we
       * should have taken 
       */
#if 0
      printf("Old entropy %g, new entropy %g\n", entropy_init, entropy_new);
      printf("Old gamma %g, new gamma %g\n", old_gamma, gamma);
      fflush(stdout);
      if (entropy_new > entropy_init) {
	dt *= (entropy_init / entropy_new) / 0.9;
      }
      else {
	dt *= (entropy_new / entropy_init) / 0.9;
      }
#endif
      return dt;
    }

    nsp = NSPEC;
    t_start = -1.; /* Nothing in rate_eq depends on t_start, so it doesn't matter what value we give it */
    rpar[0] = yn;
    rpar[1] = dl;
    rpar[2] = divv;
#ifdef RAYTRACE
    for (i=0; i<6; i++) {
      rpar[4+i] = col_tot[i];
      rpar[10+i] = col_H2[i];
    }
#ifdef CO_SHIELDING
    rpar[16+i] = col_CO[i];
#endif
#else /* RAYTRACE */
    rpar[4] = column_est;
#endif /* RAYTRACE */

#if CHEMISTRYNETWORK != 1
    abh2 = abundances[IH2];
    abhd = 0.0;
#if CHEMISTRYNETWORK != 4
    abco = abundances[ICO];
#else
    abco = 0.0
#endif
/* For outputing the time with the sheilding data */
    ctime = All.Time * All.UnitTime_in_s;  
    ekn  = energy / (BOLTZMANN * (1.0 + ABHE - abh2) * yn);
    CALC_TEMP(&abh2, &ekn, &temp);
    shield[0] = 0.0;
    shield[1] = 0.0;
    shield[2] = 0.0;
    CALC_PHOTO_WRAPPER(&temp, rpar, &abh2, &abhd, &abco, &ctime, shield);
    current_particle->H2ShieldFactor = shield[0];
    current_particle->COShieldFactor = shield[1];
    current_particle->AvEff = shield[2];
#endif

    ipar[0] = 0;
    for (i=0; i<TRAC_NUM; i++) {
      y[i] = abundances[i];
    }
    y[ITMP] = energy_cgs;

    RATE_EQ(&nsp, &t_start, y, ydot, rpar, ipar);

    if (ydot[ITMP] == 0.0) {
      dt = 1e20;
    }
    else {
      dt = DTCOOL_SCALE_FACTOR * y[ITMP] / ydot[ITMP];
    }
    dt = sqrt(dt * dt);
    dt /= All.UnitTime_in_s;
    return dt;
  }
  else if (mode == 3) {
    current_particle->Entropy = entropy_new;
  }
  else {
    printf("Unknown mode: %d!\n", mode);
    endrun(101);
  }
  return dt;
}

/* Calculate the cooling time for use in timestep.c */
/* also used to calculate the cooling rates in init.c
 * and updatecooling.c so they can be printed out*/

double GetCoolTime(struct sph_particle_data *current_particle, double radius, double gaccr)
{
  double entropy, energy, rho, divv, dl, yn, gamma;
  int    i, nsp, ipar[NIPAR], ID;
  double dtcool, a3inv, t_start;
  double hubble_a = 1, ctime;
  double rpar[NRPAR], y[NSPEC], ydot[NSPEC], shield[3];
  double dtvisc, pdv_heating;
  double abh2, abhd, abco, temp, ekn;
  double column_est, old_gamma;
  double mcloud, rcloud, mgrav, rext;
  /* Used for accretion luminosity feedback */
  double sink_mass, sink_mdot, stellar_radius, accretion_luminosity;
  double sink_dx, sink_dy, sink_dz, mydx, mydy, mydz, dist, dist2, dx, dy, dz;
  int index;
  /* End of feedback variables */
#if defined TREE_RAD || defined TREE_RAD_H2
  double columni;
#endif
#ifdef TREE_RAD
  double NH;
#endif
#ifdef TREE_RAD_H2
  double NH2;
  double NCO;
#ifdef _13CO_
  double N13CO;
#endif
#endif

  ID = COOLI.id_current;
  mcloud    = All.TotalMass*All.UnitMass_in_g;
  rcloud    = All.RMaxCloud*All.UnitLength_in_cm;
#ifdef TREE_RAD
  for (i = 0; i < NPIX; i++) {
    columni = current_particle->Projection[i] * All.UnitDensity_in_cgs * All.UnitLength_in_cm;
    NH     = columni / ((1.0 + 4.0 * ABHE) * PROTONMASS);
    PROJECT.column_density_projection[i] = NH;
  }
#endif
#ifdef TREE_RAD_H2
  for (i = 0; i < NPIX; i++) {
    columni = current_particle->ProjectionH2[i] * All.UnitDensity_in_cgs * All.UnitLength_in_cm;
    NH2     = columni / (2.0 * PROTONMASS);
    PROJECT.column_density_projection_h2[i] = NH2;
  }
#if CHEMISTRYNETWORK != 1 && CHEMISTRYNETWORK != 4
  for (i = 0; i < NPIX; i++) {
    columni = current_particle->ProjectionCO[i] * All.UnitDensity_in_cgs * All.UnitLength_in_cm;
    NCO     = columni / (28.0 * PROTONMASS);
    PROJECT.column_density_projection_co[i] = NCO;
#ifdef _13CO_
    columni = current_particle->Projection13CO[i] * All.UnitDensity_in_cgs * All.UnitLength_in_cm;
    N13CO     = columni / (29.0 * PROTONMASS);
    PROJECT.column_density_projection_13co[i] = N13CO;
#endif
  }
#else
  /* No CO for these chemical networks */
  for (i = 0; i < NPIX; i++) {
    PROJECT.column_density_projection_co[i] = 0.;
#ifdef _13CO_
    PROJECT.column_density_projection_13co[i] = 0.;
#endif
  }
#endif
#endif /* TREE_RAD_H2 */

  entropy         = current_particle->Entropy;
  rho             = current_particle->Density;
  divv            = current_particle->DivVel;

  /* Set local length-scale */
  dl = current_particle->Hsml;
  if (All.PhotochemApprox == 3 && radius > current_particle->Hsml) {
    dl = radius;
  }
  
  if (All.PhotochemApprox == 4 && radius < current_particle->Hsml) {
      radius = dl;
  }

  /* Multiply by appropriate scale-factors if using comoving coords */

  if (All.ComovingIntegrationOn) {
    a3inv    = 1/(All.Time*All.Time*All.Time); 
    hubble_a = All.Hubble * All.HubbleParam * sqrt(All.Omega0 / 
               (All.Time*All.Time*All.Time) + (1 - All.Omega0 - 
                All.OmegaLambda) / (All.Time*All.Time) + All.OmegaLambda);
    rho *= a3inv;
    dl  *= All.Time;
    divv = 3.0*hubble_a + divv / sqrt(All.Time);
  }

  for (i=0; i<TRAC_NUM; i++) { 
    y[i] = current_particle->TracAbund[i];
  }

#ifdef NO_VARIABLE_GAMMA
  gamma = old_gamma = 5./3.;
#else
  old_gamma  = current_particle->Gamma;
  gamma      = calc_gamma_from_entropy(entropy, rho, current_particle->TracAbund[IH2], old_gamma);
  if (gamma < 1.0) {
    printf("Tiny gamma! old gamma %g, gamma %g, entropy %g, H2 fraction %g, density %g\n", old_gamma, gamma,
           entropy, current_particle->TracAbund[IH2], rho);
  }
#endif
  /* 'energy' is internal energy density [in code units] */ 
  energy = entropy * pow(rho, gamma) / (gamma - 1.0);

  /* Rescale to cgs for FORTRAN routines */

  energy *= All.UnitEnergy_in_cgs / pow(All.UnitLength_in_cm, 3);
  rho    *= All.UnitDensity_in_cgs;
  divv   *= All.UnitVelocity_in_cm_per_s / All.UnitLength_in_cm;
  dl     *= All.UnitLength_in_cm;

  /* Set correct dust temperature in coolr common block */
  COOLR.tdust = current_particle->DustTemp;

  /* Set redshift */
  if (All.ComovingIntegrationOn) {
    COOLR.redshift = (1.0 / All.Time) - 1.0;
  }
  else {
    /* See note in do_chemcool_step() above */
     COOLR.redshift = pow((3. * All.Hubble * All.HubbleParam *
			    sqrt(All.Omega0) * All.Time / 2.)  + 
			    pow(1 + All.InitRedshift, -1.5), -2./3.) - 1;
  }

  yn = rho / ((1.0 + 4.0 * ABHE) * PROTONMASS); /* 1.4 if 10% He by number */
  
  
  /*When NOT using the raytrace, or the local approx for the shielding, use this bit to 
  calculate the column density for the particle and pass it to 'column_est'. This is
  the fed through the fortran until it ends up in calc_photo.F, where it can be used directly*/

   column_est = 0.0;

   if (All.PhotochemApprox == 4 ) {
     mgrav = fabs(gaccr)*radius*radius/GRAVITY;
     rext = dmax(rcloud-radius, dl);
     if (mgrav > mcloud ) {
        column_est = 0.0;
     }
     else {
       column_est = (mcloud - mgrav)/(4.18879*fabs( pow(rcloud+0.5*dl, 3) - pow(radius, 3)));
       column_est *= rext;
     }
   }
   else if ( All.PhotochemApprox == 5 ) {
#ifdef RADCOL
     column_est = current_particle->RadCol;
     column_est *= (All.UnitMass_in_g/All.UnitLength_in_cm/All.UnitLength_in_cm);
#else
  printf("Need to compile with RADCOL option if you're using PhotochemApprox = 5!\n");
  endrun(666);
#endif
   }

  if (All.ChemistryNetwork == 1 || All.ChemistryNetwork == 4 || All.ChemistryNetwork == 5 || All.ChemistryNetwork == 6) { 
    pdv_heating = current_particle->DtEntropyVisc * pow(rho, gamma) / (gamma - 1.0);
    pdv_heating *= All.UnitEnergy_in_cgs / (All.UnitTime_in_s * pow(All.UnitLength_in_cm, 3));
    if (All.ComovingIntegrationOn) {
      pdv_heating *= hubble_a;
    }
    COOLR.pdv_term = pdv_heating;
  }
  ID = COOLI.id_current;

  /* Radiative feedback from sinks -- XXX: not comoving yet */
  if (All.RadHeatFlag == 1) {
    if (All.TotN_sinks == 0) {
      RAD_SOURCE_DATA.nradsource = 0;
    }
    else {
      RAD_SOURCE_DATA.nradsource = All.TotN_sinks;
      for (i=0; i<All.TotN_sinks; i++) {
        sink_mass = All.SinkStoreMass[i] * All.UnitMass_in_g;
	/*        sink_mdot = All.SinkStoreMdot[i] * All.UnitMass_in_g / All.UnitTime_in_s; */
	/* Mdot code not yet written. In the mean time, for testing purposes, we simply assume
	 * a constant mass accretion rate of 1e-3 M_sun/yr for all sources
	 */
        sink_mdot = 1e-3 * SOLAR_MASS / (86400 * 365.25);
        /*sink_mdot = 0. * SOLAR_MASS / (86400 * 365.25);*/
        stellar_radius = compute_stellar_radius(sink_mass, sink_mdot); /* Already in cgs */
        accretion_luminosity = compute_luminosity(sink_mass, sink_mdot, stellar_radius);
	RAD_SOURCE_DATA.rad_source_luminosities[i] = accretion_luminosity;

	/*	if (sink_mass==0.){
	  printf("Radius=0 in chem time\n");
	  printf("radius %g, mass %g, mdot %g \n", stellar_radius,sink_mass,sink_mdot);
	  fflush(stdout);
	  }*/


        sink_dx = All.SinkStoreXyz[i][0];
        sink_dy = All.SinkStoreXyz[i][1];
        sink_dz = All.SinkStoreXyz[i][2];

        index = COOLI.index_current;
        mydx = P[index].Pos[0];
        mydy = P[index].Pos[1];
        mydz = P[index].Pos[2];

        dx = sink_dx - mydx;
        dy = sink_dy - mydy;
        dz = sink_dz - mydz;
#ifdef PERIODIC
        if(dx > boxHalf_X)
          dx -= boxSize_X;
	if(dx < -boxHalf_X)
          dx += boxSize_X;
	if(dy > boxHalf_Y)
	  dy -= boxSize_Y;
	if(dy < -boxHalf_Y)
	  dy += boxSize_Y;
	if(dz > boxHalf_Z)
	  dz -= boxSize_Z;
	if(dz < -boxHalf_Z)
	  dz += boxSize_Z;
#endif
        dist2 = dx * dx + dy * dy + dz * dz;
        dist = sqrt(dist2) * All.UnitLength_in_cm;
	RAD_SOURCE_DATA.rad_source_distances[i] = dist;


#ifdef DEBUG_FEEDBACK
	if (ID==1866844){
	/*if (sink_mass==0.||sink_mdot==0.){*/
	  printf("ID= %d in chem time\n",ID);
	  printf("Task %d, ID %d, sink_mass %g, sink_mdot %g, stellar_radius %g, luminosity %g\n", ThisTask, ID,
               sink_mass, sink_mdot, stellar_radius, accretion_luminosity);
	  printf("Task %d, ID %d, sink x,y,z = (%g, %g, %g), distance %g\n", ThisTask, ID, sink_dx, sink_dy, sink_dz,
               dist);
	  printf("Task %d, ID %d, my x,y,z = (%g, %g, %g)\n", ThisTask, ID, mydx, mydy, mydz);
	  fflush(stdout);
	}

	if (ID==1866845){
	/*if (sink_mass==0.||sink_mdot==0.){*/
	  printf("ID= %d in chem time\n",ID);
	  printf("Task %d, ID %d, sink_mass %g, sink_mdot %g, stellar_radius %g, luminosity %g\n", ThisTask, ID,
               sink_mass, sink_mdot, stellar_radius, accretion_luminosity);
	  printf("Task %d, ID %d, sink x,y,z = (%g, %g, %g), distance %g\n", ThisTask, ID, sink_dx, sink_dy, sink_dz,
               dist);
	  printf("Task %d, ID %d, my x,y,z = (%g, %g, %g)\n", ThisTask, ID, mydx, mydy, mydz);
	  fflush(stdout);
	}

#endif

      }
    }
  }

  nsp = NSPEC;
  t_start = -1.; /* Nothing in rate_eq depends on t_start, so it doesn't
                    matter what value we give it */
  rpar[0] = yn;
  rpar[1] = dl;
  rpar[2] = divv;
#ifdef RAYTRACE
  for (i=0; i<6; i++) {
    rpar[i+4] = current_particle->TotalColumnDensity[i] / (All.UnitLength_in_cm * All.UnitLength_in_cm);
    rpar[i+10] = current_particle->H2ColumnDensity[i]    / (All.UnitLength_in_cm * All.UnitLength_in_cm);
#ifdef CO_SHIELDING
    rpar[i+16] = current_particle->COColumnDensity[i]   / (All.UnitLength_in_cm * All.UnitLength_in_cm);
#endif
  }
#else /* RAYTRACE */
  /* Raytrace isn't being used so we can store properties for the other methods
  in here (iphoto = 4, 5, etc...) */
  rpar[4] = column_est;
#endif
  ipar[0] = 0;

#if CHEMISTRYNETWORK != 1
  abh2 = y[IH2];
  abhd = 0.0;
#if CHEMISTRYNETWORK != 4
  abco = y[ICO];
#else
  abco = 0.0;
#endif
/* For outputing the time with the sheilding data */
    ctime = All.Time * All.UnitTime_in_s;  
  ekn  = energy / (BOLTZMANN * (1.0 + ABHE - abh2) * yn);
  CALC_TEMP(&abh2, &ekn, &temp);
    shield[0] = 0.0;
    shield[1] = 0.0;
    shield[2] = 0.0;
    CALC_PHOTO_WRAPPER(&temp, rpar, &abh2, &abhd, &abco, &ctime, shield);
    current_particle->H2ShieldFactor = shield[0];
    current_particle->COShieldFactor = shield[1];
    current_particle->AvEff = shield[2];
#endif
  y[ITMP] = energy;

  RATE_EQ(&nsp, &t_start, y, ydot, rpar, ipar);

  if (ydot[ITMP] == 0.0) {
    /* Cooling time is formally infinite. Since we can't return infinity,
       however, we make do with a very big number: 10^20 seconds. */
    dtcool = 1e20;
  }
  else {
    /* We assume that the energy is non-zero */
    dtcool = DTCOOL_SCALE_FACTOR * y[ITMP] / ydot[ITMP];
  }
  /*crjs*/

#ifdef DEBUG_FEEDBACK
	if (ID==1866844){
	/*if (sink_mass==0.||sink_mdot==0.){*/
	  printf("ID= %d in chem time after primordial\n",ID);
	  printf("Task %d, ID %d, Heating %g, Cool time %g, Lacc %g, Rho %g\n", ThisTask, ID,
		 THERMALINFO.acc_heat,dtcool, accretion_luminosity,rho);
	  fflush(stdout);
	}

	if (ID==1866845){
	/*if (sink_mass==0.||sink_mdot==0.){*/
	  printf("ID= %d in chem time after primordial\n",ID);
	  printf("Task %d, ID %d, Heating %g, Cool time %g, Lacc %g, Rho %g\n ", ThisTask, ID,
		 THERMALINFO.acc_heat,dtcool, accretion_luminosity,rho);
	  fflush(stdout);
	}

#endif


  dtcool = sqrt(dtcool * dtcool); /* make sure timestep is not negative */  

  dtcool /= All.UnitTime_in_s; 
  /* Rescale to cosmological units if necessary */
  if (All.ComovingIntegrationOn) {
    dtcool *= hubble_a;
  }

  /* Also compute timescale on which Entropy varies due to viscous 
   * dissipation. If this is shorter than the radiative cooling 
   * timescale, then we use this as the cooling time.
   *
   * NB This is in CODE UNITS, not CGS.
   */
  dtvisc = 1e20;
  if (current_particle->DtEntropyVisc != 0.0) {
    dtvisc = fabs(ENTROPY_TIMESTEP_FACTOR * entropy / current_particle->DtEntropyVisc);
  }

  if (dtvisc < dtcool) {
    /* printf("dtvisc < dtcool, entropy = %g, DtEntropy = %g, dtvisc = %g, dtcool = %g\n", entropy, current_particle->DtEntropyVisc, dtvisc, dtcool); */
    dtcool = dtvisc;    
  }

  /* With our new reworking of the cooling time code, this check is no longer necessary.
   * Our initial estimate of dtcool can be below the minimum timestep, as long as our
   * second estimate is OK. If it isn't, THEN we should catch the error and stop, but
   * that can't be done here 
   */
#if 0
  if (dtcool < All.Timebase_interval) {
    printf("Zero timestep on processor %d\n", ThisTask);
    printf("Particle ID  %d\n", COOLI.id_current);
    dtcool *= All.UnitTime_in_s;
    printf("Task %d, Cooling timestep = %g\n", ThisTask, dtcool);
    /* Temperature estimate */
    T_est = (gamma - 1.0) * energy / (BOLTZMANN * (1.0 + ABHE - y[IH2]) * yn);
    printf("Task %d, Temperature = %g, Density = %g\n", ThisTask, T_est, yn);
    for (j=0; j<TRAC_NUM; j++) {
      printf("Task %d, Abundance %d = %g, rate of change = %g\n", ThisTask, j, y[j], ydot[j]);
    }
    printf("Task %d, Viscous timestep = %g\n", ThisTask, dtvisc);
#ifndef NOSTOP_WHEN_BELOW_MINTIMESTEP
    endrun(1471);
#endif
  }
#endif

  return dtcool;
}

double initial_electron_fraction(void)
{
  double abe, abhp, abcp, absip, abop, abdp, abhep, abhcop;
  double abhepp, absipp, abch3p, abh3op, abo2p, abn2hp, abmp;

  abe = 0.0;

  switch(All.ChemistryNetwork) {
  case 1:
    abhp   = All.InitHPlusAbund;
    abdp   = All.InitDIIAbund;
    abhep  = All.InitHeIIAbund;
    abhepp = All.InitHeIIIAbund;
    abe    = abhp + abdp + abhep + 2.0 * abhepp;
    break;
  case 2:
    abhp   = All.InitHPlusAbund;
    abdp   = All.InitDIIAbund;
    abhep  = All.InitHeIIAbund;
    abhepp = All.InitHeIIIAbund;
    abcp   = All.InitCIIAbund;
    absip  = All.InitSiIIAbund;
    abop   = All.InitOIIAbund;
    abe    = abhp + abdp + abhep + 2.0 * abhepp;
    abe    += abcp + absip + abop;
    break;
  case 3:
    abhp   = All.InitHPlusAbund;
    abdp   = All.InitDIIAbund;
    abhep  = All.InitHeIIAbund;
    abhepp = All.InitHeIIIAbund;
    abcp   = All.InitCIIAbund;
    absip  = All.InitSiIIAbund;
    abop   = All.InitOIIAbund;
    abhcop = All.InitHCOPlusAbund;
    absipp = All.InitSiIIIAbund;
    abch3p = All.InitCH3PlusAbund;
    abe    = abhp + abdp + abhep + 2.0 * abhepp;
    abe    += abcp + absip + abop;
    abe    += abhcop + 2.0 * absipp + abch3p;
    break;
 case 4:
 case 5:
 case 6:
    abhp  = All.InitHPlusAbund;
    abe   = abhp;
    break;
 case 7:
 case 11:
    abhp   = All.InitHPlusAbund;
    abhep  = All.InitHeIIAbund;
    abcp   = All.InitCIIAbund;
    abop   = All.InitOIIAbund;
    abhcop = All.InitHCOPlusAbund;
    abch3p = All.InitCH3PlusAbund;
    abe    = abhp + abhep + abcp + abop + abhcop + abch3p;
    break;
 case 8:
    abhp   = All.InitHPlusAbund;
    abhep  = All.InitHeIIAbund;
    abcp   = All.InitCIIAbund;
    abop   = All.InitOIIAbund;
    abhcop = All.InitHCOPlusAbund;
    abch3p = All.InitCH3PlusAbund;
    abh3op = All.InitH3OPAbund;
    abo2p  = All.InitO2PAbund;
    abn2hp = All.InitN2HPAbund;
    abe    = abhp + abhep + abcp + abop + abhcop + abch3p + abh3op + abo2p + abn2hp;
    break;
  case 15:
    abhp   = All.InitHPlusAbund;
    abhep  = All.InitHeIIAbund;
    abcp   = All.InitCIIAbund;
    abhcop = All.InitHCOPlusAbund;
    abmp   = All.InitMPAbund;
    abe    = abhp + abhep + abcp + abhcop + abmp;
    break;
 default:
   break;
  }
  return abe;
}

double compute_electron_fraction(FLOAT abundances[NSPEC])
{
  double abe;

  switch(All.ChemistryNetwork) {
  case 1:
    abe = abundances[IHP] + abundances[IDP] + abundances[IHEP] 
        + 2.0 * abundances[IHEPP];
    break;
  case 2:
    abe = abundances[IHP] + abundances[IDP] + abundances[IHEP]
        + 2.0 * abundances[IHEPP] + abundances[IC] + abundances[IO]  
        + abundances[ISi];
    break;
  case 3:
    abe = abundances[IHP] + abundances[IDP] + abundances[IHEP] 
        + 2.0 * abundances[IHEPP] +  abundances[IC] + abundances[IO]  
        + abundances[ISi]  + abundances[IHCOP]
        + 2.0 * abundances[ISIPP] + abundances[ICH3P];
    break;
  case 4:
  case 5:
  case 6:
    abe = abundances[IHP];
    break;
  case 7:
  case 11:
    abe = abundances[IHP] + abundances[IHEP]
        +  abundances[IC] + abundances[IO]  + abundances[IHCOP]
        + abundances[ICH3P];
    break;
  case 8:
    abe = abundances[IHP] + abundances[IHEP]
        +  abundances[IC] + abundances[IO]  + abundances[IHCOP]
      + abundances[ICH3P] + abundances[IH3OP] + abundances[IO2P]
      + abundances[IN2HP];
    break;
  case 15:
    abe = abundances[IHP] + abundances[IHEP] + abundances[IC]
        + abundances[IHCOP] + abundances[IMP];
    break;
  default:
    abe = 0.0; 
    break;
  }
  return abe;
}

double compute_initial_gamma(void)
{
  double abh2, abe, gamma;

  /* Simple estimate of starting gamma; this should be sufficiently
   * accurate to get us going, provided that the initial H2 fraction
   * is small. NB We now only use this for warm gas
   */
  abh2 = All.InitMolHydroAbund;
  abe  = initial_electron_fraction();
  gamma = (5.0 + 5.0 * ABHE - 3.0 * abh2 + 5.0 * abe) / 
          (3.0 + 3.0 * ABHE - abh2 + 3.0 * abe);
  return gamma;
}

/* Computes initial molecular weight in units of PROTONMASS */
double compute_initial_molecular_weight(void)
{
  double abe, abh2, abheI, abheII, abheIII, abhI, abhp, mu;

  /* Ignore minor corrections due to heavy elements, deuterium */
  switch(All.ChemistryNetwork) {
  case 1:
  case 2:
  case 3:
    abhp = All.InitHPlusAbund;
    abh2 = All.InitMolHydroAbund;
    abhI = 1.0 - abhp - 2.0 * abh2;

    abheIII = All.InitHeIIIAbund;
    abheII  = All.InitHeIIAbund;
    abheI   = ABHE - abheII - abheIII;

    abe = abhp + abheII + 2.0 * abheIII;
    break;
  case 7:
  case 8:
  case 11:
  case 15:
    abhp = All.InitHPlusAbund;
    abh2 = All.InitMolHydroAbund;
    abhI = 1.0 - abhp - 2.0 * abh2;

    abheIII = 0.0;
    abheII  = All.InitHeIIAbund;
    abheI   = ABHE - abheII;

    abe = abhp + abheII;
    break;
 case 4:
 case 5:
 case 6:
    abhp = All.InitHPlusAbund;
    abh2 = All.InitMolHydroAbund;
    abhI = 1.0 - abhp - 2.0 * abh2;

    abheIII = 0.0;
    abheII  = 0.0;
    abheI   = ABHE;

    abe = abhp;
    break;
  default:
    abe = abhp = abhI = abh2 = abheI = abheII = abheIII = 0.0;
    break;
  }

  mu = abhI + abhp + 2.0 * abh2 + 4.0 * (abheI + abheII + abheIII);
  mu /= abhI + abhp + abh2 + abheI + abheII + abheIII + abe;

  return mu;
}

#endif /* CHEMCOOL */
