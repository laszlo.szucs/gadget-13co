#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <mpi.h>
#include <gsl/gsl_math.h>
#include "allvars.h"
#include "proto.h"

/*! \file hydra.c
 *  \brief Computation of SPH forces and rate of entropy generation
 *
 *  This file contains the "second SPH loop", where the SPH forces are
 *  computed, and where the rate of change of entropy due to the shock heating
 *  (via artificial viscosity) is computed.
 */


static double hubble_a, atime, hubble_a2, fac_mu, fac_egy, fac_vsic_fix, a3inv;

#ifdef PERIODIC
static double boxSize, boxHalf;

#ifdef LONG_X
static double boxSize_X, boxHalf_X;
#else
#define boxSize_X boxSize
#define boxHalf_X boxHalf
#endif
#ifdef LONG_Y
static double boxSize_Y, boxHalf_Y;
#else
#define boxSize_Y boxSize
#define boxHalf_Y boxHalf
#endif
#ifdef LONG_Z
static double boxSize_Z, boxHalf_Z;
#else
#define boxSize_Z boxSize
#define boxHalf_Z boxHalf
#endif
#endif


/*! This function is the driver routine for the calculation of hydrodynamical
 *  force and rate of change of entropy due to shock heating for all active
 *  particles .
 */
void hydro_force(void)
{

  long long ntot, ntotleft;
  int i, j, k, n, ngrp, maxfill, source, ndone;
  int *nbuffer, *noffset, *nsend_local, *nsend, *numlist, *ndonelist;
  int level, sendTask, recvTask, nexport, place;
  double soundspeed_i;
  double tstart, tend, sumt, sumcomm;
  double timecomp = 0, timecommsumm = 0, timeimbalance = 0, sumimbalance;
  double dmax1, dmax2;
  MPI_Status status;
#ifdef CHEMCOOL
  FLOAT gamm1;
#endif

#ifdef CORRECT_SINK_PRESSURES
  double dx, dy, dz, r, r2, h_i, h_i2;
  double p_over_rho2_i;
  double mass, hacc2;
  double ghostposx, ghostposy, ghostposz;
  double hinv, hinv4, u, dwk_i, hfc;
#endif

#if defined(PRICE_SOUND)
  double rho_trans = 1.0E-14/All.UnitDensity_in_cgs;
  double Gamma1 = 1.0;
  double Gamma2 = 1.4;
#endif

#ifdef PERIODIC
  boxSize = All.BoxSize;
  boxHalf = 0.5 * All.BoxSize;
#ifdef LONG_X
  boxHalf_X = boxHalf * LONG_X;
  boxSize_X = boxSize * LONG_X;
#endif
#ifdef LONG_Y
  boxHalf_Y = boxHalf * LONG_Y;
  boxSize_Y = boxSize * LONG_Y;
#endif
#ifdef LONG_Z
  boxHalf_Z = boxHalf * LONG_Z;
  boxSize_Z = boxSize * LONG_Z;
#endif
#endif

#ifdef TIME_DEP_ART_VISC
  double f, cs_h;
#endif


#if defined(MAGNETIC) && defined(MAGFORCE)
#if defined(TIME_DEP_MAGN_DISP) || defined(TIME_DEP_EULER_DISSIPATION)
  double mu0 = 1;
#endif
#endif
#ifdef DIVBCLEANING_DEDNER
  double phiphi, tmpb;
#endif

  if(All.ComovingIntegrationOn)
    {
      /* Factors for comoving integration of hydro */
      hubble_a = All.Omega0 / (All.Time * All.Time * All.Time)
	+ (1 - All.Omega0 - All.OmegaLambda) / (All.Time * All.Time) + All.OmegaLambda;

      hubble_a = All.Hubble * sqrt(hubble_a);
      hubble_a2 = All.Time * All.Time * hubble_a;

#ifndef CHEMCOOL
      fac_mu  = pow(All.Time, 3 * (GAMMA - 1) / 2) / All.Time;
      fac_egy = pow(All.Time, 3 * (GAMMA - 1));
      fac_vsic_fix = hubble_a * pow(All.Time, 3 * GAMMA_MINUS1);
#endif

      a3inv = 1 / (All.Time * All.Time * All.Time);
      atime = All.Time;
    }
  else
    hubble_a = hubble_a2 = atime = fac_mu = fac_vsic_fix = a3inv = fac_egy = 1.0;

#if defined(MAGFORCE) && (defined(TIME_DEP_MAGN_DISP) || defined(TIME_DEP_EULER_DISSIPATION))
#ifndef MU0_UNITY
  mu0 *= (4 * M_PI);
  mu0 /= All.UnitTime_in_s * All.UnitTime_in_s *
	     All.UnitLength_in_cm / All.UnitMass_in_g;
  if(All.ComovingIntegrationOn)	
    mu0 /= (All.HubbleParam * All.HubbleParam);
#endif
#endif

  /* `NumSphUpdate' gives the number of particles on this processor that want a force update */
  for(n = 0, NumSphUpdate = 0; n < N_gas; n++)
    {
      if(P[n].ID < 0) /*SINK*/
	continue;
      if(P[n].Ti_endstep == All.Ti_Current)
	NumSphUpdate++;
    }

  numlist = malloc(NTask * sizeof(int) * NTask);
  MPI_Allgather(&NumSphUpdate, 1, MPI_INT, numlist, 1, MPI_INT, MPI_COMM_WORLD);
  for(i = 0, ntot = 0; i < NTask; i++)
    ntot += numlist[i];
  free(numlist);


  noffset = malloc(sizeof(int) * NTask);	/* offsets of bunches in common list */
  nbuffer = malloc(sizeof(int) * NTask);
  nsend_local = malloc(sizeof(int) * NTask);
  nsend = malloc(sizeof(int) * NTask * NTask);
  ndonelist = malloc(sizeof(int) * NTask);


  i = 0;			/* first particle for this task */
  ntotleft = ntot;		/* particles left for all tasks together */

  while(ntotleft > 0)
    {
      for(j = 0; j < NTask; j++)
	nsend_local[j] = 0;

      /* do local particles and prepare export list */
      tstart = second();
      for(nexport = 0, ndone = 0; i < N_gas && nexport < All.BunchSizeHydro - NTask; i++) {
	if(P[i].ID < 0) /*SINK*/
	  continue;
	if(P[i].Ti_endstep == All.Ti_Current)
	  {
	    ndone++;

	    for(j = 0; j < NTask; j++)
	      Exportflag[j] = 0;

	    hydro_evaluate(i, 0);

	    for(j = 0; j < NTask; j++)
	      {
		if(Exportflag[j])
		  {
		    for(k = 0; k < 3; k++)
		      {
			HydroDataIn[nexport].Pos[k] = P[i].Pos[k];
			HydroDataIn[nexport].Vel[k] = SphP[i].VelPred[k];
		      }
		    HydroDataIn[nexport].Hsml = SphP[i].Hsml;
		    HydroDataIn[nexport].Mass = P[i].Mass;
		    HydroDataIn[nexport].DhsmlDensityFactor = SphP[i].DhsmlDensityFactor;
		    HydroDataIn[nexport].Density = SphP[i].Density;
		    HydroDataIn[nexport].Pressure = SphP[i].Pressure;
		    HydroDataIn[nexport].Timestep = P[i].Ti_endstep - P[i].Ti_begstep;
#ifdef CHEMCOOL
                    HydroDataIn[nexport].Gamma    = SphP[i].Gamma;
#endif /* CHEMCOOL */

		    /* calculation of F1 */




#ifdef CHEMCOOL
		    soundspeed_i = sqrt(SphP[i].Gamma * SphP[i].Pressure / SphP[i].Density);
                    if(All.ComovingIntegrationOn)
                      {
                        fac_mu  = pow(All.Time, 3 * (SphP[i].Gamma - 1.0) / 2) / All.Time;
		      }
#elif defined(PRICE_SOUND)
                    if(SphP[i].Density < rho_trans) 
                      soundspeed_i = sqrt(Gamma1 * SphP[i].Pressure / SphP[i].Density);
                    else
                      soundspeed_i = sqrt(Gamma2 * SphP[i].Pressure / SphP[i].Density);
#else
		    soundspeed_i = sqrt(GAMMA * SphP[i].Pressure / SphP[i].Density);
#endif

#ifndef ALTVISCOSITY
		    HydroDataIn[nexport].F1 = fabs(SphP[i].DivVel) /
	        	(fabs(SphP[i].DivVel) + SphP[i].CurlVel +
			0.0001 * soundspeed_i / SphP[i].Hsml / fac_mu);
#else
		    HydroDataIn[nexport].F1 = SphP[i].DivVel;
#endif

#ifdef MAGNETIC
		    for(k = 0; k < 3; k++)
		      HydroDataIn[nexport].BPred[k] = SphP[i].BPred[k];

#if defined(EULERPOTENTIALS) && defined(EULER_DISSIPATION)
      	            HydroDataIn[nexport].EulerA = SphP[i].EulerA;
                    HydroDataIn[nexport].EulerB = SphP[i].EulerB;
#endif

#ifdef DIVBCLEANING_DEDNER
#ifdef SMOOTH_PHI
		    HydroDataIn[nexport].PhiPred = SphP[i].SmoothPhi;
#else
		    HydroDataIn[nexport].PhiPred = SphP[i].PhiPred;
#endif
                    HydroDataIn[nexport].Jump_Entr = SphP[i].Jump_Entr;
                    HydroDataIn[nexport].Max_Jump_Entr = SphP[i].Max_Jump_Entr;
#endif  /* DIVBCLEANING_DEDNER */
#endif /* MAGNETIC */

#ifdef TIME_DEP_ART_VISC
		    HydroDataIn[nexport].alpha = SphP[i].alpha;
#endif

#if defined(TIME_DEP_MAGN_DISP) || defined(TIME_DEP_EULER_DISSIPATION)
		    HydroDataIn[nexport].Balpha = SphP[i].Balpha;
#endif


	  	    HydroDataIn[nexport].Index = i;
		    HydroDataIn[nexport].Task = j;
		    nexport++;
		    nsend_local[j]++;
		  }
	      }
	  }
      }
      tend = second();
      timecomp += timediff(tstart, tend);

      qsort(HydroDataIn, nexport, sizeof(struct hydrodata_in), hydro_compare_key);

      for(j = 1, noffset[0] = 0; j < NTask; j++)
	noffset[j] = noffset[j - 1] + nsend_local[j - 1];

      tstart = second();

      MPI_Allgather(nsend_local, NTask, MPI_INT, nsend, NTask, MPI_INT, MPI_COMM_WORLD);

      tend = second();
      timeimbalance += timediff(tstart, tend);



      /* now do the particles that need to be exported */

      for(level = 1; level < (1 << PTask); level++)
	{
	  tstart = second();
	  for(j = 0; j < NTask; j++)
	    nbuffer[j] = 0;
	  for(ngrp = level; ngrp < (1 << PTask); ngrp++)
	    {
	      maxfill = 0;
	      for(j = 0; j < NTask; j++)
		{
		  if((j ^ ngrp) < NTask)
		    if(maxfill < nbuffer[j] + nsend[(j ^ ngrp) * NTask + j])
		      maxfill = nbuffer[j] + nsend[(j ^ ngrp) * NTask + j];
		}
	      if(maxfill >= All.BunchSizeHydro)
		break;

	      sendTask = ThisTask;
	      recvTask = ThisTask ^ ngrp;

	      if(recvTask < NTask)
		{
		  if(nsend[ThisTask * NTask + recvTask] > 0 || nsend[recvTask * NTask + ThisTask] > 0)
		    {
		      /* get the particles */
		      MPI_Sendrecv(&HydroDataIn[noffset[recvTask]],
				   nsend_local[recvTask] * sizeof(struct hydrodata_in), MPI_BYTE,
				   recvTask, TAG_HYDRO_A,
				   &HydroDataGet[nbuffer[ThisTask]],
				   nsend[recvTask * NTask + ThisTask] * sizeof(struct hydrodata_in), MPI_BYTE,
				   recvTask, TAG_HYDRO_A, MPI_COMM_WORLD, &status);
		    }
		}

	      for(j = 0; j < NTask; j++)
		if((j ^ ngrp) < NTask)
		  nbuffer[j] += nsend[(j ^ ngrp) * NTask + j];
	    }
	  tend = second();
	  timecommsumm += timediff(tstart, tend);

	  /* now do the imported particles */
	  tstart = second();
	  for(j = 0; j < nbuffer[ThisTask]; j++)
	    hydro_evaluate(j, 1);
	  tend = second();
	  timecomp += timediff(tstart, tend);

	  /* do a block to measure imbalance */
	  tstart = second();
	  MPI_Barrier(MPI_COMM_WORLD);
	  tend = second();
	  timeimbalance += timediff(tstart, tend);

	  /* get the result */
	  tstart = second();
	  for(j = 0; j < NTask; j++)
	    nbuffer[j] = 0;
	  for(ngrp = level; ngrp < (1 << PTask); ngrp++)
	    {
	      maxfill = 0;
	      for(j = 0; j < NTask; j++)
		{
		  if((j ^ ngrp) < NTask)
		    if(maxfill < nbuffer[j] + nsend[(j ^ ngrp) * NTask + j])
		      maxfill = nbuffer[j] + nsend[(j ^ ngrp) * NTask + j];
		}
	      if(maxfill >= All.BunchSizeHydro)
		break;

	      sendTask = ThisTask;
	      recvTask = ThisTask ^ ngrp;

	      if(recvTask < NTask)
		{
		  if(nsend[ThisTask * NTask + recvTask] > 0 || nsend[recvTask * NTask + ThisTask] > 0)
		    {
		      /* send the results */
		      MPI_Sendrecv(&HydroDataResult[nbuffer[ThisTask]],
				   nsend[recvTask * NTask + ThisTask] * sizeof(struct hydrodata_out),
				   MPI_BYTE, recvTask, TAG_HYDRO_B,
				   &HydroDataPartialResult[noffset[recvTask]],
				   nsend_local[recvTask] * sizeof(struct hydrodata_out),
				   MPI_BYTE, recvTask, TAG_HYDRO_B, MPI_COMM_WORLD, &status);

		      /* add the result to the particles */
		      for(j = 0; j < nsend_local[recvTask]; j++)
			{
			  source = j + noffset[recvTask];
			  place = HydroDataIn[source].Index;

			  for(k = 0; k < 3; k++)
			    SphP[place].HydroAccel[k] += HydroDataPartialResult[source].Acc[k];

#ifndef POLYTROPE
#ifdef CHEMCOOL
			  SphP[place].DtEntropyVisc += HydroDataPartialResult[source].DtEntropy;
#else
			  SphP[place].DtEntropy += HydroDataPartialResult[source].DtEntropy;
#endif /* CHEMCOOL */
#endif /* POLYTROPE */

			  if(SphP[place].MaxSignalVel < HydroDataPartialResult[source].MaxSignalVel)
			    SphP[place].MaxSignalVel = HydroDataPartialResult[source].MaxSignalVel;

#ifdef MAGNETIC
#ifndef EULERPOTENTIALS
			  for(k = 0; k < 3; k++)
			    SphP[place].DtB[k] += HydroDataPartialResult[source].DtB[k];
#endif
#ifdef DIVBCLEANING_DEDNER
			  SphP[place].DtPhi += HydroDataPartialResult[source].DtPhi;
#endif

#endif /* MAGNETIC */
#if defined(EULERPOTENTIALS) && defined(EULER_DISSIPATION)
	  SphP[place].DtEulerA += HydroDataPartialResult[source].DtEulerA;
	  SphP[place].DtEulerB += HydroDataPartialResult[source].DtEulerB;
#endif

			}
		    }
		}

	      for(j = 0; j < NTask; j++)
		if((j ^ ngrp) < NTask)
		  nbuffer[j] += nsend[(j ^ ngrp) * NTask + j];
	    }
	  tend = second();
	  timecommsumm += timediff(tstart, tend);

	  level = ngrp - 1;
	}

      MPI_Allgather(&ndone, 1, MPI_INT, ndonelist, 1, MPI_INT, MPI_COMM_WORLD);
      for(j = 0; j < NTask; j++)
	ntotleft -= ndonelist[j];
    }

  free(ndonelist);
  free(nsend);
  free(nsend_local);
  free(nbuffer);
  free(noffset);

#ifdef CHEMCOOL
  for(i=0; i<N_gas; i++) 
    {
      if(P[i].Ti_endstep == All.Ti_Current) 
        {
          gamm1 = SphP[i].Gamma - 1.0;
	  SphP[i].DtEntropyVisc *= gamm1 / (hubble_a2 * pow(SphP[i].Density, gamm1));
        }
    }
#endif /* CHEMCOOL */

  /* do final operations on results */
  tstart = second();

  for(i = 0; i < N_gas; i++) {
    if(P[i].ID < 0) /*SINK*/
      continue;
    if(P[i].Ti_endstep == All.Ti_Current)
      {
#ifndef POLYTROPE
#ifndef CHEMCOOL
	SphP[i].DtEntropy *= GAMMA_MINUS1 / (hubble_a2 * pow(SphP[i].Density, GAMMA_MINUS1));
#endif 
#endif

#ifdef MAGNETIC
#ifndef EULERPOTENTIALS 
	  /* take care of cosmological dilution */
	  if(All.ComovingIntegrationOn)
	    for(k = 0; k < 3; k++)
	      SphP[i].DtB[k] -= 2.0 * SphP[i].BPred[k];
#endif
#endif


#ifdef SPH_BND_PARTICLES
	if(P[i].ID == 0)
	  {
#ifndef POLYTROPE
#ifdef CHEMCOOL
	    SphP[i].DtEntropyVisc = 0;
#else
	    SphP[i].DtEntropy = 0;
#endif
#endif
	    for(k = 0; k < 3; k++)
	      SphP[i].HydroAccel[k] = 0;
	  }
#endif

#ifdef CORRECT_SINK_PRESSURES
      /* Need to correct for particles that are on the boundary of a sink.
      If the SPH particle sits within 0.5h of the sink boundary, we create
      a ghost particle, that sits at the sink's boundary between the sink and
      the SPH particle. The ghost inherits the properties of the current SPH
      particle, creating a constant pressure boundary over the sinks accretion
      radius. */
      h_i = SphP[i].Hsml;
      h_i2 = h_i * h_i;
      p_over_rho2_i = SphP[i].Pressure / SphP[i].Density / SphP[i].Density * SphP[i].DhsmlDensityFactor;
      mass = P[i].Mass;
      hacc2 = All.ROuter * All.ROuter;
      for(j = 0; j < All.TotN_sinks; j++)
        {
          dx = All.SinkStoreXyz[j][0] - P[i].Pos[0];
          dy = All.SinkStoreXyz[j][1] - P[i].Pos[1];
          dz = All.SinkStoreXyz[j][2] - P[i].Pos[2];
          r2 = dx * dx + dy * dy + dz * dz;
          if ( (hacc2 + h_i2*0.25) < r2 )
            {
              r = sqrt(r2);
              ghostposx = P[i].Pos[0] + 0.5*dx*h_i;
              ghostposy = P[i].Pos[1] + 0.5*dy*h_i;
              ghostposz = P[i].Pos[2] + 0.5*dz*h_i;
              dx = P[i].Pos[0] - ghostposx;
              dy = P[i].Pos[1] - ghostposy;
              dz = P[i].Pos[2] - ghostposz;
              r2 = dx * dx + dy * dy + dz * dz;
              r = sqrt(r2);
              hinv = 1.0 /h_i;
              hinv4 = hinv * hinv * hinv * hinv;
              u = r * hinv; 
              dwk_i = hinv4 * u * (KERNEL_COEFF_3 * u - KERNEL_COEFF_4);
              hfc = mass * 2.0*(p_over_rho2_i * dwk_i) / r;
              SphP[i].HydroAccel[0] -= hfc * dx;
              SphP[i].HydroAccel[1] -= hfc * dy;
              SphP[i].HydroAccel[2] -= hfc * dz;
            }
        }
#endif  /* CORRECT_SINK_PRESSURES */


#ifdef TIME_DEP_ART_VISC
/*
	  cs_h = sqrt(GAMMA * SphP[i].Pressure / SphP[i].Density) / PPP[i].Hsml;
	  f = fabs(SphP[i].DivVel) /
	    (fabs(SphP[i].DivVel) + SphP[i].CurlVel + 0.0001 * cs_h / fac_mu);
	  SphP[i].Dtalpha = -(SphP[i].alpha - All.AlphaMin) * All.DecayTime *
	    0.5 * SphP[i].MaxSignalVel / (PPP[i].Hsml * fac_mu)
	    + f * All.ViscSource * DMAX(0.0, -SphP[i].DivVel);
*/

	  SphP[i].Dtalpha = -(SphP[i].alpha - All.AlphaMin) * 0.1 * SphP[i].MaxSignalVel / SphP[i].Hsml
            + (2.0 - SphP[i].alpha) * DMAX(0.0, -SphP[i].DivVel);

	  if(All.ComovingIntegrationOn)
	    SphP[i].Dtalpha /= (hubble_a * All.Time * All.Time);
#endif


#ifdef MAGNETIC
#if defined(TIME_DEP_MAGN_DISP) || defined(TIME_DEP_EULER_DISSIPATION)
	  SphP[i].DtBalpha = -(SphP[i].Balpha - All.ArtMagDispMin) * All.ArtMagDispTime *
                    	    0.5 * SphP[i].MaxSignalVel / (SphP[i].Hsml * fac_mu)
#ifndef ROT_IN_MAG_DIS
              + All.ArtMagDispSource * fabs(SphP[i].divB) / sqrt(mu0 * SphP[i].Density);
#else
	  + All.ArtMagDispSource / sqrt(mu0 * SphP[i].Density) *
	  DMAX(fabs(SphP[i].divB), fabs(sqrt(SphP[i].RotB[0] * SphP[i].RotB[0] +
					     SphP[i].RotB[1] * SphP[i].RotB[1] +
					     SphP[i].RotB[2] * SphP[i].RotB[2])));
#endif /* End ROT_IN_MAG_DIS    */
#endif /* End TIME_DEP_MAGN_DISP */
#ifdef DIVBCLEANING_DEDNER
	    tmpb = 0.5 * SphP[i].MaxSignalVel;
            phiphi = 0;
            if(fabs(SphP[i].Jump_Entr) < 0.01)
	      phiphi = tmpb * atime * atime * All.DivBcleanHyperbolicSigma
#ifdef SMOOTH_PHI
	      * SphP[i].SmoothDivB;
	    phiphi += SphP[i].SmoothPhi * 
#else
	      * SphP[i].divB;
	    phiphi += SphP[i].PhiPred * 
#endif /* SMOOTH_PHI */
	    All.DivBcleanParabolicSigma / SphP[i].Hsml;
	      
	    if(All.ComovingIntegrationOn)
	      SphP[i].DtPhi = -phiphi * tmpb / hubble_a 
#ifdef SMOOTH_PHI
	      	- SphP[i].SmoothPhi ;
#else
	        - SphP[i].PhiPred ;
#endif
	    else
	      SphP[i].DtPhi =  -phiphi * tmpb ;
#endif /* End DEDNER */
#endif /* End MAGNETIC */
      }
  }

  tend = second();
  timecomp += timediff(tstart, tend);

  /* collect some timing information */

  MPI_Reduce(&timecomp, &sumt, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
  MPI_Reduce(&timecommsumm, &sumcomm, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
  MPI_Reduce(&timeimbalance, &sumimbalance, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);

  if(ThisTask == 0)
    {
      All.CPU_HydCompWalk += sumt / NTask;
      All.CPU_HydCommSumm += sumcomm / NTask;
      All.CPU_HydImbalance += sumimbalance / NTask;
    }
}


/*! This function is the 'core' of the SPH force computation. A target
 *  particle is specified which may either be local, or reside in the
 *  communication buffer.
 */
void hydro_evaluate(int target, int mode)
{

  int j, k, n, timestep, startnode, numngb;
  FLOAT *pos, *vel;
  FLOAT mass, h_i, dhsmlDensityFactor, rho, pressure, f1, f2;
  double acc[3], maxSignalVel;
  double dx, dy, dz, dvx, dvy, dvz, dmax1, dmax2;
  double h_i2, hinv, hinv4;
  double p_over_rho2_i, p_over_rho2_j, soundspeed_i, soundspeed_j;
  double p_over_rho2_i_ext, p_over_rho2_j_ext;
  double hfc, dwk_i, vdotr, vdotr2, visc, mu_ij, rho_ij, vsig;
  double h_j, dwk_j, r, r2, u, hfc_visc;
#ifdef CORRECT_SINK_PRESSURES
  int numngb_sph, num_sinks_press_correct;
  double ghost_position[1000][3];
#endif
#ifndef POLYTROPE
  double dtEntropy;
#endif

#if defined(PRICE_SOUND)
  double rho_trans = 1.0E-14/All.UnitDensity_in_cgs;
  double Gamma1 = 1.0;
  double Gamma2 = 1.4;
#endif


  double BulkVisc_ij;
#ifdef TIME_DEP_ART_VISC
  FLOAT alpha;
#endif

#ifdef ALTVISCOSITY
  double mu_i, mu_j;
#endif

#ifndef NOVISCOSITYLIMITER
  double dt;
#endif

#ifdef MAGNETIC
  FLOAT *bpred;

#ifndef EULERPOTENTIALS
  double dtB[3];
#endif

  double dBx, dBy, dBz;
  double magfac, magfac_i, magfac_j, magfac_i_base;
  double mu0_1;

#ifdef MAGFORCE
  double mm_i[3][3], mm_j[3][3];
  double b2_i, b2_j;
  int l;
#endif

#if defined(MAGNETIC_DISSIPATION) || defined(DIVBCLEANING_DEDNER) || defined(EULER_DISSIPATION)
  double magfac_sym;
#endif

#if defined(MAGNETIC_DISSIPATION) || defined(EULER_DISSIPATION)
  double dTu_diss_b, Balpha_ij;
#if defined(TIME_DEP_MAGN_DISP) || defined(TIME_DEP_EULER_DISSIPATION) 
  double Balpha;
#endif
#endif

#ifdef EULER_DISSIPATION
  double eulA, eulB, dTu_diss_eul, alpha_ij_eul;
  double dteulA, dteulB;
#endif


#ifdef DIVBCLEANING_DEDNER
  double PhiPred, DtPhi, phifac;
  double jump_entr, max_jump_entr;
#endif


#ifdef MAGNETIC_SIGNALVEL
  double magneticspeed_i, magneticspeed_j, vcsa2_i, vcsa2_j, Bpro2_i, Bpro2_j;
#endif

#endif /*MAGNETIC*/  

#ifdef CONVENTIONAL_VISCOSITY
  double c_ij, h_ij;
#endif


  if(mode == 0)
    {
      pos = P[target].Pos;
      vel = SphP[target].VelPred;
      h_i = SphP[target].Hsml;
      mass = P[target].Mass;
      dhsmlDensityFactor = SphP[target].DhsmlDensityFactor;
      rho = SphP[target].Density;
      pressure = SphP[target].Pressure;
      timestep = P[target].Ti_endstep - P[target].Ti_begstep;
#ifdef CHEMCOOL
      soundspeed_i = sqrt(SphP[target].Gamma * pressure / rho);
      if(All.ComovingIntegrationOn)
        {
          fac_mu  = pow(All.Time, 3 * (SphP[target].Gamma - 1.0) / 2) / All.Time;
	}
#elif defined(PRICE_SOUND)
       if(rho < rho_trans)
         soundspeed_i = sqrt(Gamma1 * pressure / rho);
       else
         soundspeed_i = sqrt(Gamma2 * pressure / rho);
#else
      soundspeed_i = sqrt(GAMMA * pressure / rho);
#endif

#ifndef ALTVISCOSITY
      f1 = fabs(SphP[target].DivVel) /
	(fabs(SphP[target].DivVel) + SphP[target].CurlVel +
	 0.0001 * soundspeed_i / SphP[target].Hsml / fac_mu);
#else
      f1 = SphP[target].DivVel;
#endif

#ifdef MAGNETIC
      bpred = SphP[target].BPred;
#ifdef DIVBCLEANING_DEDNER
#ifdef SMOOTH_PHI
      PhiPred = SphP[target].SmoothPhi;
#else
      PhiPred = SphP[target].PhiPred;
#endif /* SMOOTH_PHI */
      jump_entr = SphP[target].Jump_Entr;
      max_jump_entr = SphP[target].Max_Jump_Entr; 
#endif /* DIVBCLEANING_DEDNER */
#endif /* MAGNETIC */

#ifdef EULER_DISSIPATION
      eulA = SphP[target].EulerA;
      eulB = SphP[target].EulerB;
#endif

#ifdef TIME_DEP_ART_VISC
      alpha = SphP[target].alpha;
#endif


/*
      f1 = fabs(SphP[target].DivVel) /
	(fabs(SphP[target].DivVel) + SphP[target].CurlVel +
	 0.0001 * soundspeed_i / SphP[target].Hsml / fac_mu);
*/
#if defined(TIME_DEP_MAGN_DISP) || defined(TIME_DEP_EULER_DISSIPATION)
      Balpha = SphP[target].Balpha;
#endif

    }
  else
    {
      pos = HydroDataGet[target].Pos;
      vel = HydroDataGet[target].Vel;
      h_i = HydroDataGet[target].Hsml;
      mass = HydroDataGet[target].Mass;
      dhsmlDensityFactor = HydroDataGet[target].DhsmlDensityFactor;
      rho = HydroDataGet[target].Density;
      pressure = HydroDataGet[target].Pressure;
      timestep = HydroDataGet[target].Timestep;
#ifdef CHEMCOOL
      soundspeed_i = sqrt(HydroDataGet[target].Gamma * pressure / rho);
#elif defined(PRICE_SOUND)
      if(rho < rho_trans)
        soundspeed_i = sqrt(Gamma1 * pressure / rho);
      else
        soundspeed_i = sqrt(Gamma2 * pressure / rho);
#else
      soundspeed_i = sqrt(GAMMA * pressure / rho);
#endif
      f1 = HydroDataGet[target].F1;
#ifdef MAGNETIC
      bpred = HydroDataGet[target].BPred;
#ifdef DIVBCLEANING_DEDNER
      PhiPred = HydroDataGet[target].PhiPred;
      jump_entr = HydroDataGet[target].Jump_Entr;
      max_jump_entr = HydroDataGet[target].Max_Jump_Entr;
#endif
#ifdef EULER_DISSIPATION
      eulA = HydroDataGet[target].EulerA;
      eulB = HydroDataGet[target].EulerB;
#endif
#endif

#ifdef TIME_DEP_ART_VISC
      alpha = HydroDataGet[target].alpha;
#endif

#if defined(TIME_DEP_MAGN_DISP) || defined(TIME_DEP_EULER_DISSIPATION)
      Balpha = HydroDataGet[target].Balpha;
#endif

    }


  /* initialize variables before SPH loop is started */
  acc[0] = acc[1] = acc[2] = 0;


#ifndef POLYTROPE
  dtEntropy = 0;
#endif

  maxSignalVel = 0;

#ifdef MAGNETIC
#ifndef EULERPOTENTIALS
  for(k = 0; k < 3; k++)
    dtB[k] = 0;
#endif
#ifdef EULER_DISSIPATION
  dteulA = 0;
  dteulB = 0;
#endif
#ifdef DIVBCLEANING_DEDNER
  DtPhi = 0;
#endif
#ifdef MAGFORCE
  magfac_i_base = 1 / (rho * rho);
  mu0_1 = 1;
#ifndef MU0_UNITY
  magfac_i_base /= (4 * M_PI);
  mu0_1 /= (4 * M_PI);
  mu0_1 *= All.UnitTime_in_s * All.UnitTime_in_s *
	All.UnitLength_in_cm / All.UnitMass_in_g;
  if(All.ComovingIntegrationOn)
    mu0_1 /= (All.HubbleParam * All.HubbleParam);
#endif
#ifdef CORRECTBFRC
  magfac_i_base *= dhsmlDensityFactor;
#endif
  for(k = 0, b2_i = 0; k < 3; k++)
    {
      b2_i += bpred[k] * bpred[k];
      for(l = 0; l < 3; l++)
	mm_i[k][l] = bpred[k] * bpred[l];
    }
  for(k = 0; k < 3; k++)
    mm_i[k][k] -= 0.5 * b2_i;
#ifdef MAGNETIC_SIGNALVEL
  vcsa2_i = soundspeed_i * soundspeed_i + mu0_1 * b2_i / rho;
#endif
#endif /* end of MAGFORCE */
#endif /* end of MAGNETIC */



  p_over_rho2_i = pressure / (rho * rho) * dhsmlDensityFactor;
  h_i2 = h_i * h_i;

  /* Now start the actual SPH computation for this particle */
  startnode = All.MaxPart;
  do
    {
      numngb = ngb_treefind_pairs(&pos[0], h_i, &startnode);
      for(n = 0; n < numngb; n++)
	{
	  j = Ngblist[n];
	  dx = pos[0] - P[j].Pos[0];
	  dy = pos[1] - P[j].Pos[1];
	  dz = pos[2] - P[j].Pos[2];
#ifdef PERIODIC			/*  find the closest image in the given box size  */
	  if(dx > boxHalf_X)
	    dx -= boxSize_X;
	  if(dx < -boxHalf_X)
	    dx += boxSize_X;
	  if(dy > boxHalf_Y)
	    dy -= boxSize_Y;
	  if(dy < -boxHalf_Y)
	    dy += boxSize_Y;
	  if(dz > boxHalf_Z)
	    dz -= boxSize_Z;
	  if(dz < -boxHalf_Z)
	    dz += boxSize_Z;
#endif
	  r2 = dx * dx + dy * dy + dz * dz;
	  h_j = SphP[j].Hsml;
	  if(r2 < h_i2 || r2 < h_j * h_j)
	    {
	      r = sqrt(r2);
	      if(r > 0)
		{
		  p_over_rho2_j = SphP[j].Pressure / (SphP[j].Density * SphP[j].Density);
#ifdef CHEMCOOL
		  soundspeed_j = sqrt(SphP[j].Gamma * p_over_rho2_j * SphP[j].Density);
#elif defined(PRICE_SOUND)
                  if(SphP[j].Density < rho_trans)
                     soundspeed_j = sqrt(Gamma1 * p_over_rho2_j / SphP[j].Density);
                  else
                     soundspeed_j = sqrt(Gamma2 * p_over_rho2_j / SphP[j].Density);
#else
		  soundspeed_j = sqrt(GAMMA * p_over_rho2_j * SphP[j].Density);
#endif
		  dvx = vel[0] - SphP[j].VelPred[0];
		  dvy = vel[1] - SphP[j].VelPred[1];
		  dvz = vel[2] - SphP[j].VelPred[2];
		  vdotr = dx * dvx + dy * dvy + dz * dvz;

		  if(All.ComovingIntegrationOn)
		    vdotr2 = vdotr + hubble_a2 * r2;
		  else
		    vdotr2 = vdotr;

            rho_ij = 0.5 * (rho + SphP[j].Density); /* use rho_ij for ALL particles!*/

		  if(r2 < h_i2)
		    {
		      hinv = 1.0 / h_i;
#ifndef  TWODIMS
		      hinv4 = hinv * hinv * hinv * hinv;
#else
		      hinv4 = hinv * hinv * hinv / boxSize_Z;
#endif
		      u = r * hinv;

#if defined(CORE_TRIANGLE_KERNEL) /* taken from Read et al. 2009 */
	if(u < 0.33333333)
          {
            dwk_i = hinv4 * (-5.070699962);
          }
        else if( (u > 0.33333333) && (u < 0.5) )
          {
            dwk_i = hinv4 * u * ( 45.63629966 * u - 30.42419977); 
          }
        else
          {
            dwk_i = hinv4 * (-15.21209989) * (1.0 - u) * (1.0 - u);
          }
#elif defined(QUINTIC_KERNEL)

	if(u < 0.33333333)
          {
            dwk_i = hinv4 * 16.32599123 * ( -5.0*pow(1.0-u,4.0) + 23.33333333*pow(0.66666666 - u,4.0) - 31.66666666*pow(0.33333333 - u,4.0) );
          }
        else if( (u > 0.33333333) && (u < 0.66666666) )
          {
            dwk_i = hinv4 * 16.32599123 * ( -5.0*pow(1.0-u,4.0) + 23.33333333*pow(0.66666666 - u,4.0) );
          }
        else
          {
            dwk_i = hinv4 * 16.32599123 * ( -5.0*pow(1.0-u,4.0) );
          }
#else
		      if(u < 0.5)
			dwk_i = hinv4 * u * (KERNEL_COEFF_3 * u - KERNEL_COEFF_4);
		      else
			dwk_i = hinv4 * KERNEL_COEFF_6 * (1.0 - u) * (1.0 - u);
#endif
		    }
		  else
		    {
		      dwk_i = 0;
		    }

		  if(r2 < h_j * h_j)
		    {
		      hinv = 1.0 / h_j;
#ifndef  TWODIMS
		      hinv4 = hinv * hinv * hinv * hinv;
#else
		      hinv4 = hinv * hinv * hinv / boxSize_Z;
#endif
		      u = r * hinv;

#if defined(CORE_TRIANGLE_KERNEL)
	if(u < 0.33333333)
          {
            dwk_j = hinv4 * (-5.070699962);
          }
        else if( (u > 0.33333333) && (u < 0.5) )
          {
            dwk_j = hinv4 * u * ( 45.63629966 * u - 30.42419977); 
          }
        else
          {
            dwk_j = hinv4 * (-15.21209989) * (1.0 - u) * (1.0 - u);
          }
#elif defined(QUINTIC_KERNEL)

	if(u < 0.33333333)
          {
            dwk_j = hinv4 * 16.32599123 * ( -5.0*pow(1.0-u,4.0) + 23.33333333*pow(0.66666666 - u,4.0) - 31.66666666*pow(0.33333333 - u,4.0) );
          }
        else if( (u > 0.33333333) && (u < 0.66666666) )
          {
            dwk_j = hinv4 * 16.32599123 * ( -5.0*pow(1.0-u,4.0) + 23.33333333*pow(0.66666666 - u,4.0) );
          }
        else
          {
            dwk_j = hinv4 * 16.32599123 * ( -5.0*pow(1.0-u,4.0) );
          }

#else
		      if(u < 0.5)
			dwk_j = hinv4 * u * (KERNEL_COEFF_3 * u - KERNEL_COEFF_4);
		      else
			dwk_j = hinv4 * KERNEL_COEFF_6 * (1.0 - u) * (1.0 - u);
#endif
		    }
		  else
		    {
		      dwk_j = 0;

		    }

		  if(soundspeed_i + soundspeed_j > maxSignalVel)
		    maxSignalVel = soundspeed_i + soundspeed_j;
#ifdef MAGNETIC
		  dBx = bpred[0] - SphP[j].BPred[0];
		  dBy = bpred[1] - SphP[j].BPred[1];
		  dBz = bpred[2] - SphP[j].BPred[2];
		  magfac = P[j].Mass / r;	/* we moved 'dwk_i / rho' down ! */

		  if(All.ComovingIntegrationOn)
		    magfac *= 1. / (hubble_a * All.Time * All.Time);
		  /* last factor takes care of all cosmological prefactor */
#ifdef CORRECTDB
		  magfac *= dhsmlDensityFactor;
#endif
#if defined(MAGNETIC_DISSIPATION) || defined(DIVBCLEANING_DEDNER) || defined(EULER_DISSIPATION)
#ifdef OLD_MAGFAC_SYM
		 magfac_sym = magfac * dwk_i /rho;
#else
/*CAUTION: the new definition of magfac_sym has no 1./rho term,
so all expressions containing this need to be modified accordingly !!!*/
         magfac_sym = magfac * (dwk_i + dwk_j) * 0.5;
#endif
#endif /*MAGNETIC_DISSIPATION*/

#if defined(MAGNETIC_DISSIPATION) || defined(EULER_DISSIPATION)
#if defined(TIME_DEP_MAGN_DISP) || defined(TIME_DEP_EULER_DISSIPATION)
		  Balpha_ij = 0.5 * (Balpha + SphP[j].Balpha);
#else
		  Balpha_ij = All.ArtMagDispConst;
#endif
#endif
		  magfac *= dwk_i / rho;
#ifndef EULERPOTENTIALS
		  dtB[0] +=
		    magfac * ((bpred[0] * dvy - bpred[1] * dvx) * dy +
			      (bpred[0] * dvz - bpred[2] * dvx) * dz);
		  dtB[1] +=
		    magfac * ((bpred[1] * dvz - bpred[2] * dvy) * dz +
			      (bpred[1] * dvx - bpred[0] * dvy) * dx);
		  dtB[2] +=
		    magfac * ((bpred[2] * dvx - bpred[0] * dvz) * dx +
			      (bpred[2] * dvy - bpred[1] * dvz) * dy);
#endif

#if defined(DIVBINDUCTION) && !defined(EULERPOTENTIALS)
		  dtB[0] += magfac * vel[0] * (dBx * dx + dBy * dy + dBz * dz);
		  dtB[1] += magfac * vel[1] * (dBx * dx + dBy * dy + dBz * dz);
		  dtB[2] += magfac * vel[2] * (dBx * dx + dBy * dy + dBz * dz);
#endif


#ifdef MAGFORCE
                  magfac_j = 1 / (SphP[j].Density * SphP[j].Density);
#ifndef MU0_UNITY
		  magfac_j /= (4 * M_PI);
#endif
#ifdef CORRECTBFRC
		  magfac_j *= dwk_j * SphP[j].DhsmlDensityFactor;
		  magfac_i = dwk_i * magfac_i_base;
#else
		  magfac_i = magfac_i_base;
#endif
		  for(k = 0, b2_j = 0; k < 3; k++)
		    {
		      b2_j += SphP[j].BPred[k] * SphP[j].BPred[k];
		      for(l = 0; l < 3; l++)
			mm_j[k][l] = SphP[j].BPred[k] * SphP[j].BPred[l];
		    }
		  for(k = 0; k < 3; k++)
		    mm_j[k][k] -= 0.5 * b2_j;
#ifdef DIVBCLEANING_DEDNER
		      phifac = magfac_sym * rho / rho_ij;
		      phifac *= (PhiPred - SphP[j].PhiPred)  / rho_ij;
		      if(max_jump_entr < 0.01)
			{
		        if(All.ComovingIntegrationOn){
			  dtB[0] += phifac * atime * atime * dx;
			  dtB[1] += phifac * atime * atime * dy;
			  dtB[2] += phifac * atime * atime * dz;
#ifndef POLYTROPE			
              dtEntropy += fac_egy * mu0_1 * phifac * atime * atime *
                                       (bpred[0] * dx + bpred[1] * dy + bpred[2] * dz); 
#endif 
			}else{
			  dtB[0] += phifac * dx;
			  dtB[1] += phifac * dy;
			  dtB[2] += phifac * dz;
#ifndef POLYTROPE
              dtEntropy += mu0_1 * phifac * (bpred[0] * dx + bpred[1] * dy + bpred[2] * dz);
#endif			
			}
			}
#endif
#ifdef MAGNETIC_SIGNALVEL
		  
                  vcsa2_j = soundspeed_j * soundspeed_j + mu0_1 * b2_j / SphP[j].Density;
		  Bpro2_j = (SphP[j].BPred[0] * dx + SphP[j].BPred[1] * dy + SphP[j].BPred[2] * dz) / r;
		  Bpro2_j *= Bpro2_j;

                  magneticspeed_j = sqrt(vcsa2_j +
					 sqrt(DMAX((vcsa2_j * vcsa2_j -
						    4 * soundspeed_j * soundspeed_j * Bpro2_j
						    * mu0_1 / SphP[j].Density), 0))) / 1.4142136;
		  Bpro2_i = (bpred[0] * dx + bpred[1] * dy + bpred[2] * dz) / r;
		  Bpro2_i *= Bpro2_i;
		  magneticspeed_i = sqrt(vcsa2_i +
					 sqrt(DMAX((vcsa2_i * vcsa2_i -
						    4 * soundspeed_i * soundspeed_i * Bpro2_i
						    * mu0_1 / rho), 0))) / 1.4142136;
#endif
#ifdef MAGNETIC_DISSIPATION

		  dTu_diss_b = -magfac_sym * Balpha_ij * (dBx * dBx + dBy * dBy + dBz * dBz);
#endif
#ifdef CORRECTBFRC
		  magfac = P[j].Mass / r;
#else
		  magfac = P[j].Mass * 0.5 * (dwk_i + dwk_j) / r;
#endif
		  if(All.ComovingIntegrationOn)
		    magfac *= pow(All.Time, 3 * GAMMA);
		  /* last factor takes care of all cosmological prefactor */
#ifndef MU0_UNITY
		  magfac *= All.UnitTime_in_s * All.UnitTime_in_s *
			All.UnitLength_in_cm / All.UnitMass_in_g;
		  if(All.ComovingIntegrationOn)
			 magfac /= (All.HubbleParam * All.HubbleParam);
		  /* take care of B unit conversion into GADGET units ! */
#endif
		  for(k = 0; k < 3; k++)
		    acc[k] +=
		      magfac * ((mm_i[k][0] * magfac_i + mm_j[k][0] * magfac_j) * dx +
				(mm_i[k][1] * magfac_i + mm_j[k][1] * magfac_j) * dy +
				(mm_i[k][2] * magfac_i + mm_j[k][2] * magfac_j) * dz);

#if defined(DIVBFORCE) && !defined(EULERPOTENTIALS)
		  for(k = 0; k < 3; k++)
		    acc[k] -=
		      magfac * (((bpred[k] * bpred[0]) * magfac_i
				 + (bpred[k] * SphP[j].BPred[0]) * magfac_j) * dx
				+ ((bpred[k] * bpred[1]) * magfac_i
                                 + (bpred[k] * SphP[j].BPred[1]) * magfac_j) * dy
				+ ((bpred[k] * bpred[2]) * magfac_i +
				   (bpred[k] * SphP[j].BPred[2]) * magfac_j) * dz);
#endif

#if defined(EULERPOTENTIALS) && defined(EULER_HYBRID)
                   for(k = 0; k < 3; k++)
                     acc[k] -=
                       magfac * (((bpred[k] * bpred[0]) * magfac_i
                                  + (bpred[k] * SphP[j].BPred[0]) * magfac_j) * dx
                                 + ((bpred[k] * bpred[1]) * magfac_i
                                  + (bpred[k] * SphP[j].BPred[1]) * magfac_j) * dy
                                 + ((bpred[k] * bpred[2]) * magfac_i +
                                    (bpred[k] * SphP[j].BPred[2]) * magfac_j) * dz);
#endif

#endif
#endif /* end of MAGNETIC */

#ifndef MAGNETIC_SIGNALVEL
		  vsig = soundspeed_i + soundspeed_j;
#else
		  vsig = magneticspeed_i + magneticspeed_j;
#endif
		  if(vsig > maxSignalVel)
		    maxSignalVel = vsig;



		  if(vdotr2 < 0)	/* ... artificial viscosity */
		    {
#ifdef CHEMCOOL
                    if(All.ComovingIntegrationOn)
                      {
                        fac_mu  = pow(All.Time, 3 * (SphP[j].Gamma - 1.0) / 2) / All.Time;
		      } 
#endif /* CHEMCOOL */
		    //mu_ij = fac_mu * vdotr2 / r;	/* note: this is negative! */

		    //vsig = soundspeed_i + soundspeed_j - 3 * mu_ij;

#ifndef ALTVISCOSITY
#ifndef CONVENTIONAL_VISCOSITY
		      mu_ij = fac_mu * vdotr2 / r;	/* note: this is negative! */
#else
		      c_ij = 0.5 * (soundspeed_i + soundspeed_j);
		      h_ij = 0.5 * (h_i + h_j);
		      mu_ij = fac_mu * h_ij * vdotr2 / (r2 + 0.0001 * h_ij * h_ij);
#endif
#ifdef MAGNETIC
		      vsig -= 1.5 * mu_ij;
#else
		      vsig -= 3 * mu_ij;
#endif


		      if(vsig > maxSignalVel)
			maxSignalVel = vsig;

		      /*rho_ij = 0.5 * (rho + SphP[j].Density);*/
		      f2 =
			fabs(SphP[j].DivVel) / (fabs(SphP[j].DivVel) + SphP[j].CurlVel +
						0.0001 * soundspeed_j / fac_mu / SphP[j].Hsml);

#ifdef NO_SHEAR_VISCOSITY_LIMITER
		      f1 = f2 = 1;
#endif
#ifdef TIME_DEP_ART_VISC
		      BulkVisc_ij = 0.5 * (alpha + SphP[j].alpha);
#else
		      BulkVisc_ij = All.ArtBulkViscConst;
#endif
#ifndef CONVENTIONAL_VISCOSITY
		      visc = 0.25 * BulkVisc_ij * vsig * (-mu_ij) / rho_ij * (f1 + f2);
#else
		      visc =
			(-BulkVisc_ij * mu_ij * c_ij + 2 * BulkVisc_ij * mu_ij * mu_ij) /
			rho_ij * (f1 + f2) * 0.5;
#endif

#else /* start of ALTVISCOSITY block */
		      if(f1 < 0)
			mu_i = h_i * fabs(f1);	/* f1 hold here the velocity divergence of particle i */
		      else
			mu_i = 0;
		      if(SphP[j].DivVel < 0)
			mu_j = h_j * fabs(SphP[j].DivVel);
		      else
			mu_j = 0;
		      visc = All.ArtBulkViscConst * ((soundspeed_i + mu_i) * mu_i / rho +
						     (soundspeed_j + mu_j) * mu_j / SphP[j].Density);
#endif /* end of ALTVISCOSITY block */





		      //visc = 0.25 * All.ArtBulkViscConst * vsig * (-mu_ij) / rho_ij * (f1 + f2);

		      /* .... end artificial viscosity evaluation */
#ifndef NOVISCOSITYLIMITER
		      /* make sure that viscous acceleration is not too large */
		      dt = imax(timestep, (P[j].Ti_endstep - P[j].Ti_begstep)) * All.Timebase_interval;
		      if(dt > 0 && (dwk_i + dwk_j) < 0)
			{
#ifdef CHEMCOOL
                        if(All.ComovingIntegrationOn)
                          {
                            fac_vsic_fix = hubble_a * pow(All.Time, 3 * (SphP[j].Gamma - 1.0));
		          } 
#endif /* CHEMCOOL */
			  visc = dmin(visc, 0.5 * fac_vsic_fix * vdotr2 /
				      (0.5 * (mass + P[j].Mass) * (dwk_i + dwk_j) * r * dt));
			}
#endif
		    }
		  else
		    visc = 0;

#ifdef SET_VISC_MAX_VALUE
                  if(visc > 1.1) visc = 1.1;
#endif

		  p_over_rho2_j *= SphP[j].DhsmlDensityFactor;

		  hfc_visc = 0.5 * P[j].Mass * visc * (dwk_i + dwk_j) / r;
#ifdef EXTERNALPRESSURE 
	          p_over_rho2_j_ext = p_over_rho2_j - All.ExternalPressure/(SphP[j].Density * SphP[j].Density);
		  p_over_rho2_i_ext = p_over_rho2_i - All.ExternalPressure/(rho*rho);
                  hfc = hfc_visc + P[j].Mass * (p_over_rho2_i_ext * dwk_i + p_over_rho2_j_ext * dwk_j) / r;
#else
#ifdef RPSPH
                  dweight = P[j].Mass/SphP[j].Density/SphP[j].Density*dwk_i;
	          hfc =  hfc_visc + dweight * (SphP[j].Pressure - pressure)/r;
#else
		  hfc = hfc_visc + P[j].Mass * (p_over_rho2_i * dwk_i + p_over_rho2_j * dwk_j) / r;
#endif
#endif

		  acc[0] -= hfc * dx;
		  acc[1] -= hfc * dy;
		  acc[2] -= hfc * dz;

#ifndef POLYTROPE
 		  dtEntropy += 0.5 * hfc_visc * vdotr2;
#endif

#ifdef MAGNETIC
#ifdef EULER_DISSIPATION
        alpha_ij_eul = Balpha_ij;
#ifdef OLD_MAGFAC_SYM
		dteulA += alpha_ij_eul * 0.5 * vsig * (eulA - SphP[j].EulerA) * magfac_sym * r;
		dteulB += alpha_ij_eul * 0.5 * vsig * (eulB - SphP[j].EulerB) * magfac_sym * r;
#else
		dteulA += alpha_ij_eul * 0.5 * vsig * (eulA - SphP[j].EulerA) * magfac_sym * r * rho / (rho_ij * rho_ij);
		dteulB += alpha_ij_eul * 0.5 * vsig * (eulB - SphP[j].EulerB) * magfac_sym * r * rho / (rho_ij * rho_ij);
#endif/*OLD_MAGFAC_SYM*/
#ifndef POLYTROPE
		      dTu_diss_eul = -magfac_sym * alpha_ij_eul * (dBx * dBx + dBy * dBy + dBz * dBz);
#ifdef OLD_MAGFAC_SYM
                      dtEntropy += dTu_diss_eul * 0.25 * vsig * mu0_1 * r * 1.0 / rho;
#else
                      dtEntropy += dTu_diss_eul * 0.25 * vsig * mu0_1 * r / (rho_ij * rho_ij);
#endif/*OLD_MAGFAC_SYM*/                      
#endif/*POLYTROPE*/
#endif/*EULER_DISSIPATION*/

#ifdef MAGNETIC_DISSIPATION
#ifndef POLYTROPE
#ifdef OLD_MAGFAC_SYM
                  dtEntropy += dTu_diss_b * 0.25 * vsig * mu0_1 * r / rho;
#else
                  dtEntropy += dTu_diss_b * 0.25 * vsig * mu0_1 * r / (rho_ij * rho_ij);
#endif/*OLD_MAGFAC_SYM*/
#endif /*POLYTROPE*/
#ifndef EULERPOTENTIALS
#ifdef OLD_MAGFAC_SYM
/* old stuff, but units seem to be consistent
		  magfac_sym *= vsig * 0.5 * Balpha_ij * (dBx * dx + dBy * dy + dBz * dz) / r;
		  dtB[0] += magfac_sym * dx;
		  dtB[1] += magfac_sym * dy;
		  dtB[2] += magfac_sym * dz;
*/
		  magfac_sym *= vsig * 0.5 * Balpha_ij * r;
		  dtB[0] += magfac_sym * dBx;
		  dtB[1] += magfac_sym * dBy;
		  dtB[2] += magfac_sym * dBz;
#else
                  magfac_sym *= vsig * 0.5 * Balpha_ij * r * rho / (rho_ij * rho_ij);
		  dtB[0] += magfac_sym * dBx;
		  dtB[1] += magfac_sym * dBy;
		  dtB[2] += magfac_sym * dBz;
#endif/*OLD_MAGFAC_SYM*/
#endif/*EULERPOTENTIALS*/
#endif /* MAGNETIC_DISSIPATION */
#endif /*MAGNETIC*/



		}
	    }
	}
    }
  while(startnode >= 0);

  /* Now collect the result at the right place */
  if(mode == 0)
    {
      for(k = 0; k < 3; k++)
	SphP[target].HydroAccel[k] = acc[k];
#ifndef POLYTROPE
#ifdef CHEMCOOL
      SphP[target].DtEntropyVisc = dtEntropy;
#else
      SphP[target].DtEntropy     = dtEntropy;
#endif
#endif


      SphP[target].MaxSignalVel = maxSignalVel;

#ifdef MAGNETIC
#ifndef EULERPOTENTIALS
      for(k = 0; k < 3; k++)
	SphP[target].DtB[k] = dtB[k];
#endif
#ifdef DIVBCLEANING_DEDNER
      SphP[target].DtPhi = DtPhi;
#endif
#ifdef EULER_DISSIPATION
      SphP[target].DtEulerA = dteulA;
      SphP[target].DtEulerB = dteulB;
#endif
#endif
    }
  else
    {
      for(k = 0; k < 3; k++)
	HydroDataResult[target].Acc[k] = acc[k];
#ifndef POLYTROPE
      HydroDataResult[target].DtEntropy = dtEntropy;
#endif

      HydroDataResult[target].MaxSignalVel = maxSignalVel;
#ifdef MAGNETIC
#ifndef EULERPOTENTIALS
      for(k = 0; k < 3; k++)
	HydroDataResult[target].DtB[k] = dtB[k];
#endif
#ifdef DIVBCLEANING_DEDNER
      HydroDataResult[target].DtPhi = DtPhi;
#endif
#ifdef EULER_DISSIPATION
      HydroDataResult[target].DtEulerA = dteulA;
      HydroDataResult[target].DtEulerB = dteulB;
#endif
#endif
    }
}




/*! This is a comparison kernel for a sort routine, which is used to group
 *  particles that are going to be exported to the same CPU.
 */
int hydro_compare_key(const void *a, const void *b)
{
  if(((struct hydrodata_in *) a)->Task < (((struct hydrodata_in *) b)->Task))
    return -1;
  if(((struct hydrodata_in *) a)->Task > (((struct hydrodata_in *) b)->Task))
    return +1;
  return 0;
}
