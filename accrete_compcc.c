#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <float.h>
#include <mpi.h>

#include "allvars.h"
#include "proto.h"

#ifdef PERIODIC
static double boxSize, boxHalf;

#ifdef LONG_X
static double boxSize_X, boxHalf_X;
#else
#define boxSize_X boxSize
#define boxHalf_X boxHalf
#endif
#ifdef LONG_Y
static double boxSize_Y, boxHalf_Y;
#else
#define boxSize_Y boxSize
#define boxHalf_Y boxHalf
#endif
#ifdef LONG_Z
static double boxSize_Z, boxHalf_Z;
#else
#define boxSize_Z boxSize
#define boxHalf_Z boxHalf
#endif
#endif


/*! these macros maps a coordinate difference to the nearest periodic
 * image
 */

#define NGB_PERIODIC_X(x) (xtmp=(x),(xtmp>boxHalf_X)?(xtmp-boxSize_X):((xtmp<-boxHalf_X)?(xtmp+boxSize_X):xtmp))
#define NGB_PERIODIC_Y(x) (xtmp=(x),(xtmp>boxHalf_Y)?(xtmp-boxSize_Y):((xtmp<-boxHalf_Y)?(xtmp+boxSize_Y):xtmp))
#define NGB_PERIODIC_Z(x) (xtmp=(x),(xtmp>boxHalf_Z)?(xtmp-boxSize_Z):((xtmp<-boxHalf_Z)?(xtmp+boxSize_Z):xtmp))


/* dynamically create the sink particles and accrete gas particles if necessary */

int accrete(void) {
  int n;


  /*  only accrete gas if the inner and outer accretion radius are both > 0 */
  if (All.ROuter > 0)
    {
      if(ThisTask == 0)
	{
	  printf("accretion started\n");
	  fflush(stdout);
	}
      accrete_gas();
    }
  if(ThisTask == 0)
    {
      printf("accretion ended\n");
      fflush(stdout);
    }
  MPI_Barrier(MPI_COMM_WORLD);
  /*  only create sinks if the critical density is defined and > 0 */
  n = 0;
  
  if (All.SinkCriticalDens > 0)
    {
      MPI_Barrier(MPI_COMM_WORLD);
      if(ThisTask == 0)
	{
	  printf("creation started\n");
	  fflush(stdout);
	}
      n = create_sinks();
    }
  if(ThisTask == 0)
    {
      printf("creation ended\n");
      fflush(stdout);
    }
 
  return n;
}


/* dynamically create sink particles */
int create_sinks(void) 
{
  int i, j, NumPart0, NSinks0, TotNumPart0, TotNSinks0;
  int nsinks, nsinks1, k, k1, p, p1, imax;
  int ngbs, *nngbsexport, *noffset;
  int masterTask, overlap, overlapflag, sinkenv, sinkenvsum;
  double redshift, a3inv;
  double SinkCriticalDensity, SinkCriticalH, OuterAccrRadius;
  double InnerAccrRadius, InnerAccrRadius2,OuterAccrRadius2 ;
  double s_a, a3, egyspec;
  double rmax, rmaxtot, mass0, masstot;
  double ethermal, egravity, ekinetic;
  double xyz[3];
  double vel0[3], pos0[3], acc0[3], vel[3], pos[3], acc[3], veltot[3], postot[3], acctot[3];
  double wp, u, r, h, alpha, etot, divv, diva, Alpha, Etot, DivA, Ethermal, Egravity;
  int startnode, numngb, n;
  double dx, dy, dz, r2;
  int level, sendTask, recvTask;
  double tstart, tend;
  int nbuffer, place, nexport;
  int numhbig, numdsmall, numall;
  float out_buf[8];
  int out_buf_0;
  MPI_Status status;
  struct{
    double val;
    int rank;
  } in, out;
#ifdef PERIODIC
  double xtmp;
#endif

  /* If we're running in comvoving coordinates and the current redshift is higher than
   * SinkCriticalRedshift, then don't create sinks. This check is made so that we don't try
   * to create sinks inappropriately at high z simply because the cosmological background 
   * density is greater than SinkCriticalDens.
   */
  if (All.ComovingIntegrationOn) {
    redshift = (1.0 / All.Time) - 1.0;
    if (redshift > All.SinkCriticalRedshift) {
      return 0;
    }
    s_a = All.Hubble * All.HubbleParam * sqrt(All.Omega0 + All.Time * (1.0 - All.Omega0 - All.OmegaLambda) + pow(All.Time, 3) * All.OmegaLambda);
    a3 = All.Time * All.Time * All.Time;
  }
  else
    {
      a3 = 1;
      s_a = 1;
    }

  nngbsexport=malloc(sizeof(int)*NTask);
  noffset=malloc(sizeof(int)*NTask);

  /* kern_len = get_kernel(&knlrad, &knlpot, 1); */

  TotNumPart0 = (int) All.TotNumPart;
  TotNSinks0 = (int) All.TotN_sinks;
  NSinks0 = N_sinks;
  NumPart0 = NumPart;
  nsinks = 0;

  /* We always specify the sink particle creation parameters (SinkCriticalDens, 
   * HSinkCreate, ROuter, etc.) in physical units, so if we're running using 
   * comoving coordinates, we need to compute their values in comoving units.
   */  
  if (All.ComovingIntegrationOn) {
    a3inv = 1 / a3;
    SinkCriticalDensity = All.SinkCriticalDens / a3inv;
    SinkCriticalH       = All.HSinkCreate / All.Time;
    OuterAccrRadius     = All.ROuter / All.Time;
    OuterAccrRadius2    = OuterAccrRadius * OuterAccrRadius;
    InnerAccrRadius     = All.RInner / All.Time;
    InnerAccrRadius2    = InnerAccrRadius * InnerAccrRadius;
  }
  else {
    a3inv = 1;
    SinkCriticalDensity = All.SinkCriticalDens; 
    SinkCriticalH       = All.HSinkCreate;
    OuterAccrRadius     = All.ROuter;
    OuterAccrRadius2     = All.ROuter2;
    InnerAccrRadius     = All.RInner;
    InnerAccrRadius2    = All.RInner2;
  }

  while(1)
    {
      rmax = 0;
      imax = 0;
      overlap = 0;
      sinkenv =0;
      numhbig = 0;
      numdsmall = 0;
      numall = 0;

      for(i = 0; i < N_gas; i++)
	{
	  if (P[i].Type != 0 || P[i].ID < 0) 
	    continue;
	  if(P[i].Ti_endstep == All.Ti_Current)
	    {
	      if(SphP[i].Hsml > SinkCriticalH)
		numhbig++;
	      if(SphP[i].Density < SinkCriticalDensity)
		numdsmall++;

	      numall++;

	      if (SphP[i].Hsml < SinkCriticalH && SphP[i].Density > rmax) 
		{
		  rmax = SphP[i].Density;
		  imax = i;
		}
	    }
	}

      in.val = rmax;
      in.rank = ThisTask;
  
      MPI_Allreduce(&in, &out, 1, MPI_DOUBLE_INT, MPI_MAXLOC, MPI_COMM_WORLD);
 
      rmaxtot = out.val;
      masterTask = out.rank;

      if(rmaxtot <= SinkCriticalDensity)
	break;

      /*nachbarn holen, dabei feststellen, ob Ueberlapp vorliegt*/

      if(ThisTask == masterTask)
	{
	  for(j=0; j<3; j++)
	    xyz[j] = P[imax].Pos[j];
	  /*  printf("%d: overlapflag: %d\n", ThisTask, overlapflag); */
	}

      MPI_Bcast(xyz, 3, MPI_DOUBLE, masterTask, MPI_COMM_WORLD); /* tell all tasks the position of the particle */

      startnode = All.MaxPart;
      numngb = 0;
      do
	{
	  ngbs = ngb_treefind_variable_sink(xyz, 2.0*OuterAccrRadius, &startnode);
	  for(n = 0; n < ngbs; n++)
	    {
	      j = Ngblist[n];

	      dx = xyz[0] - P[j].Pos[0];
	      dy = xyz[1] - P[j].Pos[1];
	      dz = xyz[2] - P[j].Pos[2];

#ifdef PERIODIC			
/*  now find the closest image in the given box size  */
	      dx = NGB_PERIODIC_X(dx);
	      dy = NGB_PERIODIC_Y(dy);
	      dz = NGB_PERIODIC_Z(dz);
#endif
	      r2 = dx * dx + dy * dy + dz * dz;

	      if(r2 < 4.0*OuterAccrRadius2)
		numngb++;      
	    }
	}
      while(startnode >= 0);
      ngbs = numngb;
      if(ngbs > 0)
	sinkenv = 1;
      MPI_Allreduce(&sinkenv, &sinkenvsum, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD); /* is there already a sink close by ? */
      if(ThisTask == masterTask)
	{
	  P[imax].Ti_endstep = -P[imax].Ti_endstep - 1; /*mark as inactive - need to be changed later on*/
	}
      if(sinkenvsum>0)
	continue;
      
      nsinks1 = nsinks; /* initialize nsink1 for sink printout*/

      for(j = 0; j < NTask; j++)
	Exportflag[j] = 0;

      startnode = All.MaxPart;
      numngb = 0;
      do
	{
	  ngbs = ngb_treefind_variable(xyz, OuterAccrRadius, &startnode); /*find gas neighbors*/
	  for(n = 0; n < ngbs; n++)
	    {
	      j = Ngblist[n];

	      dx = xyz[0] - P[j].Pos[0];
	      dy = xyz[1] - P[j].Pos[1];
	      dz = xyz[2] - P[j].Pos[2];

#ifdef PERIODIC			
/*  now find the closest image in the given box size  */
	      dx = NGB_PERIODIC_X(dx);
	      dy = NGB_PERIODIC_Y(dy);
	      dz = NGB_PERIODIC_Z(dz);	      
#endif
	      r2 = dx * dx + dy * dy + dz * dz;

	      if(r2 < OuterAccrRadius2)
		numngb++;      
	    }
	}
      while(startnode >= 0);
      /*ngbs = numngb;*/ 

      if(ThisTask == masterTask)
	{
	  for(j = 0; j < NTask; j++)
	    {
	      overlap += Exportflag[j];
	    }
	}

      MPI_Allreduce(&overlap, &overlapflag, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);

      if(overlapflag) /*there is an overlap*/
	{
	  MPI_Allgather(&numngb, 1, MPI_INT, nngbsexport, 1, MPI_INT, MPI_COMM_WORLD );

	  for(i=nbuffer=0; i<NTask; i++)
	    nbuffer += nngbsexport[i];
     
	  noffset[0]=0;
	  for(i=1; i<NTask; i++)
	    noffset[i]= noffset[i-1] + nngbsexport[i-1];  
 
	  /*fill buffer with neighbour data*/
	  nexport = 0;
	  if(nngbsexport[ThisTask] > 0)
	    {
	      for(n = 0; n < ngbs; n++)
		{
		  p = Ngblist[n];

		  dx = xyz[0] - P[p].Pos[0];
		  dy = xyz[1] - P[p].Pos[1];
		  dz = xyz[2] - P[p].Pos[2];

#ifdef PERIODIC			
/*  now find the closest image in the given box size  */
		  dx = NGB_PERIODIC_X(dx);
		  dy = NGB_PERIODIC_Y(dy);
		  dz = NGB_PERIODIC_Z(dz);
#endif
		  r2 = dx * dx + dy * dy + dz * dz;

		  if(r2 < OuterAccrRadius2)
		    {
		      place = noffset[ThisTask] + nexport;
		      nexport++;

		      for(j=0; j<3; j++)
			{
			  NgbsDataIn[place].Pos[j] = P[p].Pos[j];
			}
		      NgbsDataIn[place].Mass = P[p].Mass;
	  
		    }
		}
	    }
	      
	  /* now start big communication */
  
      
	  tstart=second(); 
 
	  for(level=1;level<NTask;level++)
	    {
	      sendTask = ThisTask;
	      recvTask = ThisTask ^ level;
      
	      MPI_Sendrecv(&NgbsDataIn[noffset[sendTask]], nngbsexport[sendTask]*sizeof(struct ngbsdata_in), MPI_BYTE, recvTask, TAG_ANY,
			   &NgbsDataIn[noffset[recvTask]], nngbsexport[recvTask]*sizeof(struct ngbsdata_in), MPI_BYTE, recvTask, TAG_ANY, MPI_COMM_WORLD, &status);
	    }
    
	  tend=second();
	  All.CPU_CommSum+= timediff(tstart, tend);

	  /*----------------------------------------*/
	  mass0 = 0;
	  alpha = 0;
	  etot = 0;
	  diva = 0;
	  divv = 0;
	  ethermal = 0.0;
	  egravity = 0.0;
	  ekinetic = 0.0;
	   
	  for(j=0; j<3; j++)
	    {
	      pos0[j] = 0.0;
	      vel0[j] = 0.0;
	      acc0[j] = 0.0;
	    }
	 
	  if(numngb>0)
	    {
	      for(k=0; k < ngbs; k++)
		{
		  p = Ngblist[k];

		  dx = xyz[0] - P[p].Pos[0];
		  dy = xyz[1] - P[p].Pos[1];
		  dz = xyz[2] - P[p].Pos[2];

#ifdef PERIODIC			
/*  now find the closest image in the given box size  */
		  dx = NGB_PERIODIC_X(dx);
		  dy = NGB_PERIODIC_Y(dy);
		  dz = NGB_PERIODIC_Z(dz);
#endif
		  r2 = dx * dx + dy * dy + dz * dz;

		  if(r2 < OuterAccrRadius2)
		    {
		  
		      mass0 += P[p].Mass;
		      /*  printf("%d: ngbsID: %d %g %g %g\n",ThisTask,P[p].ID,P[p].Pos[0],P[p].Pos[1],P[p].Pos[2]); */
		      for(j=0; j<3; j++)
			{
			  if (All.ComovingIntegrationOn) {
			    /* Convert from comoving units to physical units */
			    pos0[j] += P[p].Mass*P[p].Pos[j]*All.Time;
			    vel0[j] += P[p].Mass*P[p].Vel[j]*sqrt(All.Time);
			    acc0[j] += P[p].Mass * s_a * (0.5 * P[p].Vel[j] / All.Time +
							  P[p].GravAccel[j]);
#ifdef PMGRID
			    acc0[j] += P[p].Mass * s_a * P[p].GravPM[j];
#endif			     
			    acc0[j] += P[p].Mass * s_a * SphP[p].HydroAccel[j];
			  }
			  else {
			    pos0[j] += P[p].Mass*P[p].Pos[j];
			    vel0[j] += P[p].Mass*P[p].Vel[j];
			    acc0[j] += P[p].Mass*P[p].GravAccel[j];
#ifdef PMGRID
			    acc0[j] += P[p].Mass*P[p].GravPM[j];
#endif			     
			    acc0[j] += P[p].Mass*SphP[p].HydroAccel[j];
			     
			  }
			}
		    }
		}
	    }
	   
	  MPI_Allreduce(&mass0, &masstot, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
	  MPI_Allreduce(pos0, postot, 3, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
	  MPI_Allreduce(vel0, veltot, 3, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
	  MPI_Allreduce(acc0, acctot, 3, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
	  for(j=0; j<3; j++)
	    {
	      postot[j] /= masstot;
	      veltot[j] /= masstot;
	      acctot[j] /= masstot;
	    }

	  if(numngb>0)
	    {
	      place = 0;
	      for(k=0; k < ngbs; k++)
		{
		  p = Ngblist[k];

		  dx = xyz[0] - P[p].Pos[0];
		  dy = xyz[1] - P[p].Pos[1];
		  dz = xyz[2] - P[p].Pos[2];

#ifdef PERIODIC			
/*  now find the closest image in the given box size  */
		  dx = NGB_PERIODIC_X(dx);
		  dy = NGB_PERIODIC_Y(dy);
		  dz = NGB_PERIODIC_Z(dz);
#endif
		  r2 = dx * dx + dy * dy + dz * dz;

		  if(r2 < OuterAccrRadius2)
		    {
#ifdef ISOTHERM_EQS
		      egyspec = SphP[p].Entropy;
#else
#ifdef POLYTROPE
                      egyspec = get_energy(SphP[p].Density) / (SphP[p].Density * a3inv);
#else
#ifdef CHEMCOOL
		      egyspec = SphP[p].Entropy / (SphP[p].Gamma - 1.0) * pow(SphP[p].Density / a3, (SphP[p].Gamma - 1.0));
#else
		      egyspec = SphP[p].Entropy / (GAMMA_MINUS1) * pow(SphP[p].Density / a3, GAMMA_MINUS1);
#endif /* CHEMCOOL */
#endif /* POLYTROPE */
#endif /* ISOTHERM_EQS */
		      ethermal += P[p].Mass*egyspec;
		      for(j=0; j<3; j++)
			{    
			  if (All.ComovingIntegrationOn) {
			    /* Need to use comoving units here, so that the periodic() call works properly */
			    pos[j] = P[p].Pos[j] - postot[j] / All.Time;
#ifdef PERIODIC
			    if(j==0)
			      pos[j] =  NGB_PERIODIC_X(pos[j]);
			    if(j==1)
			      pos[j] = NGB_PERIODIC_Y(pos[j]);
			    if(j==2)
			      pos[j] = NGB_PERIODIC_Z(pos[j]);
#endif
			    /* Convert back to physical */
			    pos[j] *= All.Time;
			    vel[j] = P[p].Vel[j]*sqrt(All.Time) - veltot[j];
			    acc[j] = s_a * (0.5 * P[p].Vel[j] / All.Time + P[p].GravAccel[j]) - acctot[j];
#ifdef PMGRID
			    acc[j] += s_a * P[p].GravPM[j];
#endif			     
			    acc[j] += s_a * SphP[p].HydroAccel[j];
			  }
			  else {
			    pos[j] = P[p].Pos[j] - postot[j];
#ifdef PERIODIC
			    if(j==0)
			      pos[j] = NGB_PERIODIC_X(pos[j]);
			    if(j==1)
			      pos[j] = NGB_PERIODIC_Y(pos[j]);
			    if(j==2)
			      pos[j] = NGB_PERIODIC_Z(pos[j]);
#endif
			    vel[j] = P[p].Vel[j] - veltot[j];
			    acc[j] = P[p].GravAccel[j] - acctot[j];
#ifdef PMGRID
			    acc[j] += P[p].GravPM[j];
#endif			     
			    acc[j] += SphP[p].HydroAccel[j];
			  }
			}
		      u = 0.5*P[p].Mass*(vel[0]*vel[0]+vel[1]*vel[1]+vel[2]*vel[2]);
		      ekinetic += u;
/* #ifdef EXTGRAV */
/* 		      P[p].Potential = 0.0; */
/* 		      fixed_grav_pot(p); */
 /* 		      egravity += P[p].Potential; */
/* #endif  */
		      r = sqrt(pos[0]*pos[0]+pos[1]*pos[1]+pos[2]*pos[2]);
		      divv += P[p].Mass * (vel[0]*pos[0]+vel[1]*pos[1]+vel[2]*pos[2])/r;
		      diva += P[p].Mass * (acc[0]*pos[0]+acc[1]*pos[1]+acc[2]*pos[2])/r;
		      for(k1=0; k1 < noffset[ThisTask]+place; k1++)
			{
			  p1 = k1;
			  for(j=0; j<3; j++)
			    {
			      pos[j] = NgbsDataIn[p1].Pos[j] - P[p].Pos[j];
#ifdef PERIODIC
			      if(j==0)
				pos[j] = NGB_PERIODIC_X(pos[j]);
			      if(j==1)
				pos[j] = NGB_PERIODIC_Y(pos[j]);
			      if(j==2)
				pos[j] = NGB_PERIODIC_Z(pos[j]);
#endif
			      if (All.ComovingIntegrationOn) {
				pos[j] *= All.Time;
			      }
			    }
			  r = sqrt(pos[0]*pos[0]+pos[1]*pos[1]+pos[2]*pos[2]);
			  h = 2.8*All.SofteningTable[0]; /*needs to be adapted for different softening*/
			  u = r/h;
			  if(u>=1)
			    {
			      egravity += -All.G*P[p].Mass*NgbsDataIn[p1].Mass/r;
			    }
			  else
			    {
			      if(u < 0.5)
				wp = -2.8 + u * u * (5.333333333333 + u * u * (6.4 * u - 9.6));
			      else
				wp = -3.2 + 0.066666666667 / u + u * u * (10.666666666667 +
									  u * (-16.0 + u * (9.6 - 2.133333333333 * u)));
			      egravity += All.G*P[p].Mass*NgbsDataIn[p1].Mass*wp/h;
			    }
			}
		      place++;
		    }
		}
	      etot = ethermal + egravity + ekinetic;

	    }
	  /* sum up contributions from all tasks */


	  MPI_Allreduce(&ethermal, &Ethermal, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
	  MPI_Allreduce(&egravity, &Egravity, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
	  MPI_Allreduce(&etot, &Etot, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
	  MPI_Allreduce(&diva, &DivA, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
	   
	  Alpha = -Ethermal/Egravity;
	  if (Alpha < 0.5 && DivA < 0 && Etot < 0)
	    {
	      TotNumPart0++;
	      TotNSinks0++;
	      nsinks++;

	      if(ThisTask == masterTask) /*create sink*/
		{
		  NSinks0++;
		  NumPart0++;
		  if(NumPart0 > All.MaxPart)
		    printf("Allocated memory exceeded by Sinks %d %d\n",NumPart0,All.MaxPart);

		  P[NumPart0-1].ID = - TotNSinks0; 
#ifdef ID_TWO
		  P[NumPart0-1].ID2 = - TotNSinks0;
#endif 
		  P[NumPart0-1].Type = 5;
		  P[NumPart0-1].Mass = masstot;
		  for(j=0; j<3; j++)
		    {   
		      if (All.ComovingIntegrationOn) {
			P[NumPart0-1].Pos[j] = postot[j] / All.Time;
			P[NumPart0-1].Vel[j] = veltot[j] / sqrt(All.Time);
			P[NumPart0-1].GravAccel[j] = acctot[j] / s_a - 0.5 * P[NumPart0-1].Vel[j] / All.Time;
#ifdef PMGRID
			P[NumPart0-1].GravPM[j] = 0.0;
#endif			     
		      }
		      else {
			P[NumPart0-1].Pos[j] = postot[j];
			P[NumPart0-1].Vel[j] = veltot[j];
			P[NumPart0-1].GravAccel[j] = acctot[j];
#ifdef PMGRID
			P[NumPart0-1].GravPM[j] = 0.0;
#endif				 
		      }
		    }
		  P[NumPart0-1].Ti_endstep = -P[imax].Ti_endstep-1;
		  P[NumPart0-1].Ti_begstep = P[imax].Ti_begstep;
		  P[NumPart0-1].OldAcc = 0.0;

		  /* sink file */
		  out_buf_0 = P[NumPart0-1].Ti_endstep;
		  out_buf[1] = masstot;
		  out_buf[2] = postot[0];
		  out_buf[3] = postot[1];
		  out_buf[4] = postot[2];
		  out_buf[5] = veltot[0];
		  out_buf[6] = veltot[1];
		  out_buf[7] = veltot[2];

		  if (masterTask != 0) 
		    {
		      MPI_Send(&out_buf_0, 1, MPI_INT, 0, 1, MPI_COMM_WORLD);
		    }
		}
	      if (ThisTask == 0) {
		if (masterTask != 0) 
		  {   
		    MPI_Recv(&out_buf_0, 1, MPI_INT, masterTask, 1, MPI_COMM_WORLD, &status);
		  }
	      }
	      if (ThisTask == masterTask) {
	       if (masterTask != 0) 
		    {
		      MPI_Send(out_buf, 8, MPI_FLOAT, 0, 1, MPI_COMM_WORLD);
		    }
	      }
	      if (ThisTask == 0) {
		if (masterTask != 0) 
		  {    
		    MPI_Recv(out_buf, 8, MPI_FLOAT, masterTask, 1, MPI_COMM_WORLD, &status);
		  }
		fprintf(FdSink, "NEWSINK(1):%-2d %4d %4d %10.3E %10.3E %10.3E %10.3E %10.3E %10.3E %10.3E\n", masterTask, TotNSinks0, out_buf_0, out_buf[1], out_buf[2], out_buf[3], out_buf[4], out_buf[5], out_buf[6], out_buf[7]);
		fflush(FdSink);
	      }

	      if(numngb > 0)
		for (k = 0; k < ngbs; k++)
		  {
		    p = Ngblist[k];

		    dx = xyz[0] - P[p].Pos[0];
		    dy = xyz[1] - P[p].Pos[1];
		    dz = xyz[2] - P[p].Pos[2];

#ifdef PERIODIC			/*  now find the closest image in the given box size  */
		    dx = NGB_PERIODIC_X(dx);
		    dy = NGB_PERIODIC_Y(dy);
		    dz = NGB_PERIODIC_Z(dz);
#endif
		    r2 = dx * dx + dy * dy + dz * dz;

		    if(r2 < OuterAccrRadius2)
		      {     
			P[p].ID = - TotNSinks0;
		      }
		  }
	       
	    }
	  /* flag that the particles have been examined for sink creation by negating the Force Flag */
	  if(numngb > 0)
	    for(k=0; k < ngbs; k++)
	      {
		p = Ngblist[k];

		dx = xyz[0] - P[p].Pos[0];
		dy = xyz[1] - P[p].Pos[1];
		dz = xyz[2] - P[p].Pos[2];

#ifdef PERIODIC			/*  now find the closest image in the given box size  */
		dx =  NGB_PERIODIC_X(dx);
		dy = NGB_PERIODIC_Y(dy);
		dz = NGB_PERIODIC_Z(dz);
#endif
		r2 = dx * dx + dy * dy + dz * dz;

		if(r2 < OuterAccrRadius2)
		  {
		    if(P[p].Ti_endstep > 0) 
		      P[p].Ti_endstep = -P[p].Ti_endstep - 1;
		  }
	      }
	}
      else /* no overlap */
	{
	  if(ThisTask == masterTask)
	    {
	      mass0 = 0;
	      for(j=0; j<3; j++)
		{
		  pos0[j] = 0.0;
		  vel0[j] = 0.0;
		  acc0[j] = 0.0;
		}
	      for(k=0; k < ngbs; k++)
		{
		  p = Ngblist[k];

		  dx = xyz[0] - P[p].Pos[0];
		  dy = xyz[1] - P[p].Pos[1];
		  dz = xyz[2] - P[p].Pos[2];

#ifdef PERIODIC			/*  now find the closest image in the given box size  */
		  dx = NGB_PERIODIC_X(dx);
		  dy = NGB_PERIODIC_Y(dy);
		  dz = NGB_PERIODIC_Z(dz);
#endif
		  r2 = dx * dx + dy * dy + dz * dz;

		  if(r2 < OuterAccrRadius2)
		    {
		      mass0 += P[p].Mass;
		      for(j=0; j<3; j++)
			{
			  if (All.ComovingIntegrationOn) {
			    /* Convert from comoving units to physical units */
			    pos0[j] += P[p].Mass*P[p].Pos[j]*All.Time;
			    vel0[j] += P[p].Mass*P[p].Vel[j]*sqrt(All.Time);
			    acc0[j] += P[p].Mass * s_a * (0.5 * P[p].Vel[j] / All.Time +
							  P[p].GravAccel[j]);
#ifdef PMGRID
			    acc0[j] += P[p].Mass * s_a * P[p].GravPM[j];
#endif			     
			    acc0[j] += P[p].Mass * s_a * SphP[p].HydroAccel[j];
			  }
			  else {
			    pos0[j] += P[p].Mass*P[p].Pos[j];
			    vel0[j] += P[p].Mass*P[p].Vel[j];
			    acc0[j] += P[p].Mass*P[p].GravAccel[j];
#ifdef PMGRID
			    acc0[j] += P[p].Mass*P[p].GravPM[j];
#endif			     
			    acc0[j] += P[p].Mass*SphP[p].HydroAccel[j];
			     
			  }
			}
			   
		    }
		}
	   
	      for(j=0; j<3; j++)
		{
		  pos0[j] /= mass0;
		  vel0[j] /= mass0;
		  acc0[j] /= mass0;
		}
	   
	      divv = 0;
	      diva = 0;
	      ethermal = 0.0;
	      egravity = 0.0;
	      ekinetic = 0.0;
	   
	      for(k=0; k < ngbs; k++)
		{
		  p = Ngblist[k];

		  dx = xyz[0] - P[p].Pos[0];
		  dy = xyz[1] - P[p].Pos[1];
		  dz = xyz[2] - P[p].Pos[2];

#ifdef PERIODIC			/*  now find the closest image in the given box size  */
		  dx = NGB_PERIODIC_X(dx);
		  dy = NGB_PERIODIC_Y(dy);
		  dz = NGB_PERIODIC_Z(dz);
#endif
		  r2 = dx * dx + dy * dy + dz * dz;

		  if(r2 < OuterAccrRadius2)
		    {
#ifdef ISOTHERM_EQS
		      egyspec = SphP[p].Entropy;
#else
#ifdef POLYTROPE
                      egyspec = get_energy(SphP[p].Density) / (SphP[p].Density * a3inv);
#else
#ifdef CHEMCOOL
		      egyspec = SphP[p].Entropy / (SphP[p].Gamma - 1.0) * pow(SphP[p].Density / a3, (SphP[p].Gamma - 1.0));
#else
		      egyspec = SphP[p].Entropy / (GAMMA_MINUS1) * pow(SphP[p].Density / a3, GAMMA_MINUS1);
#endif /* CHEMCOOL */
#endif /* POLYTROPE */
#endif /* ISOTHERM_EQS */
		      ethermal += P[p].Mass*egyspec;
		      for(j=0; j<3; j++)
			{
			  if (All.ComovingIntegrationOn) {
			    pos[j] = P[p].Pos[j] - pos0[j] / All.Time;
#ifdef PERIODIC
			    if(j==0)
			      pos[j] = NGB_PERIODIC_X(pos[j]);
			    if(j==1)
			      pos[j] = NGB_PERIODIC_Y(pos[j]);
			    if(j==2)
			      pos[j] = NGB_PERIODIC_Z(pos[j]);
#endif
			    /* Convert back to physical */
			    pos[j] *= All.Time;
			    vel[j] = P[p].Vel[j]*sqrt(All.Time) - vel0[j];
			    acc[j] = s_a * (0.5 * P[p].Vel[j] / All.Time + P[p].GravAccel[j]) - acc0[j];
#ifdef PMGRID
			    acc[j] += s_a * P[p].GravPM[j];
#endif			     
			    acc[j] += s_a * SphP[p].HydroAccel[j];
			  }
			  else {
			    pos[j] = P[p].Pos[j] - postot[j];
#ifdef PERIODIC
			    if(j==0)
			      pos[j] =  NGB_PERIODIC_X(pos[j]);
			    if(j==1)
			      pos[j] = NGB_PERIODIC_Y(pos[j]);
			    if(j==2)
			      pos[j] = NGB_PERIODIC_Z(pos[j]);
#endif
			    vel[j] = P[p].Vel[j] - vel0[j];
			    acc[j] = P[p].GravAccel[j] - acc0[j];
#ifdef PMGRID
			    acc[j] += P[p].GravPM[j];
#endif			     
			    acc[j] += SphP[p].HydroAccel[j];
			  }
			}
		      u = 0.5*P[p].Mass*(vel[0]*vel[0]+vel[1]*vel[1]+vel[2]*vel[2]);
		      ekinetic += u;
#ifdef EXTGRAV
		      P[p].Potential = 0.0;
		      fixed_grav_pot(p);
		      egravity += P[p].Potential;
#endif
		      r = sqrt(pos[0]*pos[0]+pos[1]*pos[1]+pos[2]*pos[2]);
		      divv += P[p].Mass * (vel[0]*pos[0]+vel[1]*pos[1]+vel[2]*pos[2])/r;
		      diva += P[p].Mass * (acc[0]*pos[0]+acc[1]*pos[1]+acc[2]*pos[2])/r;
		      for(k1=0; k1 < k; k1++)
			{
			  p1 = Ngblist[k1];

			  dx = xyz[0] - P[p1].Pos[0];
			  dy = xyz[1] - P[p1].Pos[1];
			  dz = xyz[2] - P[p1].Pos[2];

#ifdef PERIODIC			/*  now find the closest image in the given box size  */
			  dx = NGB_PERIODIC_X(dx);
			  dy = NGB_PERIODIC_Y(dy);
			  dz = NGB_PERIODIC_Z(dz);
#endif
			  r2 = dx * dx + dy * dy + dz * dz;

			  if(r2 < OuterAccrRadius2)
			    {
			      for(j=0; j<3; j++)
				{
				  pos[j] = P[p1].Pos[j] - P[p].Pos[j];
#ifdef PERIODIC
				  if(j==0)
				    pos[j] =  NGB_PERIODIC_X(pos[j]);
				  if(j==1)
				    pos[j] = NGB_PERIODIC_Y(pos[j]);
				  if(j==2)
				    pos[j] = NGB_PERIODIC_Z(pos[j]);
#endif
				  if (All.ComovingIntegrationOn) {
				    pos[j] *= All.Time;
				  }
				}
			      r = sqrt(pos[0]*pos[0]+pos[1]*pos[1]+pos[2]*pos[2]);
			      h = 2.8*All.SofteningTable[0]; /*needs to be adapted for different softening*/
			      u = r/h;
			      if(u>=1)
				egravity += -All.G*P[p].Mass*P[p1].Mass/r;
			      else
				{
				  if(u < 0.5)
				    wp = -2.8 + u * u * (5.333333333333 + u * u * (6.4 * u - 9.6));
				  else
				    wp = -3.2 + 0.066666666667 / u + u * u * (10.666666666667 +
									      u * (-16.0 + u * (9.6 - 2.133333333333 * u)));
				  egravity += All.G*P[p].Mass*P[p1].Mass*wp/h;
				}
			       
			    }
			}
		    }
		}
	      alpha = -ethermal/egravity;
	      etot = ethermal + egravity + ekinetic;

	      Alpha = alpha;
	      Etot = etot;
	      DivA = diva;
	      masstot = mass0;

	      for(j=0; j<3; j++)
		{
		  postot[j] = pos0[j];
		  veltot[j] = vel0[j];
		  acctot[j] = acc0[j];
		}
	      if (Alpha < 0.5 && DivA < 0 && Etot < 0)
		{
		  TotNumPart0++;
		  TotNSinks0++;
		  nsinks++;

		  if(ThisTask == masterTask) /*create sink*/
		    {
		      NSinks0++;
		      NumPart0++;
		      if(NumPart0 > All.MaxPart)
			printf("Allocated memory exceeded by Sinks %d %d\n",NumPart0,All.MaxPart);

		      P[NumPart0-1].ID = - TotNSinks0; 
#ifdef ID_TWO
		      P[NumPart0-1].ID2 = - TotNSinks0;
#endif 
		      P[NumPart0-1].Type = 5;
		      P[NumPart0-1].Mass = masstot;
		      for(j=0; j<3; j++)
			{   
			  if (All.ComovingIntegrationOn) {
			    P[NumPart0-1].Pos[j] = postot[j] / All.Time;
			    P[NumPart0-1].Vel[j] = veltot[j] / sqrt(All.Time);
			    P[NumPart0-1].GravAccel[j] = acctot[j] / s_a - 0.5 * P[NumPart0-1].Vel[j] / All.Time;
#ifdef PMGRID
			    P[NumPart0-1].GravPM[j] = 0.0;
#endif			     
			  }
			  else {
			    P[NumPart0-1].Pos[j] = postot[j];
			    P[NumPart0-1].Vel[j] = veltot[j];
			    P[NumPart0-1].GravAccel[j] = acctot[j];
#ifdef PMGRID
			    P[NumPart0-1].GravPM[j] = 0.0;
#endif				 
			  }
			}
		      /* XXX: IGM - sink metallcity here */

		      P[NumPart0-1].Ti_endstep = -P[imax].Ti_endstep-1;
		      P[NumPart0-1].Ti_begstep = P[imax].Ti_begstep;
		      P[NumPart0-1].OldAcc = 0.0;

		      /* sink file */
		      out_buf_0 = P[NumPart0-1].Ti_endstep;
		      out_buf[1] = masstot;
		      out_buf[2] = postot[0];
		      out_buf[3] = postot[1];
		      out_buf[4] = postot[2];
		      out_buf[5] = veltot[0];
		      out_buf[6] = veltot[1];
		      out_buf[7] = veltot[2];

		      if (masterTask != 0) {
			MPI_Send(out_buf, 8, MPI_FLOAT, 0, 1, MPI_COMM_WORLD);
			MPI_Send(&out_buf_0, 1, MPI_INT, 0, 1, MPI_COMM_WORLD);
		      }
		      /*  printf("*****NEWSINK(2):%d %d %d %g %g %g %g %g %g %g %g %g %g %g %g %g %g %d\n",ThisTask,P[NumPart0].ID,P[NumPart0].Type,P[NumPart0].Mass,P[NumPart0].Pos[0],P[NumPart0].Pos[1],P[NumPart0].Pos[2],P[NumPart0].Vel[0],P[NumPart0].Vel[1],P[NumPart0].Vel[2],P[NumPart0].Accel[0],P[NumPart0].Accel[1],P[NumPart0].Accel[2],P[NumPart0].CurrentTime ,Alpha,DivA,Etot,imax);  */ 
		   
		      for (k = 0; k < ngbs; k++)
			{
			  p = Ngblist[k];

			  dx = xyz[0] - P[p].Pos[0];
			  dy = xyz[1] - P[p].Pos[1];
			  dz = xyz[2] - P[p].Pos[2];

#ifdef PERIODIC			/*  now find the closest image in the given box size  */
			  dx = NGB_PERIODIC_X(dx);
			  dy = NGB_PERIODIC_Y(dy);
			  dz = NGB_PERIODIC_Z(dz);
#endif
			  r2 = dx * dx + dy * dy + dz * dz;

			  if(r2 < OuterAccrRadius2)
			    {		       
			      P[p].ID = - TotNSinks0;
			    }
			}
	       
		    }
		  for(k=0; k < ngbs; k++)
		    {
		      p = Ngblist[k];

		      dx = xyz[0] - P[p].Pos[0];
		      dy = xyz[1] - P[p].Pos[1];
		      dz = xyz[2] - P[p].Pos[2];

#ifdef PERIODIC			/*  now find the closest image in the given box size  */
		      dx = NGB_PERIODIC_X(dx);
		      dy = NGB_PERIODIC_Y(dy);
		      dz = NGB_PERIODIC_Z(dz);
#endif
		      r2 = dx * dx + dy * dy + dz * dz;

		      if(r2 < OuterAccrRadius2)
			{
			  if(P[p].Ti_endstep > 0)
			    P[p].Ti_endstep = -P[p].Ti_endstep - 1;
			}
		    }	       
		}
	    }
	  /* tell the other tasks that a sink has been created*/
	  MPI_Bcast(&TotNumPart0, 1, MPI_INT, masterTask, MPI_COMM_WORLD);
	  MPI_Bcast(&TotNSinks0, 1, MPI_INT, masterTask, MPI_COMM_WORLD);
	  MPI_Bcast(&nsinks, 1, MPI_INT, masterTask, MPI_COMM_WORLD);
	  /* sink file */
	  if (nsinks > nsinks1 && ThisTask == 0) {
	    if (masterTask != 0) {
	      MPI_Recv(out_buf, 8, MPI_FLOAT, masterTask, 1, MPI_COMM_WORLD, &status);
	      MPI_Recv(&out_buf_0, 1, MPI_INT, masterTask, 1, MPI_COMM_WORLD, &status);
	    }
	    fprintf(FdSink, "NEWSINK(2):%-2d %4d %4d %10.3E %10.3E %10.3E %10.3E %10.3E %10.3E %10.3E\n", masterTask, TotNSinks0, out_buf_0, out_buf[1], out_buf[2], out_buf[3], out_buf[4], out_buf[5], out_buf[6], out_buf[7]);

	    fflush(FdSink);
	  }       
	}
    }
  All.TotNumPart = (long long)TotNumPart0;
  All.TotN_sinks = (long long)TotNSinks0;
  N_sinks = NSinks0;
  NumPart = NumPart0;
  
  for(i = 0; i < NumPart; i++)
    {
      if(P[i].Ti_endstep < 0)
	P[i].Ti_endstep = -P[i].Ti_endstep - 1;
    }

  if (nsinks > 0) 
    {
      printf("** %d :%d SINKS Created %d %d %d %d\n", ThisTask, nsinks, NumPart, N_sinks, TotNumPart0, TotNSinks0);
      fflush(stdout);
    }

  return nsinks;
}


void accrete_gas(void)
{
   int i, j, k, p, h;
    int nbuffer, place, nexport, ngbs;
    int *nsinkexport, *noffset;
    float vel[3];
    double e0, e1, mu, dx, dy, dz, r2;
    double OuterAccrRadius, InnerAccrRadius, InnerAccrRadius2, OuterAccrRadius2;
    int level, sendTask, recvTask;
    int startnode, n;
    double tstart, tend;
#ifdef PERIODIC
  double xtmp;
#endif
    MPI_Status status;
    
   if (All.ComovingIntegrationOn) {
    OuterAccrRadius     = All.ROuter / All.Time;
    OuterAccrRadius2    = OuterAccrRadius * OuterAccrRadius;
    InnerAccrRadius     = All.RInner / All.Time;
    InnerAccrRadius2    = InnerAccrRadius * InnerAccrRadius;
  }
  else {
    OuterAccrRadius     = All.ROuter;
    OuterAccrRadius2    = All.ROuter2;
    InnerAccrRadius     = All.RInner;
    InnerAccrRadius2    = All.RInner2;
  }
    
    nsinkexport= malloc(sizeof(int)*NTask);
    noffset=malloc(sizeof(int)*NTask);
    MPI_Allgather(&N_sinks, 1, MPI_INT, nsinkexport, 1, MPI_INT, MPI_COMM_WORLD );

    for(i=nbuffer=0; i<NTask; i++)
      nbuffer += nsinkexport[i];
     
    if(nbuffer == 0)
      return;
    noffset[0]=0;
    for(i=1; i<NTask; i++)
      noffset[i]= noffset[i-1] + nsinkexport[i-1];

    /*fill buffer with sink data*/
    nexport = 0;
    if(nsinkexport[ThisTask] > 0)
      {
        for(i=N_gas; i<NumPart; i++)
	  {
	    if(P[i].Type == 5)
	      {
		place = noffset[ThisTask] + nexport;
		nexport++;

		for(j=0; j<3; j++)
		  {
		    SinkDataIn[place].Pos[j] = P[i].Pos[j];
		    SinkDataIn[place].Vel[j] = P[i].Vel[j];
		    SinkDataIn[place].GravAccel[j] = P[i].GravAccel[j];
#ifdef PMGRID
		     SinkDataIn[place].GravPM[j] = P[i].GravPM[j];
#endif		    
		  }
		SinkDataIn[place].Mass = P[i].Mass;
		SinkDataIn[place].ID = P[i].ID;
	  
	      }
	  }
      }
 
    /* now start big communication */
  
    tstart=second();
 
    for(level=1;level<NTask;level++)
      {
        sendTask = ThisTask;
        recvTask = ThisTask ^ level;
      
        MPI_Sendrecv(&SinkDataIn[noffset[sendTask]], nsinkexport[sendTask]*sizeof(struct sinkdata_in), MPI_BYTE, recvTask, TAG_ANY,
		     &SinkDataIn[noffset[recvTask]], nsinkexport[recvTask]*sizeof(struct sinkdata_in), MPI_BYTE, recvTask, TAG_ANY, MPI_COMM_WORLD, &status);
      }
    
    tend=second();
    All.CPU_CommSum+= timediff(tstart, tend);
  
    for(i=0; i<nbuffer;i++) /*tasks work in parallel on sinks*/
      {
        for(j=0; j<3; j++)
	  {
	    SinkDataResult[i].Pos[j] =0;
	    SinkDataResult[i].Vel[j] =0;
	    SinkDataResult[i].GravAccel[j] =0;
#ifdef PMGRID
	    SinkDataResult[i].GravPM[j] = 0;
#endif
	  }
        SinkDataResult[i].Mass =0;
      }

  for(i=0; i<nbuffer;i++) /*tasks work in parallel on sinks*/
      {
        startnode = All.MaxPart;

	do
	{
	  ngbs = ngb_treefind_variable(SinkDataIn[i].Pos, OuterAccrRadius, &startnode); /*find gas neighbors*/
	  for(n = 0; n < ngbs; n++)
	    {
	      p = Ngblist[n];

	      dx = SinkDataIn[i].Pos[0] - P[p].Pos[0];
	      dy = SinkDataIn[i].Pos[1] - P[p].Pos[1];
	      dz = SinkDataIn[i].Pos[2] - P[p].Pos[2];

#ifdef PERIODIC			/*  now find the closest image in the given box size  */
	      dx = NGB_PERIODIC_X(dx);
	      dy = NGB_PERIODIC_Y(dy);
	      dz = NGB_PERIODIC_Z(dz);	      
#endif
	      r2 = dx * dx + dy * dy + dz * dz;

	      if(r2 < OuterAccrRadius2)
		{
		  if (P[p].Ti_endstep != All.Ti_Current) /* do not accrete particles not on the current time step */
		    continue;
		  if (P[p].ID < 0) /* avoid particles that have been accreted */
		    continue;
		  if(r2 < InnerAccrRadius2)
		    add_gas_to_sink(p, i);  /* particles inside inner accretion radius */
		  else
		    { 
		      /* check if the particle is bound. */
		      if (All.ComovingIntegrationOn) {
			for(j=0; j<3; j++)
			  vel[j] = (P[p].Vel[j] - SinkDataIn[i].Vel[j]) * sqrt(All.Time);
		      }
		      else {
			for(j=0; j<3; j++)
			  vel[j] = P[p].Vel[j] - SinkDataIn[i].Vel[j];
		      }
		        mu = (P[p].Mass * SinkDataIn[i].Mass)/(P[p].Mass + SinkDataIn[i].Mass);
			e0 = 0.5*mu*(vel[0]*vel[0] + vel[1]*vel[1] + vel[2]*vel[2]);
			if (All.ComovingIntegrationOn) {
			  e0 -= All.G*P[p].Mass*SinkDataIn[i].Mass/sqrt(r2*All.Time*All.Time);
			}
			else {
			  e0 -= All.G*P[p].Mass*SinkDataIn[i].Mass/sqrt(r2);
			}
			if (e0 >= 0) continue;
			/* check if the particle is more tightly bound to another sink */
			e1 = e0+1.0;
			for (h = 0; h < nbuffer; h++)
			  {
			    if (h == i)
			      continue;
			    if (All.ComovingIntegrationOn) {
			      for(j=0; j<3; j++)
				vel[j] = (P[p].Vel[j] - SinkDataIn[h].Vel[j]) * sqrt(All.Time);
			    }
			    else {
			      for(j=0; j<3; j++)
				vel[j] = P[p].Vel[j] - SinkDataIn[h].Vel[j];
			    }
			    mu = (P[p].Mass * SinkDataIn[h].Mass)/(P[p].Mass + SinkDataIn[h].Mass);
			    e1 = 0.5*mu*(vel[0]*vel[0] + vel[1]*vel[1] + vel[2]*vel[2]);
			    dx = P[p].Pos[0] - SinkDataIn[h].Pos[0];
			    dy = P[p].Pos[1] - SinkDataIn[h].Pos[1];
			    dz = P[p].Pos[2] - SinkDataIn[h].Pos[2];
#ifdef PERIODIC			/*  now find the closest image in the given box size  */
			    dx = NGB_PERIODIC_X(dx);
			    dy = NGB_PERIODIC_Y(dy);
			    dz = NGB_PERIODIC_Z(dz);
#endif
			    if (All.ComovingIntegrationOn) {
			      dx *= All.Time;
			      dy *= All.Time;
			      dz *= All.Time;
			    }
			    e1 -= All.G*P[p].Mass*SinkDataIn[h].Mass/sqrt(dx*dx+dy*dy+dz*dz);
			    if (e1 < e0) break;
			  }
			if (e1 >= e0)
			  add_gas_to_sink(p, i);
		    }
		}
	    }
	} while(startnode >= 0);
      }
	      
   tstart=second();
   for(level=1; level<NTask; level++)
     {
       sendTask = ThisTask;
       recvTask = ThisTask ^ level;
	
       MPI_Sendrecv(&SinkDataResult[noffset[recvTask]], nsinkexport[recvTask]*sizeof(struct sinkdata_out), MPI_BYTE, recvTask, TAG_ANY,
  		  &SinkDataPartialResult[0], nsinkexport[sendTask]*sizeof(struct sinkdata_out), MPI_BYTE, recvTask, TAG_ANY, MPI_COMM_WORLD, &status);

       for(i=0; i<nsinkexport[ThisTask]; i++)
         {
  	 SinkDataResult[noffset[ThisTask] + i].Mass += SinkDataPartialResult[i].Mass;
  	 /* summation of the necessary values*/

  	 for(k=0;k<3;k++)
  	   {
  	     SinkDataResult[noffset[ThisTask] + i].Pos[k] += SinkDataPartialResult[i].Pos[k];
  	     SinkDataResult[noffset[ThisTask] + i].Vel[k] += SinkDataPartialResult[i].Vel[k];
  	     SinkDataResult[noffset[ThisTask] + i].GravAccel[k] += SinkDataPartialResult[i].GravAccel[k];
#ifdef PMGRID
	     SinkDataResult[noffset[ThisTask] + i].GravPM[k] += SinkDataPartialResult[i].GravPM[k];
#endif
  	   }
         }
     }
   tend=second();
   All.CPU_CommSum+= timediff(tstart,tend);

    if(nsinkexport[ThisTask] > 0)
      {
        for(i=N_gas, nexport=0; i<NumPart; i++) /* new sink data reported back to hometask */
  	{
  	  if(P[i].Type == 5)
  	    {
  	      place = noffset[ThisTask] + nexport;
  	      nexport++;

  	      for(j=0; j<3; j++)
  		{
  		  P[i].Pos[j] = SinkDataResult[place].Pos[j]+SinkDataIn[place].Pos[j];
  		  P[i].Vel[j] = SinkDataResult[place].Vel[j]+SinkDataIn[place].Vel[j];
  		  P[i].GravAccel[j] = SinkDataResult[place].GravAccel[j]+SinkDataIn[place].GravAccel[j];
#ifdef PMGRID
		  P[i].GravPM[j] = SinkDataResult[place].GravPM[j]+SinkDataIn[place].GravPM[j];
#endif
  		}
  	      P[i].Mass = SinkDataResult[place].Mass+SinkDataIn[place].Mass;
  	      printf("SinkData:%d %g %g %g %g %d\n",ThisTask,P[i].Vel[0],P[i].Vel[1],P[i].Vel[2],P[i].Mass,P[i].ID); 
  	    }
  	}
      }
   free(nsinkexport);
   free(noffset);
}


/* add an accreted gas particle p to the sink i, and remove p from the
   sph particle list */
void add_gas_to_sink(int p, int i)
{
    double ms, mg, r;
    int j, z;

	printf("accreted:%d %g %g %g %g %d %d\n",ThisTask,P[p].Vel[0],P[p].Vel[1],P[p].Vel[2],P[p].Mass,P[p].ID,P[p].Type);

    z = P[p].ID;
    P[p].ID = SinkDataIn[i].ID;
    ms = SinkDataIn[i].Mass;
    mg = P[p].Mass;
    r = mg/(ms + mg);
 
    for(j=0; j<3; j++)
      {
        SinkDataResult[i].Pos[j] += -r*(SinkDataIn[i].Pos[j] - P[p].Pos[j]);
        SinkDataResult[i].Vel[j] += -r*(SinkDataIn[i].Vel[j] - P[p].Vel[j]);
        SinkDataResult[i].GravAccel[j] += -r*(SinkDataIn[i].GravAccel[j] - P[p].GravAccel[j]);
#ifdef PMGRID
	SinkDataResult[i].GravPM[j] += -r*(SinkDataIn[i].GravPM[j] - P[p].GravPM[j]);
#endif
      }

    SinkDataResult[i].Mass += mg;
}

