#ifdef CHEMCOOL

#include <stdio.h>
#include <math.h>
#include "allvars.h"
#include "proto.h"

double compute_stellar_radius(double mass, double mdot) 
{
  double radius;
  /* This is from Hosokawa & Omukai 2008, who cite 
   * Stahler, Palla & Salpeter 1986 as the originators. 
   * Strictly, it's valid only for a constant accretion 
   * rate, and only at early times (i.e. M_star <~ 10 M_sun), 
   * but it should suffice for testing purposes
   */
  mass /= SOLAR_MASS;
  mdot /= SOLAR_MASS;
  mdot *= 86400 * 365.25; /* Convert from s^-1 -> yr^-1 */
  mdot /= 1e-2;

  radius = 26. * pow(mass, 0.27) * pow(mdot, 0.41);  /* In solar radii */
  radius *= SOLAR_RADIUS;

  /*  if (radius==0.){
     printf("Radius=0\n in compute radius");
     printf("radius %g, mass %g, mdot %g, solar radius %g, solar mass %g \n", radius,mass,mdot,SOLAR_RADIUS,SOLAR_MASS);
     fflush(stdout);
     }*/


  return radius;
}

double compute_luminosity(double mass, double mdot, double radius)
{
  double luminosity;
  luminosity = GRAVITY * mass * mdot / radius;

  /* if (radius==0.){
     printf("Radius=0 IN COMPUTE L\n");
     printf("radius %g, mass %g, mdot %g \n", radius,mass,mdot);
     fflush(stdout);
     }*/


  return luminosity;
}

#endif
