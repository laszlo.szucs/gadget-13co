/*! \file proto.h
 *  \brief this file contains all function prototypes of the code
 */

#ifndef ALLVARS_H
#include "allvars.h"
#endif

#ifdef HAVE_HDF5
#include <hdf5.h>
#endif

#ifdef TURBULENCE
#include "turbulence.h" /*akj*/ 
#endif 



void   advance_and_find_timesteps(void);
void   allocate_commbuffers(void);
void   allocate_memory(void);
void   begrun(void);
int    blockpresent(enum iofields blocknr);
int    block_not_needed_for_startup(enum iofields blocknr);

#ifdef MAGNETIC
#ifdef BSMOOTH
void bsmooth(void);
void bsmooth_evaluate(int i, int mode);
#endif
#ifdef BFROMROTA
void rot_a(void);
void rot_a_evaluate(int i, int mode);
#endif
#endif

void   catch_abort(int sig);
void   catch_fatal(int sig);
void   check_omega(void);
void   close_outputfiles(void);
int    compare_key(const void *a, const void *b);
void   compute_accelerations(int mode);
void   compute_global_quantities_of_system(void);
void   compute_potential(void);
int    dens_compare_key(const void *a, const void *b);
void   density(void);

#if defined(MAGNETIC) && defined(BSMOOTH)
void bsmooth(void);
#endif

void   density_decouple(void);
#ifdef IGM
void   density_evaluate(int i, int mode, int metal_disperse);
#else
void   density_evaluate(int i, int mode);
#endif

void   distribute_file(int nfiles, int firstfile, int firsttask, int lasttask, int *filenr, int *master, int *last);
double dmax(double, double);
double dmin(double, double);
#ifdef POLYTROPE
void define_EOS_table(int);
#endif
void   do_box_wrapping(void);

void   domain_Decomposition(void); 
int    domain_compare_key(const void *a, const void *b);
int    domain_compare_key(const void *a, const void *b);
int    domain_compare_toplist(const void *a, const void *b);
void   domain_countToGo(void);
void   domain_decompose(void);
void   domain_determineTopTree(void);
void   domain_exchangeParticles(int partner, int sphflag, int send_count, int recv_count);
void   domain_findExchangeNumbers(int task, int partner, int sphflag, int *send, int *recv);
void   domain_findExtent(void);
int    domain_findSplit(int cpustart, int ncpu, int first, int last);
void   domain_shiftSplit(void);
void   domain_sumCost(void);
void   domain_topsplit(int node, peanokey startkey);
void   domain_topsplit_local(int node, peanokey startkey);

double drift_integ(double a, void *param);
void   dump_particles(void);
void   empty_read_buffer(enum iofields blocknr, int offset, int pc, int type);
void   endrun(int);
void   energy_statistics(void);
void   every_timestep_stuff(void);

void   ewald_corr(double dx, double dy, double dz, double *fper);
void   ewald_force(int ii, int jj, int kk, double x[3], double force[3]);
void   ewald_init(void);
double ewald_pot_corr(double dx, double dy, double dz);
double ewald_psi(double x[3]);

//void   fill_Tab_IO_Labels(void);
void   fill_write_buffer(enum iofields blocknr, int *pindex, int pc, int type);
void   find_dt_displacement_constraint(double hfac);
int    find_files(char *fname);
int    find_next_outputtime(int time);
void   find_next_sync_point_and_drift(void);

void   force_create_empty_nodes(int no, int topnode, int bits, int x, int y, int z, int *nodecount, int *nextfree);
void   force_exchange_pseudodata(void);
void   force_flag_localnodes(void);
void   force_insert_pseudo_particles(void);
void   force_setupnonrecursive(int no);
void   force_treeallocate(int maxnodes, int maxpart); 
int    force_treebuild(int npart);
int    force_treebuild_single(int npart);
int    force_treeevaluate(int target, int mode, double *ewaldcountsum);
int    force_treeevaluate_direct(int target, int mode);
int    force_treeevaluate_ewald_correction(int target, int mode, double pos_x, double pos_y, double pos_z, double aold);
void   force_treeevaluate_potential(int target, int type);
void   force_treeevaluate_potential_shortrange(int target, int mode);
int    force_treeevaluate_shortrange(int target, int mode);
void   force_treefree(void);
void   force_treeupdate_pseudos(void);
void   force_update_hmax(void);
void   force_update_len(void);
void   force_update_node(int no, int flag);
void   force_update_node_hmax_local(void);
void   force_update_node_hmax_toptree(void);
void   force_update_node_len_local(void);
void   force_update_node_len_toptree(void);
void   force_update_node_recursive(int no, int sib, int father);
void   force_update_pseudoparticles(void);
void   force_update_size_of_parent_node(int no);

void   free_memory(void);

int    get_bytes_per_blockelement(enum iofields blocknr);
void   get_dataset_name(enum iofields blocknr, char *buf);
int    get_datatype_in_block(enum iofields blocknr);
double get_drift_factor(int time0, int time1);
double get_gravkick_factor(int time0, int time1);
double get_hydrokick_factor(int time0, int time1);
int    get_particles_in_block(enum iofields blocknr, int *typelist);
#ifdef POLYTROPE
FLOAT  get_pressure(FLOAT density);
FLOAT  get_energy(FLOAT density);
#endif
double get_random_number(int id);
void   get_Tab_IO_Label(enum iofields blocknr, char *label);
int    get_timestep(int p, double *a, int flag, int verbose);
int    get_values_per_blockelement(enum iofields blocknr);

int    grav_tree_compare_key(const void *a, const void *b);
void   gravity_forcetest(void);
void   gravity_tree(void);
void   gravity_tree_shortrange(void);
double gravkick_integ(double a, void *param);

int    hydro_compare_key(const void *a, const void *b);
void   hydro_evaluate(int target, int mode);
void   hydro_force(void);
double hydrokick_integ(double a, void *param);

int    imax(int, int);
int    imin(int, int);

void   init(void);
void   init_drift_table(void);
void   init_peano_map(void);

void   long_range_force(void);
void   long_range_init(void);
void   long_range_init_regionsize(void);
void   move_particles(int time0, int time1);
size_t my_fread(void *ptr, size_t size, size_t nmemb, FILE * stream);
size_t my_fwrite(void *ptr, size_t size, size_t nmemb, FILE * stream);

int    ngb_clear_buf(FLOAT searchcenter[3], FLOAT hguess, int numngb);
void   ngb_treeallocate(int npart);
void   ngb_treebuild(void);
int    ngb_treefind_pairs(FLOAT searchcenter[3], FLOAT hsml, int *startnode);
int    ngb_treefind_variable(FLOAT searchcenter[3], FLOAT hguess, int *startnode);
int    ngb_treefind_variable_sink(FLOAT searchcenter[3], FLOAT hguess, int *startnode);
void   ngb_treefree(void);
void   ngb_treesearch(int);
void   ngb_treesearch_pairs(int);
void   ngb_update_nodes(void);

void   open_outputfiles(void);

peanokey peano_hilbert_key(int x, int y, int z, int bits);
void   peano_hilbert_order(void);

void   pm_init_nonperiodic(void);
void   pm_init_nonperiodic_allocate(int dimprod);
void   pm_init_nonperiodic_free(void);
void   pm_init_periodic(void);
void   pm_init_periodic_allocate(int dimprod);
void   pm_init_periodic_free(void);
void   pm_init_regionsize(void);
void   pm_setup_nonperiodic_kernel(void);
int    pmforce_nonperiodic(int grnr);
void   pmforce_periodic(void);
int    pmpotential_nonperiodic(int grnr);
void   pmpotential_periodic(void);

double pow(double, double);  /* on some old DEC Alphas, the correct prototype for pow() is missing, even when math.h is included */

#ifdef RADCOL
void density_profile(void);
#endif

#ifdef DMPROFILE
void dm_profile(void);
#endif

#ifdef DMA
void BlumenthalContraction(double initialRadius[], 
                           double initialBaryonMassProfile[],
  	        	   double initialDMMassProfile[],
			   double finalBaryonRadius[],
                           double finalBaryonMass[],
  			   double radiusFactor,
			   int sizeInitial,
			   int sizeFinal,
                           double finalDMRadius[],
			   double finalDMMass[]);
#endif

#ifdef RAYTRACE
void   raytrace(void);
void   raytrace_init_regionsize(void);
#endif
void   read_file(char *fname, int readTask, int lastTask);
void   read_header_attributes_in_hdf5(char *fname);
void   read_ic(char *fname);
int    read_outputlist(char *fname);
void   read_parameter_file(char *fname);
void   readjust_timebase(double TimeMax_old, double TimeMax_new);

void   reorder_gas(void);
void   reorder_particles(void);
void   restart(int mod);
void   run(void);
void   savepositions(int num);

double second(void);

void   seed_glass(void);
void   set_random_numbers(void);
void   set_softenings(void);
void   set_units(void);

void   setup_smoothinglengths(void);
void   statistics(void);
void   terminate_processes(void);
double timediff(double t0, double t1);
void   get_max_rcloud(void);

#ifdef HAVE_HDF5
void   write_header_attributes_in_hdf5(hid_t handle);
#endif
void   write_file(char *fname, int readTask, int lastTask);
void   write_pid_file(void);

/* SINK: accrete routines */
int get_kernel(double **r, double **knl, int type);
int accrete(void);
int create_sinks(void);
void accrete_gas(void);
void add_gas_to_sink(int p, int i);
double get_polytrope(double dens);
void update_global_sink_data(void);

#ifdef CHEMCOOL
double do_chemcool_step(double dt, struct sph_particle_data *current_particle,
                        double radius, double gaccr, int mode);
void save_output_chemcool(struct sph_particle_data *current_particle);
#ifdef RAYTRACE
#ifdef CO_SHIELDING
#ifdef RANGER
double evolve_abundances_(double* dt, double* dl, double* yn, double* divv,
			   double* energy, double* abundances, 
			   double* col_tot, double* col_H2, double* col_CO);
#else
double evolve_abundances__(double* dt, double* dl, double* yn, double* divv,
			   double* energy, double* abundances, 
			   double* col_tot, double* col_H2, double* col_CO);
#endif
#else
#ifdef RANGER
double evolve_abundances_(double* dt, double* dl, double* yn, double* divv,
			   double* energy, double* abundances, 
			   double* col_tot, double* col_H2);
#else
double evolve_abundances__(double* dt, double* dl, double* yn, double* divv,
			   double* energy, double* abundances, 
			   double* col_tot, double* col_H2);
#endif
#endif /* CO_SHIELDING */
#else
#ifdef RANGER
double evolve_abundances_(double* dt, double* dl, double* yn, double* divv,
                          double* energy, double* abundances,
                          double* column_est);
#else
double evolve_abundances__(double* dt, double* dl, double* yn, double* divv,
			   double* energy, double* abundances,
                          double* column_est);
#endif
#endif /* RAYTRACE */
double compute_electron_fraction(FLOAT abundances[NSPEC]);
double compute_initial_electron_fraction(void);
double compute_initial_gamma(void);
double compute_initial_molecular_weight(void);
void rate_eq_(int* nsp, double* t, double* y, double* ydot, double* rpar,
              int* ipar);
void chemcool_init(void);
void coolinmo_(void);
void cheminmo_(void);
double GetCoolTime(struct sph_particle_data *current_particle, double radius, double gaccr);

double compute_stellar_radius(double mass, double mdot);
double compute_luminosity(double mass, double mdot, double radius);

#ifdef RANGER
void calc_gamma_(double* abh2, double* ekn, double* gamma);
void calc_gamma_temp_(double* abh2, double* temp, double* gamma);
void calc_spec_(double* temp, double* en);
void calc_temp_(double* abh2, double* ekn, double* temp);
double compute_gamma_(double* abh2, double* ekn, double* gamma);
void init_temperature_lookup_(void);
void init_tolerances_(void);
void load_h2_table_(void);
#if CHEMISTRYNETWORK == 7
void calc_photo_(double* temp, double rpar[NRPAR], double* abh2, double* abhd, double* abco);
#else
void calc_photo_(double* temp, double rpar[NRPAR], double* abh2, double* abhd, double* abco,
	         double phrates[NPR], double* G_dust, double* chi_isrf);
#endif
#else
void calc_gamma__(double* abh2, double* ekn, double* gamma);
void calc_gamma_temp__(double* abh2, double* temp, double* gamma);
void calc_spec__(double* temp, double* en);
void calc_temp__(double* abh2, double* ekn, double* temp);
double compute_gamma__(double* abh2, double* ekn, double* gamma);
void init_temperature_lookup__(void);
void init_tolerances__(void);
void load_h2_table__(void);
#if CHEMISTRYNETWORK == 7
void calc_photo__(double* temp, double rpar[NRPAR], double* abh2, double* abhd, double* abco);
#else
void calc_photo__(double* temp, double rpar[NRPAR], double* abh2, double* abhd, double* abco,
	         double phrates[NPR], double* G_dust, double* chi_isrf);
#endif
#endif
#endif

#if defined TREE_RAD || defined TREE_RAD_H2
#ifdef RANGER
void project_column_(double* column, double* columnH2, double* dx_heal, double* dy_heal, double* dz_heal, double* dist, double* nodesize, double* ang_radius, FLOAT projection[NPIX], FLOAT projectionH2[NPIX], int* inode);
void get_angular_coords_(int* k, double* theta, double* phi);
void get_pixels_for_xyz_axes_(int pixels[6]);
#else
void project_column__(double* column, double* columnH2, double* dx_heal, double* dy_heal, double* dz_heal, double* dist, double* nodesize, double* ang_radius, FLOAT projection[NPIX], FLOAT projectionH2[NPIX], int* inode);
void get_angular_coords__(int* k, double* theta, double* phi);
void get_pixels_for_xyz_axes__(int pixels[6]);
#endif
void buildcol2d_(void);
#endif

/* added by Florian Buerzle */
void usedmakefileopts(void);

/* added by rjs*/
#ifdef CHEMCOOL
void updatecooling(void);
#endif

/* pcc */
#ifdef GALACTIC_POTENTIAL
void galactic_potential(void);
double spiral_potential_calc(double x, double y, double z, double ti); 
#endif
