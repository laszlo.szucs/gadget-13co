#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <mpi.h>
#include <gsl/gsl_math.h>

#include "allvars.h"
#include "proto.h"


/*! \file predict.c
 *  \brief drift particles by a small time interval
 *
 *  This function contains code to implement a drift operation on all the
 *  particles, which represents one part of the leapfrog integration scheme.
 */


/*! This function drifts all particles from the current time to the future:
 *  time0 - > time1
 *
 *  If there is no explicit tree construction in the following timestep, the
 *  tree nodes are also drifted and updated accordingly. Note: For periodic
 *  boundary conditions, the mapping of coordinates onto the interval
 *  [0,All.BoxSize] is only done before the domain decomposition, or for
 *  outputs to snapshot files.  This simplifies dynamic tree updates, and
 *  allows the domain decomposition to be carried out only every once in a
 *  while.
 */
void move_particles(int time0, int time1)
{
  int i, j;
  double dt_drift, dt_gravkick, dt_hydrokick, dt_entr;
  double t0, t1;

#ifdef XXLINFO
#ifdef MAGNETIC
  double MeanB_part = 0, MeanB_sum;

#ifdef TRACEDIVB
  double MaxDivB_part = 0, MaxDivB_all;
#endif
#endif
#ifdef TIME_DEP_ART_VISC
  double MeanAlpha_part = 0, MeanAlpha_sum;
#endif
#endif


#ifdef CHEMCOOL
  double t2, t3;
  double new_entropy, new_gamma, rho;
#endif
  double radius, gaccr, dummy;

#ifdef SEMI_IMPLICIT
  double dt_full, dt_semi, entropy_new, entropy_asym;
#endif

  t0 = second();

  if(All.ComovingIntegrationOn)
    {
      dt_drift = get_drift_factor(time0, time1);
      dt_gravkick = get_gravkick_factor(time0, time1);
      dt_hydrokick = get_hydrokick_factor(time0, time1);
    }
  else
    {
      dt_drift = dt_gravkick = dt_hydrokick = (time1 - time0) * All.Timebase_interval;
    }

#ifdef CHEMCOOL
  t2 = second();
#endif

  for(i = 0; i < NumPart; i++)
    {
      if(P[i].Type == 0 && P[i].ID < 0) /*SINK*/
	continue;
      for(j = 0; j < 3; j++)
	P[i].Pos[j] += P[i].Vel[j] * dt_drift;

      if(P[i].Type == 0)
	{
#ifdef PMGRID
	  for(j = 0; j < 3; j++)
	    SphP[i].VelPred[j] +=
	      (P[i].GravAccel[j] + P[i].GravPM[j]) * dt_gravkick + SphP[i].HydroAccel[j] * dt_hydrokick;
#else
	  for(j = 0; j < 3; j++)
	    SphP[i].VelPred[j] += P[i].GravAccel[j] * dt_gravkick + SphP[i].HydroAccel[j] * dt_hydrokick;
#endif
	  SphP[i].Density *= exp(-SphP[i].DivVel * dt_drift);
	  SphP[i].Hsml *= exp(0.333333333333 * SphP[i].DivVel * dt_drift);

	  if(SphP[i].Hsml < All.MinGasHsml)
	    SphP[i].Hsml = All.MinGasHsml;


//fb 07.02.09
//we need dt_entr for computing BPred!
          dt_entr = (time1 - (P[i].Ti_begstep + P[i].Ti_endstep) / 2) * All.Timebase_interval;
#ifdef POLYTROPE
          SphP[i].Pressure = get_pressure(SphP[i].Density);
	  if (SphP[i].Pressure <= 0.0) {
	    printf("Error in predict: particle %d has a negative pressure!\n", P[i].ID);
	    printf("p = %g\n");
	    endrun(90210);
	  }
#else
#ifdef CHEMCOOL
          if (All.NeedAbundancesForOutput == 1) {
           /* If dt_entr is positive, we need to evolve the chemical network forward in time
            * to get the required values. On the other hand, if dt_entr is negative (i.e. this
            * particle has already evolved past the output time), then we need to go back in
            * time. Since we can't evolve the network backwards, we instead simply use the
            * values at the output time that we computed & stored earlier.
            * 
            * Note that the timestep is constrained such that we can only have overshot one
            * output time, so the values we stored are certain to be the values for that 
            * output time.
            */
           if (dt_entr > 0) { /* Output is in this particle's future */
             COOLI.id_current = P[i].ID;
	     COOLI.index_current = i;
	     radius = sqrt(P[i].Pos[0] * P[i].Pos[0] + P[i].Pos[1] * P[i].Pos[1] + P[i].Pos[2] * P[i].Pos[2]);
             gaccr = fabs(P[i].GravAccel[0]*P[i].Pos[0] + P[i].GravAccel[1]*P[i].Pos[1] + P[i].GravAccel[2]*P[i].Pos[2]);
             gaccr /= radius;
             dummy = do_chemcool_step(dt_entr, &SphP[i], radius, gaccr, 0);
           }
           SphP[i].Pressure = SphP[i].EntropyOut * pow(SphP[i].Density, SphP[i].GammaOut);
         }
         else {
#ifdef SEMI_IMPLICIT
           dt_full = (P[i].Ti_endstep - P[i].Ti_begstep) * All.Timebase_interval;
           dt_semi = (time1 - P[i].Ti_begstep) * All.Timebase_interval;
	   if (dt_full == 0.0) {
	     entropy_new = SphP[i].Entropy;
	   }
           else if (dt_full < 1e-4 * SphP[i].DtTherm) {
	     /* Approximate exponential terms */
             entropy_new = SphP[i].Ebeg * (1 - dt_semi / dt_full) + SphP[i].Efinal * dt_semi / dt_full;
	   }
	   else {
             entropy_asym = (SphP[i].Efinal - SphP[i].Ebeg * exp(-dt_full / SphP[i].DtTherm)) 
                          / (1.0 - exp(-dt_full / SphP[i].DtTherm));
             entropy_new  = SphP[i].Ebeg * exp(-dt_semi / SphP[i].DtTherm) + entropy_asym * (1.0 - exp(-dt_semi / SphP[i].DtTherm));
	   }
	   SphP[i].Pressure = entropy_new * pow(SphP[i].Density, SphP[i].Gamma);
#else /* SEMI_IMPLICIT */
	   new_entropy = SphP[i].Entropy + SphP[i].DtEntropy * dt_entr;
           rho = SphP[i].Density;
           if (All.ComovingIntegrationOn) {
	     rho = rho / (All.Time * All.Time * All.Time);
	   }
           if (new_entropy < 0.0) {
             printf("Particle %d has a negative entropy: ent = %g, dtent = %g, dt = %g\n", P[i].ID, SphP[i].Entropy, SphP[i].DtEntropy, dt_entr);
             endrun(103);
	   }
#ifdef NO_VARIABLE_GAMMA
           new_gamma = 5./3.;
#else
	   new_gamma = calc_gamma_from_entropy(new_entropy, rho, SphP[i].TracAbund[IH2], SphP[i].Gamma);
#endif
	   SphP[i].Pressure = new_entropy * pow(SphP[i].Density, new_gamma);
#endif /* SEMI_IMPLICIT */
         }
#else /* CHEMCOOL */
	  SphP[i].Pressure = (SphP[i].Entropy + SphP[i].DtEntropy * dt_entr) * pow(SphP[i].Density, GAMMA);
#endif /* CHEMCOOL */
	  if (SphP[i].Pressure <= 0.0) {
	    printf("Error in predict: particle %d has a negative pressure!\n", P[i].ID);
#ifdef SEMI_IMPLICIT
	    printf("ent = %g, ent_asym = %g, ent_beg = %g, ent_end = %g\n", entropy_new, entropy_asym, SphP[i].Ebeg, SphP[i].Efinal);
#else
	    printf("p = %g, ent = %g, dent_dt = %g, dt_entr = %g\n", SphP[i].Pressure, SphP[i].Entropy, SphP[i].DtEntropy, dt_entr);
#endif
            printf("position %g %g %g  \n", P[i].Pos[0], P[i].Pos[1], P[i].Pos[2]);
            printf("density %g \n", SphP[i].Density);
	    endrun(90210);
	  }
#endif /* POLYTROPE */

#ifdef MAGNETIC
#ifndef EULERPOTENTIALS
	  for(j = 0; j < 3; j++)
	    SphP[i].BPred[j] += SphP[i].DtB[j] * dt_entr;
#endif
#if defined(EULERPOTENTIALS) && defined(EULER_DISSIPATION)
          SphP[i].EulerA += SphP[i].DtEulerA * dt_entr;
          SphP[i].EulerB += SphP[i].DtEulerB * dt_entr;
#endif
#ifdef DIVBCLEANING_DEDNER
	  SphP[i].PhiPred += SphP[i].DtPhi * dt_entr;
          SphP[i].Jump_Entr= sqrt(SphP[i].BPred[0]*SphP[i].BPred[0]+
	        	          SphP[i].BPred[1]*SphP[i].BPred[1]+
	  			  SphP[i].BPred[2]*SphP[i].BPred[2]);

          if(SphP[i].Jump_Entr != 0.0 )
            SphP[i].Jump_Entr = fabs(PPP[i].Hsml / (SphP[i].MaxSignalVel * SphP[i].Jump_Entr) *
			sqrt(SphP[i].DtB[0]*SphP[i].DtB[0]+SphP[i].DtB[1]*SphP[i].DtB[1]+SphP[i].DtB[2]*SphP[i].DtB[2]));     
#endif
#endif /* MAGNETIC */
#ifdef XXLINFO
	  if(Flag_FullStep == 1)
	    {
#ifdef MAGNETIC
	      MeanB_part += sqrt(SphP[i].BPred[0] * SphP[i].BPred[0] +
				 SphP[i].BPred[1] * SphP[i].BPred[1] + SphP[i].BPred[2] * SphP[i].BPred[2]);
#ifdef TRACEDIVB
	      MaxDivB_part = DMAX(MaxDivB, fabs(SphP[i].divB));
#endif
#endif
#ifdef TIME_DEP_ART_VISC
	      MeanAlpha_part += SphP[i].alpha;
#endif
	    }
#endif
	}
    }

#ifdef CHEMCOOL
  t3 = second();
#endif 

  /* if domain-decomp and tree are not going to be reconstructed, update dynamically.  */
  if(All.NumForcesSinceLastDomainDecomp < All.TotNumPart * All.TreeDomainUpdateFrequency)
    {
      for(i = 0; i < Numnodestree; i++)
	for(j = 0; j < 3; j++)
	  Nodes[All.MaxPart + i].u.d.s[j] += Extnodes[All.MaxPart + i].vs[j] * dt_drift;

      force_update_len();

      force_update_pseudoparticles();
    }

  t1 = second();

#ifdef CHEMCOOL
  All.CPU_Chemcool += timediff(t2, t3);
#endif
  All.CPU_Predict += timediff(t0, t1);

#ifdef XXLINFO
  if(Flag_FullStep == 1)
    {
#ifdef MAGNETIC
      MPI_Reduce(&MeanB_part, &MeanB_sum, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
      if(ThisTask == 0)
	MeanB = MeanB_sum / All.TotN_gas;
#ifdef TRACEDIVB
      MPI_Reduce(&MaxDivB_part, &MaxDivB_all, 1, MPI_DOUBLE, MPI_MAX, 0, MPI_COMM_WORLD);
      if(ThisTask == 0)
	MaxDivB = MaxDivB_all;
#endif
#endif
#ifdef TIME_DEP_ART_VISC
      MPI_Reduce(&MeanAlpha_part, &MeanAlpha_sum, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
      if(ThisTask == 0)
	MeanAlpha = MeanAlpha_sum / All.TotN_gas;
#endif
    }
#endif



}



/*! This function makes sure that all particle coordinates (Pos) are
 *  periodically mapped onto the interval [0, BoxSize].  After this function
 *  has been called, a new domain decomposition should be done, which will
 *  also force a new tree construction.
 */
#ifdef PERIODIC
void do_box_wrapping(void)
{
  int i, j;
  double boxsize[3];

  for(j = 0; j < 3; j++)
    boxsize[j] = All.BoxSize;

#ifdef LONG_X
  boxsize[0] *= LONG_X;
#endif
#ifdef LONG_Y
  boxsize[1] *= LONG_Y;
#endif
#ifdef LONG_Z
  boxsize[2] *= LONG_Z;
#endif

  for(i = 0; i < NumPart; i++) 
    {
      if(P[i].Type == 0 && P[i].ID < 0) /*SINK*/
      continue;
    for(j = 0; j < 3; j++)
      {
	while(P[i].Pos[j] < 0)
	  P[i].Pos[j] += boxsize[j];

	while(P[i].Pos[j] >= boxsize[j])
	  P[i].Pos[j] -= boxsize[j];
      }
    }
}
#endif

#ifdef POLYTROPE
FLOAT get_pressure(FLOAT density) 
{
  FLOAT p, p1, p2, d1, d2, dd, logdens;
  int indx;

  /* Rescale from code units to CGS number density of H nuclei */
  density *= All.UnitDensity_in_cgs / ((1.0 + 4.0 *ABHE)*PROTONMASS);
  logdens = log10(density);
  if (logdens <= All.MinTabulatedDensity) {
    /* At densities below the minimum tabulated density, pressure scales as p ~ rho^All.PolyIndexLowDensity */
    p  = All.EOSPressure[0];
    p *= pow((pow(10, logdens) / pow(10, All.MinTabulatedDensity)), All.PolyIndexLowDensity);
  }
  else if (logdens >= All.MaxTabulatedDensity) {
    /* At densities above the maximum tabulated density, pressure scales as p ~ rho^All.PolyIndexHighDensity */
    p  = All.EOSPressure[All.EOSFullTableSize - 1];
    p *= pow((pow(10, logdens) / pow(10, All.MaxTabulatedDensity)), All.PolyIndexHighDensity);
  }
  else {
    indx = floor((logdens - All.MinTabulatedDensity) / All.EOSDensDel);
    d1 = All.EOSDensity[indx];
    d2 = All.EOSDensity[indx+1];
    p1 = All.EOSPressure[indx];
    p2 = All.EOSPressure[indx+1];
    dd = (logdens - d1) / (d2 - d1);
    p  = pow(10, log10(p1) + dd * (log10(p2) - log10(p1)));
  }

  /* Convert p back into code units */
  p /= All.UnitPressure_in_cgs;
  return p;
}

FLOAT get_energy(FLOAT density) 
{
  FLOAT e, e1, e2, d1, d2, dd, logdens;
  int indx;

  /* Rescale from code units to CGS number density of H nuclei */
  density *= All.UnitDensity_in_cgs / ((1.0 + 4.0 *ABHE)*PROTONMASS);
  logdens = log10(density);
  if (logdens <= All.MinTabulatedDensity) {
    /* At densities below the minimum tabulated density, energy scales as e ~ rho^All.PolyIndexLowDensity */
    e  = All.EOSEnergy[0];
    e *= pow((pow(10, logdens) / pow(10, All.MinTabulatedDensity)), All.PolyIndexLowDensity);
  }
  else if (logdens >= All.MaxTabulatedDensity) {
    /* At densities above the maximum tabulated density, energy scales as e ~ rho^All.PolyIndexHighDensity */
    e  = All.EOSEnergy[All.EOSFullTableSize - 1];
    e *= pow((pow(10, logdens) / pow(10, All.MaxTabulatedDensity)), All.PolyIndexHighDensity);
  }
  else {
    indx = floor((logdens - All.MinTabulatedDensity) / All.EOSDensDel);
    d1 = All.EOSDensity[indx];
    d2 = All.EOSDensity[indx+1];
    e1 = All.EOSEnergy[indx];
    e2 = All.EOSEnergy[indx+1];
    dd = (logdens - d1) / (d2 - d1);
    e  = pow(10, log10(e1) + dd * (log10(e2) - log10(e1)));
  }

  /* Convert e back into code units. Note that conversion factor
   * for the internal energy density is the same as that for the
   * pressure
   */
  e /= All.UnitPressure_in_cgs;
  return e;
}


#endif /* POLYTROPE */
